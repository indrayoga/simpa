<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserValidation{

	/** @var codeigniter objek */
	protected $CI;
	/** @var object, for db accesss */
	protected $DB;
	/**
	 * Error messages list
	 *
	 * @var	array
	 */
	public $error_msg = array();

	public function  __construct(){
        // Assign the CodeIgniter super-object
        $this->CI =& get_instance();
        $this->DB=$this->CI->db;
	}

	/**
	 * Set an error message
	 *
	 * @param	string	$msg
	 * @return	object
	 */
	public function setError($msg, $log_level = 'error')
	{

		is_array($msg) OR $msg = array($msg);
		foreach ($msg as $val)
		{
			$this->error_msg[] = $msg;
			log_message($log_level, $msg);
		}

		return $this;
	}

	/**
	 * Display the error message
	 *
	 * @param	string	$open
	 * @param	string	$close
	 * @return	string
	 */
	public function getError($open = '<p>', $close = '</p>')
	{
		return (count($this->error_msg) > 0) ? $open.implode($close.$open, $this->error_msg).$close : '';
	}

	/**
	 * Validasi Email
	 *
	 * @param string $email email yang akan divalidasi
	 * @return true jika email valid, false jika tidak
	 * @throws email yang tidak valid
	 */
	public function validateEmailAddress($email) {
		if (empty($email)) {
			$this->setError('Email Tidak Boleh Kosong', 'debug');
			return false;
		}

		$email = trim($email);

		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$this->setError('Email Tidak Valid', 'debug');
			return false;
		}

		return true;
	}

	/**
	 * Validasi Password
	 *
	 * @param string $password password yang akan divalidasi
	 * @return true jika password valid, false jika tidak
	 * @throws InvalidPasswordException jika password tidak valid
	 */
	public function validatePassword($password) {
		if (empty($password)) {
			$this->setError('Password Tidak Boleh Kosong', 'debug');
			return false;
		}

		$password = trim($password);

		if (strlen($password) < 1) {
			$this->setError('Email Tidak Boleh Kosong', 'debug');
			return false;
		}

		return $password;
	}

	/**
	 * Validasi Username
	 *
	 * @param string $username username yang akan divalidasi
	 * @return true jika username valid, false jika tidak
	 * @throws InvalidUsernameException jika username tidak valid
	 */
	public function validateUsername($username) {
		if (empty($username)) {
			$this->setError('Username Tidak Boleh Kosong', 'debug');
			return false;
		}

		$username = trim($username);

		if (strlen($username) < 1) {
			$this->setError('Username Tidak Boleh Kosong', 'debug');
			return false;
		}

		return true;
	}

	/**
	 * Validasi Username yang Beda
	 *
	 * @param string $username username yang akan divalidasi
	 * @return true jika username unik, false jika tidak
	 * @throws InvalidUsernameException jika username tidak valid
	 */
	public function uniqeUsername($username) {
		$this->DB->where('USERNAME',$username);
		$this->DB->from('admin');
		$count=$this->DB->count_all_results();
		if($count>0){
			$this->setError('Username sudah ada', 'debug');
			return false;
		}
		return true;
	}

	public function isUserActive($username){
		$this->DB->where('USERNAME',$username);
		$this->DB->from('admin');
		$query=$this->DB->get();
		if($query->num_rows() == 0){
			$this->setError('Username anda tidak aktif/di blokir', 'debug');
			return false;
		}else{
			$row=$query->row_array();
			if($row['STATUS']!='AKTIF'){
				$this->setError('Username anda tidak aktif/di blokir', 'debug');
				return false;
			}
		}
		return true;
		
	}

}