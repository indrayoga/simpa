<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	/**
	 * @internal
	 *
	 * 
	 */
	public function __construct() {
		parent::__construct();
		$this->load->helper('form');
		$this->load->library(array('auth','form_validation'));
		var_dump($this->auth);
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	public function index()
	{
		$this->load->view('welcome_message');
	}

	/**
	 * login form
	 *
	 * @return string form login
	 */
	public function login(){
		if($this->_validasiFormLogin()){
			if($this->auth->login($this->input->post('username'),$this->input->post('password'))){
				$data=$this->db->get_where('tahun_anggaran',array('aktif'=>1))->row_array();
				$this->session->set_userdata('tahun_anggaran', $data['tahun']);
				redirect('/admin/dashboard','refresh');
			}		
		}
		$csrf = array(
		        'name' => $this->security->get_csrf_token_name(),
		        'hash' => $this->security->get_csrf_hash()
		);
		$data=array('csrf'=>$csrf);		
		$this->load->view('user/login',$data);			

	}

	/**
	 * logout
	 *
	 * @return void
	 */
	public function logout(){
		$this->auth->logout();
		redirect('user/login');
	}

	public function _validasiFormLogin(){
        $this->form_validation->set_rules('username', 'Username', 'required',
                array('required' => ' %s Harus Di isi.')
        );

        $this->form_validation->set_rules('password', 'Password', 'required',
                array('required' => ' %s Harus Di isi.')
        );
        if ($this->form_validation->run() == FALSE)
        {
                return false;
        }
        else
        {
                return true;
        }

	}
}