<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends CI_Controller {

	public function  __construct(){
		parent::__construct();
		$this->user=$this->auth->getUser();
		$this->load->model(array('mkk','mdipa'));
	}

	public function userpemohon(){
		if($this->input->is_ajax_request()==false)show_error('bukan ajax request');
		$dipa=$this->db->get_where('dipa',array('id_dipa'=>$this->input->post('id_output')))->row_array();
		$items=$this->db->get_where('users',array('seksi'=>$dipa['kode_bidang']))->result_array();
		$this->output->set_content_type('application/json')->set_output(json_encode($items));	
	}

	public function suboutput(){
		if($this->input->is_ajax_request()==false)show_error('bukan ajax request');
		$items=$this->mdipa->getAllSubOutput($this->input->post('id_output'));
		$this->output->set_content_type('application/json')->set_output(json_encode($items));	
	}

	public function subkomponen(){
		if($this->input->is_ajax_request()==false)show_error('bukan ajax request');
		$items=$this->mdipa->getAllSubOutput($this->input->post('id_induk'));
		$this->output->set_content_type('application/json')->set_output(json_encode($items));	
	}

	public function kalenderkegiatan(){
		if($this->input->is_ajax_request()==false)show_error('bukan ajax request');
		$items=$this->mkk->getByIdKegiatan($this->input->post('id'));
		$this->output->set_content_type('application/json')->set_output(json_encode($items));	
	}

	public function akunkegiatan(){
		if($this->input->is_ajax_request()==false)show_error('bukan ajax request');
		$items=$this->mdipa->getAllAkunKegiatan($this->input->post('id'));
		$this->output->set_content_type('application/json')->set_output(json_encode($items));			
	}
}