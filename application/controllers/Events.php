<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Events extends CI_Controller {
	public $user;
	public function  __construct(){
		parent::__construct();
		$this->user=$this->auth->getUser();
		$this->load->model(array('mum','mkk','mkpa','mppk','mdipa'));
		$this->load->library('form_validation');
	}

	public function index()
	{
		if($this->auth->checkRole('Pemohon')){
			$this->db->join('dipa','kalender_kegiatan.id_kegiatan=dipa.id_dipa');
			$items=$this->db->get_where('kalender_kegiatan',array())->result_array();			
		}else{
			$this->db->join('dipa','kalender_kegiatan.id_kegiatan=dipa.id_dipa');
			$items=$this->db->get_where('kalender_kegiatan',array())->result_array();						
		}
		$data=array();
		foreach ($items as $item) {
			# code...
			$hitungminggu=(($item['bulan_berakhir']-1)*4)+$item['minggu_terakhir'];
			$date = new DateTime();
			$date->setISODate($item['tahun_anggaran'], $hitungminggu, 7);
			$data[]=array(
				'title'=>$item['kegiatan'],
				'start'=>$item['tgl_mulai'],
				'end'=>$date->format('Y-m-d')
				);
		}
		echo json_encode($data);
	}

}
