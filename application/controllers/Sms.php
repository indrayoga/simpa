<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sms extends CI_Controller {
	public $user;
	public function  __construct(){
		parent::__construct();
		$this->user=$this->auth->getUser();
		$this->load->model(array('mum','mkk','mkpa','mppk','mdipa'));
		$this->load->library('form_validation');
	}

	public function send()
	{
		$dbsms=$this->load->database('sms',TRUE);
		//var_dump($dbsms);
		$data=array(
					'pesan'=>"Slmt pagi bpk/ibu yth, berdsarkn kalender kerja, terdpt jadwal yg akan dilaksanakan 3mnggu yad. Mohon utk segera mengajukan UM melalui aplikasi SIMPA. Trm ksh.",
			'notujuan'=>'081350912242'
			);
		$dbsms->insert('sms_outbox',$data);
		return true;
	}

	public function kirim(){
		//$dbsms=$this->load->database('sms',TRUE);
		$items=$this->db->query("select *,kalender_kegiatan.id as id_kalender_kegiatan from kalender_kegiatan join users on kalender_kegiatan.id_user=users.id join dipa on kalender_kegiatan.id_kegiatan=dipa.id_dipa where date(tgl_mulai) = DATE_ADD(curdate(),INTERVAL 3 WEEK) and kalender_kegiatan.id not in (select id_kalender_kegiatan from sms_outbox group by id_kalender_kegiatan)")->result_array();
		$users=$this->db->where_in('role',array('PPK','PPSPM','BENDAHARA','KPA'))->get('users')->result_array();
		if(!empty($items)){
			$data=array();
			$datasms=array();
			foreach ($items as $item) {
				# code...
				$data[]=array(
					'pesan'=>"Slmt pagi bpk/ibu yth, berdsarkn kalender kerja, terdpt jadwal yg akan dilaksanakan 3mnggu yad. Mohon utk segera mengajukan UM melalui aplikasi SIMPA. Trm ksh.",
					'notujuan'=>$item['no_hp']
					);
				$datasms[]=array(
					'pesan'=>"Slmt pagi bpk/ibu yth, berdsarkn kalender kerja, terdpt jadwal yg akan dilaksanakan 3mnggu yad. Mohon utk segera mengajukan UM melalui aplikasi SIMPA. Trm ksh.",
					'id_kalender_kegiatan'=>$item['id_kalender_kegiatan'],
					'no_tujuan'=>$item['no_hp'],
					'id_user'=>$item['id_user']
					);
				foreach ($users as $user) {
					# code...
					$data[]=array(
						'pesan'=>"Slmt pagi bpk/ibu yth, berdsarkn kalender kerja, terdpt jadwal yg akan dilaksanakan 3mnggu yad. Mohon utk segera mengajukan UM melalui aplikasi SIMPA. Trm ksh.",
						'notujuan'=>$user['no_hp']
						);
					$datasms[]=array(
						'pesan'=>"Slmt pagi bpk/ibu yth, berdsarkn kalender kerja, terdpt jadwal yg akan dilaksanakan 3mnggu yad. Mohon utk segera mengajukan UM melalui aplikasi SIMPA. Trm ksh.",
						'id_kalender_kegiatan'=>$item['id_kalender_kegiatan'],
						'no_tujuan'=>$user['no_hp'],
						'id_user'=>$user['id']
						);
				}
				//$this->db->update('outbox',array('send'=>1),array('UpdatedInDB'=>$item['UpdatedInDB']));
			}
		//$dbsms->insert('sms_outbox',$data);			
		$this->db->insert_batch('sms_outbox',$datasms);			
		}
	}
}
