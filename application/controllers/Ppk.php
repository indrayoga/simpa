<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ppk extends CI_Controller {
	public $user;
	public function  __construct(){
		parent::__construct();
		$this->user=$this->auth->getUser();
		$this->load->model(array('mum','mppk','mdipa','mkk'));
		$this->load->library('form_validation');
	}

	public function _remap($method,$param=array()){
	        if ($method === 'page'){
	                $this->index();
	        }else if ($method === 'search'){
	        	$data=array();
	        	if($this->input->post('id_kegiatan')!="")$data['kegiatan']=$this->input->post('id_kegiatan');
	        	if($this->input->post('waktu')!="")$data['waktu']=$this->input->post('waktu');
	        	if($this->input->post('lokasi')!="")$data['lokasi']=$this->input->post('lokasi');
	        	if($this->input->post('tag_ppk')!="")$data['tag_ppk']=$this->input->post('tag_ppk');
	        	if($this->input->post('tag_kpa')!="")$data['tag_kpa']=$this->input->post('tag_kpa');
				$str = $this->uri->assoc_to_uri($data);        		
	            redirect('ppk/'.$str.'/page/1/');
	        }else if (method_exists($this, $method)){
                return call_user_func_array(array($this, $method),$param);
	        }else{
	            $this->index();	        	
	        }
	}

	public function index()
	{
		if ($this->auth->checkRole('PPK')===false)show_error('Anda Tidak Memiliki Akses Ke Halaman Ini');
		$default=array('page','kegiatan','waktu','lokasi','tag_kpa','tag_ppk');
		$search = $this->uri->uri_to_assoc(2,$default);
		$search['lokasi']=urldecode($search['lokasi']);
		$this->load->helper('date');
		$perPage=10;
		if(empty($search['page']))$search['page']=1;
		$offset=($search['page']*$perPage)-$perPage;
		$total_rows = $this->mum->getCountRows(['user'=>'','kegiatan'=>$search['kegiatan'],'waktu'=>convertDate($search['waktu'],'d-m-Y','Y-m-d'),'lokasi'=>$search['lokasi'],'tag_kpa'=>$search['tag_kpa'],'tag_ppk'=>$search['tag_ppk']]);

		$data=array(
			'csrf' => array(
		        'name' => $this->security->get_csrf_token_name(),
		        'hash' => $this->security->get_csrf_hash()
			),
			'offset'=>$offset,
			'search'=>$search,
			'datakegiatan'=>$this->mdipa->getByBidang(),
			'items'=>$this->mum->getAll(['user'=>'','kegiatan'=>$search['kegiatan'],'waktu'=>convertDate($search['waktu'],'d-m-Y','Y-m-d'),'lokasi'=>$search['lokasi'],'tag_kpa'=>1,'tag_ppk'=>$search['tag_ppk']])
			);
		array_walk($data['items'],array($this,'_getStatusPPK'));
		array_walk($data['items'],function(&$item){
			$item['jumlah']=$this->mum->getTotalDetilBelanja($item['id']);
		});
		unset($search['page']);
		$str = $this->uri->assoc_to_uri($search);        		
		$base_url = base_url()."ppk/{$str}/page/";
		$data['paging']=$this->_createPaging($base_url,$total_rows,$perPage);		
		$this->load->view('header');
		$this->load->view('ppk/index',$data);
		$this->load->view('footer');
	}

	public function _createPaging($base_url,$total_rows,$per_page=10){
		$this->load->library('pagination');
		$config['base_url'] = $base_url;
		$config['total_rows'] = $total_rows;
		$config['per_page'] = $per_page;
		$config['full_tag_open'] = '<nav><ul class="pagination">';
		$config['first_link'] = FALSE;
		$config['last_link'] = FALSE;
		$config['prev_tag_open'] = '<li>';
		$config['next_tag_open'] = '<li>';
		$config['num_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['next_tag_close'] = '</li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li><a>';
		$config['cur_tag_close'] = '</a></li>';
		$config['full_tag_close'] = '</ul></nsav>';
		$config['use_page_numbers'] = TRUE;
		$this->pagination->initialize($config);
		return $this->pagination->create_links();				
	}

	public function _getStatusPPK(&$item){

		switch ($item['tag_ppk']) {
			case 0:
				if($item['tag_revisi_ppk']==0){
					$item['status']="Menunggu Persetujuan PPK";
				}else{
					$item['status']="Menunggu Persetujuan PPK (Sudah Revisi)";
				}
				$item['status_code']=0;
				break;
			case 1:
				$item['status']='<img src="'.base_url().'/assets/img/sf/sign-check.svg" width="24" height="24" alt="Lulus">';
				$item['status_code']=1;
				break;
			case 2:
				$item['status_code']=2;
				$catatan=$this->mppk->getCatatanTerbaru($item['id']);
				$item['status']='<img src="'.base_url().'/assets/img/sf/sign-error.svg" width="24" height="24" alt="Tidak Lulus"> <br/><button type="button" class="btn btn-sm btn-danger" data-toggle="popover" data-trigger="focus" data-placement="top" title="Catatan" data-content="'.$this->security->xss_clean($catatan['catatan']).'">[klik untuk melihat pesan]</button>';
				break;
			
			default:
				$item['status_code']=0;
				$item['status']="Menunggu Persetujuan PPK";
				break;
		}

		switch ($item['tag_evaluasi']) {
			case 0:
				$item['status_evaluasi']="Hasil Kegiatan Belum di Upload";
				break;
			case 1:
				$item['status_evaluasi']='<img src="'.base_url().'/assets/img/sf/sign-error.svg" width="24" height="24" alt="Belum di evaluasi">';
				$item['status_code']=1;
				break;
			case 2:
				$item['status_evaluasi']='<img src="'.base_url().'/assets/img/sf/sign-check.svg" width="24" height="24" alt="Selesai">';
				break;
			
			default:
				$item['status_evaluasi']="Hasil Kegiatan Belum di Upload";
				break;
		}
	}

	public function pengeluaran(){
		if ($this->auth->checkRole('PPK')===false)show_error('Anda Tidak Memiliki Akses Ke Halaman Ini');

		$data=array(
			'items'=>$this->mppk->getAllPengeluaran()
			);
		$this->load->view('header');
		$this->load->view('ppk/pengeluaran',$data);
		$this->load->view('footer');
	}

	public function detail($id)
	{
		if ($this->auth->checkRole('PPK')===false)show_error('Anda Tidak Memiliki Akses Ke Halaman Ini');

		$data=array(
			'csrf' => array(
		        'name' => $this->security->get_csrf_token_name(),
		        'hash' => $this->security->get_csrf_hash()
			),
			'datakegiatan'=>$this->mdipa->getByBidang(),
			'id'=>$this->encryption->encrypt($id),
			'datarab'=>$this->mum->getDetil($id),
			'dataakun'=>$this->db->get('akun')->result_array(),
			'item'=>$this->mum->get($id)
			);
			list($data['item']['tanggal'],$data['item']['jam'])=explode(" ", $data['item']['waktu']);
		$this->load->view('header');
		$this->load->view('ppk/detail',$data);
		$this->load->view('footer');
	}

	public function evaluasi($id)
	{
		if ($this->auth->checkRole('PPK')===false)show_error('Anda Tidak Memiliki Akses Ke Halaman Ini');

		$data=array(
			'csrf' => array(
		        'name' => $this->security->get_csrf_token_name(),
		        'hash' => $this->security->get_csrf_hash()
			),
			'datakegiatan'=>$this->mdipa->getByBidang(),
			'id'=>$this->encryption->encrypt($id),
			'items'=>$this->mum->getAllHasilKegiatan($id),
			'item'=>$this->mum->get($id)
			);
			list($data['item']['tanggal'],$data['item']['jam'])=explode(" ", $data['item']['waktu']);
		$this->load->view('header');
		$this->load->view('ppk/evaluasi',$data);
		$this->load->view('footer');
	}

	public function approval(){
		$rowUM=$this->mum->get($this->encryption->decrypt($this->input->post('id')));
		if(empty($rowUM))show_404();
		if($this->input->post('status')==2){
			$this->form_validation->set_rules('catatan', 'Catatan', 'required');
		}else{
			$this->form_validation->set_rules('catatan', 'Catatan','max_length[10000]');			
		}
		if ($this->form_validation->run() == FALSE)
		{
			$this->detail($rowUM['id']);
		}else{
			$data=array(
				'id_pengajuan_um'=>$rowUM['id'],
				'status'=>$this->input->post('status'),
				'catatan'=>$this->input->post('catatan'),
				'id_user'=>$this->user['id']
				);
			$this->mppk->create($data);
			$dataum=array('tag_ppk'=>$this->input->post('status'));
			$this->mum->update($dataum,$rowUM['id']);	

			if($this->input->post('status')==1){
				$pesan="Pengajuan UM untuk Kegiatan ".$rowUM['kegiatan']." Lulus oleh PPK";
			}else{
				$pesan="Pengajuan UM untuk Kegiatan ".$rowUM['kegiatan']." Tidak Lulus oleh KPA";				
			}
			$datalog=array(
				'id_user'=>$rowUM['id_user'],
				'role'=>'Pemohon',
				'pesan'=>$pesan
				);
			$this->db->insert('pesan',$datalog);

			redirect('ppk');
		}
	}

	public function modify($id)
	{
		if ($this->auth->checkRole('PPK')===false)show_error('Anda Tidak Memiliki Akses Ke Halaman Ini');

		$data=array(
			'csrf' => array(
		        'name' => $this->security->get_csrf_token_name(),
		        'hash' => $this->security->get_csrf_hash()
			),
			'id'=>$this->encryption->encrypt($id),
			'item'=>$this->mum->get($id),
			'dataoutput'=>$this->mdipa->getAllOutput(),
			'datarab'=>$this->mum->getDetil($id),
			'dataakun'=>$this->db->get('akun')->result_array(),
			'datakegiatan'=>$this->mkk->getAll($this->user['id'])
			);
		$data['kegiatan']=$this->mum->getTreeKegiatan($data['item']['id_kegiatan']);
		if(count($data['kegiatan'])==3){
			$data['datakomponenkegiatan']=$this->mdipa->getAllSubOutput($data['kegiatan'][0]['id_dipa']);
			$data['datakegiatan']=$this->mdipa->getAllSubOutput($data['kegiatan'][1]['id_dipa']);
		}
		if(count($data['kegiatan'])==4){
			$data['datasuboutput']=$this->mdipa->getAllSubOutput($data['kegiatan'][0]['id_dipa']);
			$data['datakomponenkegiatan']=$this->mdipa->getAllSubOutput($data['kegiatan'][1]['id_dipa']);
			$data['datakegiatan']=$this->mdipa->getAllSubOutput($data['kegiatan'][2]['id_dipa']);
		}

			list($data['item']['tanggal'],$data['item']['jam'])=explode(" ", $data['item']['waktu']);
		$this->load->view('header');
		$this->load->view('ppk/modify',$data);
		$this->load->view('footer');
	}

	public function update(){
		$rowUM=$this->mum->get($this->encryption->decrypt($this->input->post('id')));
		if(empty($rowUM))show_404();
		$this->form_validation->set_rules('id_output', 'Output', 'required');
		if($this->input->post('id_komponen_kegiatan')=="" && $this->input->post('id_sub_komponen_kegiatan')==""){
			$this->form_validation->set_rules('id_output', 'Komponen Kegiatan', 'required');			
		}
		$this->form_validation->set_rules('tahun_anggaran', 'Tahun', 'required|exact_length[4]');
		$this->form_validation->set_rules('tanggal', 'Tanggal', array('required','exact_length[10]'));
		$this->form_validation->set_rules('jam', 'Jam', array('exact_length[8]'));
		$this->form_validation->set_rules('lokasi', 'Lokasi', array('required'));

		if ($this->form_validation->run() == FALSE)
		{
			$this->modify($rowUM['id']);
		}else{
			$tanggal = date_create_from_format('d-m-Y', $this->input->post('tanggal'));
			if($this->input->post('jam')=="")$jam="00:00:00"; else $jam=$this->input->post('jam');
			$waktu=date_format($tanggal, 'Y-m-d').' '.$jam;
			if($this->input->post('id_sub_komponen_kegiatan')==""){
				$id_kegiatan=$this->input->post('id_komponen_kegiatan');
			}else{
				$id_kegiatan=$this->input->post('id_sub_komponen_kegiatan');				
			}
			$data=array(
				'tahun_anggaran'=>$this->input->post('tahun_anggaran'),
				'id_kalender_kegiatan'=>$this->input->post('id_kalender_kegiatan'),
				'id_kegiatan'=>$id_kegiatan,
				'waktu'=>$waktu,
				'lokasi'=>$this->input->post('lokasi'),
				'jumlah_peserta'=>$this->input->post('jumlah_peserta'),
				'kode_mata_anggaran'=>$this->input->post('kode_mata_anggaran'),
				);
			$this->mum->update($data,$rowUM['id']);
			$kode_akun=$this->input->post('kode_akun');
			$this->db->delete('detil_pengajuan_um',array('id_pengajuan_um'=>$rowUM['id']));
			if(!empty($kode_akun)){
				foreach ($this->input->post('kode_akun') as $key => $value) {
					# code...
					$datarab=array(
						'id_pengajuan_um'=>$rowUM['id'],
						'kode_akun'=>$value,
						'detil_belanja'=>$this->input->post('detil_belanja')[$key],
						'vol'=>$this->input->post('vol')[$key],
						'satuan'=>$this->input->post('satuan')[$key],
						'harga_satuan'=>$this->input->post('harga')[$key],
						'jumlah'=>$this->input->post('harga')[$key]*$this->input->post('vol')[$key]
						);
					$this->mum->createDetil($datarab);
				}

			}
			redirect('ppk');
		}
	}

	public function selesai(){
		$rowUM=$this->mum->get($this->encryption->decrypt($this->input->post('id')));
		if(empty($rowUM))show_404();
		$dataum=array('tag_evaluasi'=>2);
		$this->mum->update($dataum,$rowUM['id']);	

		$data=array(
			'id_pengajuan_um'=>$rowUM['id'],
			'id_user'=>$this->user['id']
			);
		$this->mppk->createEvaluasi($data);

		$datalog=array(
			'id_user'=>$rowUM['id_user'],
			'role'=>'Pemohon',
			'pesan'=>'Kegiatan '.$rowUM['kegiatan'].' Telah dinyatakan selesai'
			);
		$this->db->insert('pesan',$datalog);

		redirect('ppk');
	}

	public function addpengeluaran()
	{
		if ($this->auth->checkRole('PPK')===false)show_error('Anda Tidak Memiliki Akses Ke Halaman Ini');
		$data=array(
			'csrf' => array(
		        'name' => $this->security->get_csrf_token_name(),
		        'hash' => $this->security->get_csrf_hash()
			),
			'databidang'=>$this->db->get('bidang')->result_array(),
			);

		$this->load->view('header');
		$this->load->view('ppk/addpengeluaran',$data);
		$this->load->view('footer');			
	}

	public function editpengeluaran($id)
	{
		if ($this->auth->checkRole('PPK')===false)show_error('Anda Tidak Memiliki Akses Ke Halaman Ini');
		$data=array(
			'csrf' => array(
		        'name' => $this->security->get_csrf_token_name(),
		        'hash' => $this->security->get_csrf_hash()
			),
			'databidang'=>$this->db->get('bidang')->result_array(),
			'item'=>$this->mppk->getPengeluaran($id)
			);
		$this->load->view('header');
		$this->load->view('ppk/editpengeluaran',$data);
		$this->load->view('footer');			
	}

	public function createpengeluaran(){
		$this->form_validation->set_rules('kode_bidang', 'Bidang', 'required');
		$this->form_validation->set_rules('tanggal', 'Tanggal', array('required','exact_length[10]'));
		$this->form_validation->set_rules('uraian', 'Uraian', array('required'));
		$this->form_validation->set_rules('jumlah', 'Jumlah', array('required'));
		if ($this->form_validation->run() == FALSE)
		{
			$this->addpengeluaran();
		}else{
			$this->db->trans_start();
			$tanggal = date_create_from_format('d-m-Y', $this->input->post('tanggal'));
			if($this->input->post('jam')=="")$jam="00:00:00"; else $jam=$this->input->post('jam');
			$waktu=date_format($tanggal, 'Y-m-d');
			if($this->input->post('id_sub_komponen_kegiatan')==""){
				$id_kegiatan=$this->input->post('id_komponen_kegiatan');
			}else{
				$id_kegiatan=$this->input->post('id_sub_komponen_kegiatan');				
			}
			$data=array(
				'tahun_anggaran'=>$this->input->post('tahun_anggaran'),
				'kode_bidang'=>$this->input->post('kode_bidang'),
				'tanggal'=>$waktu,
				'uraian'=>$this->input->post('uraian'),
				'jumlah'=>$this->input->post('jumlah'),
				'id_user'=>$this->user['id']
				);
			$id_pengajuan_um=$this->mppk->createpengeluaran($data);

			$this->db->trans_complete();
			redirect('ppk/pengeluaran');
		}
	}

	public function updatepengeluaran(){
		$this->form_validation->set_rules('kode_bidang', 'Bidang', 'required');
		$this->form_validation->set_rules('tanggal', 'Tanggal', array('required','exact_length[10]'));
		$this->form_validation->set_rules('uraian', 'Uraian', array('required'));
		$this->form_validation->set_rules('jumlah', 'Jumlah', array('required'));
		$item=$this->mppk->getPengeluaran($this->input->post('id'));
		if ($this->form_validation->run() == FALSE)
		{
			$this->editpengeluaran($item['id']);
		}else{
			$this->db->trans_start();
			$tanggal = date_create_from_format('d-m-Y', $this->input->post('tanggal'));
			if($this->input->post('jam')=="")$jam="00:00:00"; else $jam=$this->input->post('jam');
			$waktu=date_format($tanggal, 'Y-m-d');
			if($this->input->post('id_sub_komponen_kegiatan')==""){
				$id_kegiatan=$this->input->post('id_komponen_kegiatan');
			}else{
				$id_kegiatan=$this->input->post('id_sub_komponen_kegiatan');				
			}
			$data=array(
				'tahun_anggaran'=>$this->input->post('tahun_anggaran'),
				'kode_bidang'=>$this->input->post('kode_bidang'),
				'tanggal'=>$waktu,
				'uraian'=>$this->input->post('uraian'),
				'jumlah'=>$this->input->post('jumlah'),
				'id_user'=>$this->user['id']
				);
			$id_pengajuan_um=$this->mppk->updatepengeluaran($data,$item['id']);

			$this->db->trans_complete();
			redirect('ppk/pengeluaran');
		}
	}

	public function hapuspengeluaran($id){
		$this->mppk->deletepengeluaran($id);
		redirect('ppk/pengeluaran');
	}
}
