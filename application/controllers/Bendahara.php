<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bendahara extends CI_Controller {
	public $user;
	public function  __construct(){
		parent::__construct();
		$this->user=$this->auth->getUser();
		$this->load->model(array('mum','mbendahara','mdipa','mkk'));
		$this->load->library('form_validation');
	}

	public function _remap($method,$param=array()){
	        if ($method === 'page'){
	                $this->index();
	        }else if ($method === 'search'){
	        	$data=array();
	        	if($this->input->post('id_kegiatan')!="")$data['kegiatan']=$this->input->post('id_kegiatan');
	        	if($this->input->post('waktu')!="")$data['waktu']=$this->input->post('waktu');
	        	if($this->input->post('lokasi')!="")$data['lokasi']=$this->input->post('lokasi');
				$str = $this->uri->assoc_to_uri($data);        		
	            redirect('bendahara/'.$str.'/page/1/');
	        }else if (method_exists($this, $method)){
                return call_user_func_array(array($this, $method),$param);
	        }else{
	            $this->index();	        	
	        }
	}

	public function index()
	{
		if ($this->auth->checkRole('BENDAHARA')===false)show_error('Anda Tidak Memiliki Akses Ke Halaman Ini');
		$default=array('page','kegiatan','waktu','lokasi');
		$search = $this->uri->uri_to_assoc(2,$default);
		$search['lokasi']=urldecode($search['lokasi']);
		$this->load->helper('date');
		$perPage=10;
		$offset=($search['page']*$perPage)-$perPage;
		$total_rows = $this->mum->getCountRows(['user'=>'','kegiatan'=>$search['kegiatan'],'waktu'=>convertDate($search['waktu'],'d-m-Y','Y-m-d'),'lokasi'=>$search['lokasi'],'tag_kpa'=>1,'tag_ppk'=>1]);


		$data=array(
			'csrf' => array(
		        'name' => $this->security->get_csrf_token_name(),
		        'hash' => $this->security->get_csrf_hash()
			),
			'offset'=>$offset,
			'search'=>$search,
			'datakegiatan'=>$this->mdipa->getByBidang(),
			'items'=>$this->mum->getAll(['user'=>'','kegiatan'=>$search['kegiatan'],'waktu'=>convertDate($search['waktu'],'d-m-Y','Y-m-d'),'lokasi'=>$search['lokasi'],'tag_kpa'=>1,'tag_ppk'=>1])
			);
		array_walk($data['items'],array($this,'_getStatusBendahara'));
		array_walk($data['items'],function(&$item){
			if($item['tag_ppk']==1)$item['status_ppk']="Lulus"; else $item['status_ppk']="Tidak Lulus";
			if($item['tag_kpa']==1)$item['status_kpa']="Disetujui"; else $item['status_kpa']="Tidak Disetujui";
			if($item['tag_spm']==1)$item['status_spm']="Lulus"; else $item['status_spm']="Tidak Lulus";
			if($item['tag_bendahara']==1)$item['status_bendahara']="Lulus"; else $item['status_bendahara']="Tidak Lulus";
			if($item['spm']){
				$item['spm_file']="<a href='".base_url('uploads/'.$item['spm_file'])."'>[Lihat SPM]</a>";
			}
			if($item['tanda_terima']){
				$item['tanda_terima_file']="<a href='".base_url('uploads/'.$item['tanda_terima_file'])."'>[Lihat Tanda Terima]</a>";
			}
			$item['jumlah']=$this->mum->getTotalDetilBelanja($item['id']);
		});
		unset($search['page']);
		$str = $this->uri->assoc_to_uri($search);        		
		$base_url = base_url()."bendahara/{$str}/page/";
		$data['paging']=$this->_createPaging($base_url,$total_rows,$perPage);		

		$this->load->view('header');
		$this->load->view('bendahara/index',$data);
		$this->load->view('footer');
	}

	public function _createPaging($base_url,$total_rows,$per_page=10){
		$this->load->library('pagination');
		$config['base_url'] = $base_url;
		$config['total_rows'] = $total_rows;
		$config['per_page'] = $per_page;
		$config['full_tag_open'] = '<nav><ul class="pagination">';
		$config['first_link'] = FALSE;
		$config['last_link'] = FALSE;
		$config['prev_tag_open'] = '<li>';
		$config['next_tag_open'] = '<li>';
		$config['num_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['next_tag_close'] = '</li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li><a>';
		$config['cur_tag_close'] = '</a></li>';
		$config['full_tag_close'] = '</ul></nsav>';
		$config['use_page_numbers'] = TRUE;
		$this->pagination->initialize($config);
		return $this->pagination->create_links();				
	}

	public function _getStatusBendahara(&$item){

		switch ($item['tag_bendahara']) {
			case 0:
				if($item['tag_revisi_bendahara']==0){
					$item['status']="Menunggu Persetujuan Bendahara";
				}else{
					$item['status']="Menunggu Persetujuan Bendahara (Sudah Revisi)";
				}
				$item['status_code']=0;
				break;
			case 1:
				$item['status']='<img src="'.base_url().'/assets/img/sf/sign-check.svg" width="24" height="24" alt="Lulus">';
				$item['status_code']=1;
				break;
			case 2:
				$item['status_code']=2;
				$catatan=$this->mbendahara->getCatatanTerbaru($item['id']);
				$item['status']='<img src="'.base_url().'/assets/img/sf/sign-error.svg" width="24" height="24" alt="Tidak Lulus"> <br/><button type="button" class="btn btn-sm btn-danger" data-toggle="popover" data-trigger="focus" data-placement="top" title="Catatan" data-content="'.$this->security->xss_clean($catatan['catatan']).'">[klik untuk melihat pesan]</button>';
				break;
			
			default:
				$item['status_code']=0;
				$item['status']="Menunggu Persetujuan Bendahara";
				break;
		}
	}

	public function modify($id)
	{
		if ($this->auth->checkRole('BENDAHARA')===false)show_error('Anda Tidak Memiliki Akses Ke Halaman Ini');

		$data=array(
			'csrf' => array(
		        'name' => $this->security->get_csrf_token_name(),
		        'hash' => $this->security->get_csrf_hash()
			),
			'id'=>$this->encryption->encrypt($id),
			'item'=>$this->mum->get($id),
			'dataoutput'=>$this->mdipa->getAllOutput(),
			'datarab'=>$this->mum->getDetil($id),
			'dataakun'=>$this->db->get('akun')->result_array(),
			'datakegiatan'=>$this->mkk->getAll($this->user['id'])
			);
		$data['kegiatan']=$this->mum->getTreeKegiatan($data['item']['id_kegiatan']);
		if(count($data['kegiatan'])==3){
			$data['datakomponenkegiatan']=$this->mdipa->getAllSubOutput($data['kegiatan'][0]['id_dipa']);
			$data['datakegiatan']=$this->mdipa->getAllSubOutput($data['kegiatan'][1]['id_dipa']);
		}
		if(count($data['kegiatan'])==4){
			$data['datasuboutput']=$this->mdipa->getAllSubOutput($data['kegiatan'][0]['id_dipa']);
			$data['datakomponenkegiatan']=$this->mdipa->getAllSubOutput($data['kegiatan'][1]['id_dipa']);
			$data['datakegiatan']=$this->mdipa->getAllSubOutput($data['kegiatan'][2]['id_dipa']);
		}

			list($data['item']['tanggal'],$data['item']['jam'])=explode(" ", $data['item']['waktu']);
		$this->load->view('header');
		$this->load->view('bendahara/modify',$data);
		$this->load->view('footer');
	}

	public function update(){
		$rowUM=$this->mum->get($this->encryption->decrypt($this->input->post('id')));
		if(empty($rowUM))show_404();
		$this->form_validation->set_rules('id_output', 'Output', 'required');
		if($this->input->post('id_komponen_kegiatan')=="" && $this->input->post('id_sub_komponen_kegiatan')==""){
			$this->form_validation->set_rules('id_output', 'Komponen Kegiatan', 'required');			
		}
		$this->form_validation->set_rules('tahun_anggaran', 'Tahun', 'required|exact_length[4]');
		$this->form_validation->set_rules('tanggal', 'Tanggal', array('required','exact_length[10]'));
		$this->form_validation->set_rules('jam', 'Jam', array('exact_length[8]'));
		$this->form_validation->set_rules('lokasi', 'Lokasi', array('required'));

		if ($this->form_validation->run() == FALSE)
		{
			$this->modify($rowUM['id']);
		}else{
			$tanggal = date_create_from_format('d-m-Y', $this->input->post('tanggal'));
			if($this->input->post('jam')=="")$jam="00:00:00"; else $jam=$this->input->post('jam');
			$waktu=date_format($tanggal, 'Y-m-d').' '.$jam;
			if($this->input->post('id_sub_komponen_kegiatan')==""){
				$id_kegiatan=$this->input->post('id_komponen_kegiatan');
			}else{
				$id_kegiatan=$this->input->post('id_sub_komponen_kegiatan');				
			}
			$data=array(
				'tahun_anggaran'=>$this->input->post('tahun_anggaran'),
				'id_kalender_kegiatan'=>$this->input->post('id_kalender_kegiatan'),
				'id_kegiatan'=>$id_kegiatan,
				'waktu'=>$waktu,
				'lokasi'=>$this->input->post('lokasi'),
				'jumlah_peserta'=>$this->input->post('jumlah_peserta'),
				'kode_mata_anggaran'=>$this->input->post('kode_mata_anggaran'),
				);
			$this->mum->update($data,$rowUM['id']);
			$kode_akun=$this->input->post('kode_akun');
			$this->db->delete('detil_pengajuan_um',array('id_pengajuan_um'=>$rowUM['id']));
			if(!empty($kode_akun)){
				foreach ($this->input->post('kode_akun') as $key => $value) {
					# code...
					$datarab=array(
						'id_pengajuan_um'=>$rowUM['id'],
						'kode_akun'=>$value,
						'detil_belanja'=>$this->input->post('detil_belanja')[$key],
						'vol'=>$this->input->post('vol')[$key],
						'satuan'=>$this->input->post('satuan')[$key],
						'harga_satuan'=>$this->input->post('harga')[$key],
						'jumlah'=>$this->input->post('harga')[$key]*$this->input->post('vol')[$key]
						);
					$this->mum->createDetil($datarab);
				}

			}
			redirect('bendahara');
		}
	}

	public function detail($id)
	{
		if ($this->auth->checkRole('BENDAHARA')===false)show_error('Anda Tidak Memiliki Akses Ke Halaman Ini');

		$data=array(
			'csrf' => array(
		        'name' => $this->security->get_csrf_token_name(),
		        'hash' => $this->security->get_csrf_hash()
			),
			'datakegiatan'=>$this->mdipa->getByBidang(),
			'id'=>$this->encryption->encrypt($id),
			'datarab'=>$this->mum->getDetil($id),
			'dataakun'=>$this->db->get('akun')->result_array(),
			'item'=>$this->mum->get($id)
			);
			list($data['item']['tanggal'],$data['item']['jam'])=explode(" ", $data['item']['waktu']);
		$this->load->view('header');
		$this->load->view('bendahara/detail',$data);
		$this->load->view('footer');
	}

	public function kwitansi($id)
	{
		if ($this->auth->checkRole('BENDAHARA')===false)show_error('Anda Tidak Memiliki Akses Ke Halaman Ini');

		$data=array(
			'csrf' => array(
		        'name' => $this->security->get_csrf_token_name(),
		        'hash' => $this->security->get_csrf_hash()
			),
			'datakegiatan'=>$this->mdipa->getByBidang(),
			'id'=>$this->encryption->encrypt($id),
			'datarab'=>$this->mum->getDetil($id),
			'dataakun'=>$this->db->get('akun')->result_array(),
			'item'=>$this->mum->get($id),
			'datappk'=>$this->db->get_where('users',array('role'=>'PPK'))->result_array()
			);
		list($data['item']['tanggal'],$data['item']['jam'])=explode(" ", $data['item']['waktu']);
		$this->load->view('header');
		$this->load->view('bendahara/kwitansi',$data);
		$this->load->view('footer');
	}

	public function uploadtandaterima($id,$id_tanda_terima="")
	{
		if ($this->auth->checkRole('BENDAHARA')===false)show_error('Anda Tidak Memiliki Akses Ke Halaman Ini');

		$data=array(
			'csrf' => array(
		        'name' => $this->security->get_csrf_token_name(),
		        'hash' => $this->security->get_csrf_hash()
			),
			'id'=>$this->encryption->encrypt($id),
			'id_tanda_terima'=>$this->encryption->encrypt($id_tanda_terima),
			'item'=>$this->mum->get($id)
			);
		$this->load->view('header');
		$this->load->view('bendahara/tandaterima',$data);
		$this->load->view('footer');
	}

	public function upload(){
		$rowUM=$this->mum->get($this->encryption->decrypt($this->input->post('id')));
		$rowBendahara=$this->mbendahara->get($this->encryption->decrypt($this->input->post('id_tanda_terima')));
		if(empty($rowUM))show_404();
        $config['upload_path']          = './uploads/';
        $config['allowed_types']        = 'gif|jpg|png|pdf';
        $config['file_ext_tolower']		= TRUE;
        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('userfile')){
				$_SESSION['error_upload'] = $this->upload->display_errors("","");
				$this->session->mark_as_flash('error_upload');
                $this->detail($rowUM['id']);
        }
        else{
        		if(!empty($rowBendahara)){
	                $upload_data = $this->upload->data();
					$data=array(
						'id_pengajuan_um'=>$rowUM['id'],
						'tanda_terima'=>$upload_data['file_name'],
						'id_user'=>$this->user['id']
						);
					$this->mbendahara->update($data,$rowBendahara['id']);
					unlink('uploads/'.$rowBendahara['tanda_terima']);
					$dataum=array('tag_tanda_terima'=>1);
					$this->mum->update($dataum,$rowUM['id']);			
					redirect('bendahara');        			
        		}else{
	                $upload_data = $this->upload->data();
					$data=array(
						'id_pengajuan_um'=>$rowUM['id'],
						'tanda_terima'=>$upload_data['file_name'],
						'id_user'=>$this->user['id']
						);
					$this->mbendahara->create($data);
					$dataum=array('tag_tanda_terima'=>1);
					$this->mum->update($dataum,$rowUM['id']);			
					redirect('bendahara');        			
        		}
			$datalog=array(
				'id_user'=>$rowUM['id_user'],
				'role'=>'Pemohon',
				'pesan'=>'Bendahara Telah Mengupload Tanda Terima'
				);
			$this->db->insert('pesan',$datalog);
        }
	}

	public function approval(){
		$rowUM=$this->mum->get($this->encryption->decrypt($this->input->post('id')));
		if(empty($rowUM))show_404();
		if($this->input->post('status')==2){
			$this->form_validation->set_rules('catatan', 'Catatan', 'required');
		}else{
			$this->form_validation->set_rules('catatan', 'Catatan','max_length[10000]');			
		}
		if ($this->form_validation->run() == FALSE)
		{
			$this->detail($rowUM['id']);
		}else{
			$data=array(
				'id_pengajuan_um'=>$rowUM['id'],
				'status'=>$this->input->post('status'),
				'catatan'=>$this->input->post('catatan'),
				'id_user'=>$this->user['id']
				);
			$this->mbendahara->createbendahara($data);
			$dataum=array('tag_bendahara'=>$this->input->post('status'));
			$this->mum->update($dataum,$rowUM['id']);	

			if($this->input->post('status')==1){
				$pesan="Pengajuan UM untuk Kegiatan ".$rowUM['kegiatan']." Lulus oleh Bendahara";
			}else{
				$pesan="Pengajuan UM untuk Kegiatan ".$rowUM['kegiatan']." Tidak Lulus oleh Bendahara";				
			}
			$datalog=array(
				'id_user'=>$rowUM['id_user'],
				'role'=>'Pemohon',
				'pesan'=>$pesan
				);
			$this->db->insert('pesan',$datalog);

			redirect('bendahara');
		}
	}

	public function simpankwitansi(){
		$rowUM=$this->mum->get($this->encryption->decrypt($this->input->post('id')));
		if(empty($rowUM))show_404();
		$this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
		$this->form_validation->set_rules('tanggal_disetujui', 'Tanggal', 'required');
		$this->form_validation->set_rules('tanggal_diterima', 'Tanggal', 'required');
		$this->form_validation->set_rules('nomor', 'Nomor', 'required');
		$this->form_validation->set_rules('kepada', 'Kepada', 'required');
		$this->form_validation->set_rules('untuk_pembayaran', 'Untuk Pembayaran', 'required');
		$this->form_validation->set_rules('jumlah', 'Jumlah', 'required');
		$this->form_validation->set_rules('ppk', 'PPK', 'required');
		$this->form_validation->set_rules('bendahara', 'Bendahara', 'required');
		if ($this->form_validation->run() == FALSE)
		{
			$this->kwitansi($rowUM['id']);
		}else{
			$tanggal = date_create_from_format('d-m-Y', $this->input->post('tanggal'));
			$tanggal=date_format($tanggal, 'Y-m-d');
			$tanggal_disetujui = date_create_from_format('d-m-Y', $this->input->post('tanggal_disetujui'));
			$tanggal_disetujui=date_format($tanggal_disetujui, 'Y-m-d');
			$tanggal_diterima = date_create_from_format('d-m-Y', $this->input->post('tanggal_diterima'));
			$tanggal_diterima=date_format($tanggal_diterima, 'Y-m-d');
			$data=array(
				'id_pengajuan_um'=>$rowUM['id'],
				'tanggal_surat'=>$tanggal,
				'tanggal_disetujui'=>$tanggal_disetujui,
				'tanggal_diterima'=>$tanggal_diterima,
				'nomor'=>$this->input->post('nomor'),
				'jumlah'=>$this->input->post('jumlah'),
				'kepada'=>$this->input->post('kepada'),
				'untuk_pembayaran'=>$this->input->post('untuk_pembayaran'),
				'bendahara'=>$this->input->post('bendahara'),
				'penerima'=>$this->input->post('kepada'),
				'ppk'=>$this->input->post('ppk'),
				'id_user'=>$this->user['id']
				);
			$this->db->insert('kwitansi',$data);
			$id=$this->db->insert_id();
			redirect('bendahara/cetakkwitansi/'.$id);
		}
	}

	public function cetakkwitansi($id){
        		$item=$this->mbendahara->getKwitansi($id);
                $file=VIEWPATH."bendahara/formatkwintansi.pdf";
                //$file=VIEWPATH."formatpdf/surat_permohonan.pdf";
                $config=array('file'=>$file);
                $this->load->library('MyPDFI',$config);
                $this->mypdfi->AddPage();
                $this->mypdfi->SetFont('Arial','',10);
                $this->mypdfi->text(82,42,$item['tanggal_surat']);
                $this->mypdfi->text(130,42,$item['nomor']);
                $this->mypdfi->SetFont('Arial','B',10);
                $this->mypdfi->text(7,60,"Rp. ".number_format($item['jumlah'], 0, "", "."));
                $this->mypdfi->SetFont('Arial','',10);                
                $this->mypdfi->text(37,85,": ".$item['kepada']);
                $this->mypdfi->text(37,89,": ".$item['untuk_pembayaran']);

                $this->mypdfi->text(51,149.5,$item['tanggal_disetujui']);
                $this->mypdfi->text(101,149.5,$item['tanggal_diterima']);
                $this->mypdfi->text(154,144.5,$item['tanggal_diterima']);

                $this->mypdfi->text(7,175,$item['bendahara']);
                $this->mypdfi->text(7,180,"NIP.".$item['nip_bendahara']);
                $this->mypdfi->text(75,175,$item['penerima']);
                $this->mypdfi->text(75,180,"NIP.".$item['nip_penerima']);
                $this->mypdfi->text(142,175,$item['ppk']);
                $this->mypdfi->text(142,180,"NIP.".$item['nip_ppk']);
	            $this->mypdfi->Output();                
	}

}
