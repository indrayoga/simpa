<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Realisasi extends CI_Controller {
	public $user;
	public function  __construct(){
		parent::__construct();
		$this->user=$this->auth->getUser();
		$this->load->model(array('mlaporan','mdipa'));
		$this->load->library('form_validation');
	}

	public function index()
	{
		//if ($this->auth->checkRole('Pemohon')===false)show_error('Anda Tidak Memiliki Akses Ke Halaman Ini');
		if($this->user['seksi']=="-")$this->user['seksi']="";
		if($this->input->post('tahun')!="")$tahun=$this->input->post('tahun'); else $tahun=date('Y');
		if($this->input->post('bulan')!="")$bulan=$this->input->post('bulan'); else $bulan=date('m');
		$data=array(
			'csrf' => array(
		        'name' => $this->security->get_csrf_token_name(),
		        'hash' => $this->security->get_csrf_hash()
			),
			'datakegiatan'=>$this->mdipa->getByBidang(),
			'tahun'=>$tahun,
			'bulan'=>$bulan,
			'items'=>$this->mlaporan->getRealisasi($bulan,$tahun)
			);
		$this->load->view('admin/header');
		$this->load->view('admin/laporan/realisasi',$data);
		$this->load->view('footer');
	}
}
