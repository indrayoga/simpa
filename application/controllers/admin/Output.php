<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Output extends CI_Controller {
	public function  __construct(){
		parent::__construct();
		$this->load->model(array('moutput','mbidang'));
		$this->load->library('form_validation');
	}

	public function index()
	{
		if ($this->auth->check('admin#output')===false)show_error('Anda Tidak Memiliki Akses Ke Halaman Ini');

		$data=array(
			'items'=>$this->moutput->getAll()
			);
		$this->load->view('admin/header');
		$this->load->view('admin/output/index',$data);
		$this->load->view('admin/footer');
	}

	public function add()
	{
		if ($this->auth->check('admin#output#add')===false)show_error('Anda Tidak Memiliki Akses Ke Halaman Ini');

		$data=array(
			'csrf' => array(
		        'name' => $this->security->get_csrf_token_name(),
		        'hash' => $this->security->get_csrf_hash()
			),
			'databidang' => $this->mbidang->getAll()			
		);

		$this->load->view('admin/header');
		$this->load->view('admin/output/new',$data);
		$this->load->view('admin/footer');
	}

	public function modify($id)
	{
		if ($this->auth->check('admin#output#modify')===false)show_error('Anda Tidak Memiliki Akses Ke Halaman Ini');

		$data=array(
			'csrf' => array(
		        'name' => $this->security->get_csrf_token_name(),
		        'hash' => $this->security->get_csrf_hash()
			),
			'id'=>$this->encryption->encrypt($id),
			'item'=>$this->moutput->get($id),
			'databidang' => $this->mbidang->getAll()			
			);
		$this->load->view('admin/header');
		$this->load->view('admin/output/modify',$data);
		$this->load->view('admin/footer');
	}

	public function create(){
		$this->form_validation->set_rules('tahun_anggaran', 'Tahun Anggaran', array('required','exact_length[4]','numeric'));
		$this->form_validation->set_rules('kode_bidang', 'Bidang', array('required'));
		$this->form_validation->set_rules('kode_kegiatan', 'Kode', array('required'));
		$this->form_validation->set_rules('kegiatan', 'Kegiatan', array('required','min_length[1]','max_length[255]','is_unique[dipa.kegiatan]'));
		if ($this->form_validation->run() == FALSE)
		{
			$this->add();
		}else{
			$data=array(
				'tahun_anggaran'=>$this->input->post('tahun_anggaran'),
				'kode_bidang'=>$this->input->post('kode_bidang'),
				'kode_kegiatan'=>$this->input->post('kode_kegiatan'),
				'kegiatan'=>$this->input->post('kegiatan')
				);
			$this->moutput->create($data);
			redirect('admin/output');
		}
	}

	public function update(){
		$rowDipa=$this->moutput->get($this->encryption->decrypt($this->input->post('id')));
		if(empty($rowDipa))show_404();
		$this->form_validation->set_rules('tahun_anggaran', 'Tahun Anggaran', array('required','exact_length[4]','numeric'));
		$this->form_validation->set_rules('kode_bidang', 'Bidang', array('required'));
		$this->form_validation->set_rules('kode_kegiatan', 'Kode', array('required'));
		$this->form_validation->set_rules('kegiatan', 'Kegiatan', array('required','min_length[1]','max_length[255]'));
		if ($this->form_validation->run() == FALSE)
		{
			$this->modify($rowDipa['id_dipa']);
		}else{
			$data=array(
				'tahun_anggaran'=>$this->input->post('tahun_anggaran'),
				'kode_bidang'=>$this->input->post('kode_bidang'),
				'kode_kegiatan'=>$this->input->post('kode_kegiatan'),
				'kegiatan'=>$this->input->post('kegiatan')
				);
			$this->moutput->update($data,$rowDipa['id_dipa']);
			redirect('admin/output');
		}
	}

	public function delete($id)
	{
		if(empty($id) )return show_404();
		$datauser = $this->moutput->delete($id);			
		redirect('admin/output');		
	}


}
