<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Akun extends CI_Controller {
	public function  __construct(){
		parent::__construct();
		$this->load->model('makun');
		$this->load->library('form_validation');
	}

	public function index()
	{
		if ($this->auth->check('admin#akun')===false)show_error('Anda Tidak Memiliki Akses Ke Halaman Ini');

		$data=array(
			'items'=>$this->makun->getAll()
			);
		$this->load->view('admin/header');
		$this->load->view('admin/akun/index',$data);
		$this->load->view('admin/footer');
	}

	public function add()
	{
		if ($this->auth->check('admin#akun#add')===false)show_error('Anda Tidak Memiliki Akses Ke Halaman Ini');

		$data=array(
			'csrf' => array(
		        'name' => $this->security->get_csrf_token_name(),
		        'hash' => $this->security->get_csrf_hash()
			)
			);

		$this->load->view('admin/header');
		$this->load->view('admin/akun/new',$data);
		$this->load->view('admin/footer');
	}

	public function modify($kode_akun)
	{
		if ($this->auth->check('admin#akun#modify')===false)show_error('Anda Tidak Memiliki Akses Ke Halaman Ini');

		$data=array(
			'csrf' => array(
		        'name' => $this->security->get_csrf_token_name(),
		        'hash' => $this->security->get_csrf_hash()
			),
			'item'=>$this->makun->get($kode_akun)
			);
		$this->load->view('admin/header');
		$this->load->view('admin/akun/modify',$data);
		$this->load->view('admin/footer');
	}

	public function create(){
		$this->form_validation->set_rules('kode_akun', 'Kode Akun', array('is_unique[akun.kode_akun]','required'),array('is_unique'=>'Data Sudah Ada'));
		$this->form_validation->set_rules('nama_akun', 'Nama Akun', array('required'));
		if ($this->form_validation->run() == FALSE)
		{
			$this->add();
		}else{
			$this->makun->create(array('kode_akun'=>$this->input->post('kode_akun'),'nama_akun'=>$this->input->post('nama_akun')));
			redirect('admin/akun');
		}
	}

	public function update(){
		$this->form_validation->set_rules('kode_akun', 'Kode Akun', array('required',array('valid_uniqe_kode_akun',function($value){
				$query=$this->db->query('select * from akun where kode_akun="'.$value.'" and kode_akun !="'.$this->input->post('kode_akun_lama').'" ');
				$item=$query->row_array();
				if(empty($item))return true;
				return false;
			})),array('is_unique'=>'Data Sudah Ada','valid_uniqe_kode_akun'=>'Kode Akun sudah digunakan'));
		$this->form_validation->set_rules('nama_akun', 'Nama Akun', array('required'));
		if ($this->form_validation->run() == FALSE)
		{
			$this->modify($this->input->post('kode_akun'));
		}else{
			$this->makun->update(array('kode_akun'=>$this->input->post('kode_akun'),'nama_akun'=>$this->input->post('nama_akun')),$this->input->post('kode_akun'));
			redirect('admin/akun');
		}
	}

	public function delete($kode_akun)
	{
		if(empty($kode_akun) )return show_404();
		$datauser = $this->makun->delete($kode_akun);			
		redirect('admin/akun');		
	}


}
