<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dipa extends CI_Controller {
	public function  __construct(){
		parent::__construct();
		$this->load->model(array('mdipa','mbidang'));
		$this->load->library('form_validation');
	}

	public function index()
	{
		if ($this->auth->check('admin#akses')===false)show_error('Anda Tidak Memiliki Akses Ke Halaman Ini');

		$data=array(
			'items'=>$this->mdipa->getAll()
			);
		$this->load->view('admin/header');
		$this->load->view('admin/dipa/index',$data);
		$this->load->view('admin/footer');
	}

	public function add($id)
	{
		if ($this->auth->check('admin#dipa#add')===false)show_error('Anda Tidak Memiliki Akses Ke Halaman Ini');

		$data=array(
			'csrf' => array(
		        'name' => $this->security->get_csrf_token_name(),
		        'hash' => $this->security->get_csrf_hash()
			),
			'id'=>$this->encryption->encrypt($id),
			'item'=>$this->mdipa->get($id),
			'databidang' => $this->mbidang->getAll()			
			);
		$this->load->view('admin/header');
		$this->load->view('admin/dipa/new',$data);
		$this->load->view('admin/footer');
	}

	public function append($id)
	{
		if ($this->auth->check('admin#dipa#append')===false)show_error('Anda Tidak Memiliki Akses Ke Halaman Ini');
		$data=array(
			'csrf' => array(
		        'name' => $this->security->get_csrf_token_name(),
		        'hash' => $this->security->get_csrf_hash()
			),
			'id'=>$this->encryption->encrypt($id),
			'item'=>$this->mdipa->get($id),
			'databidang' => $this->mbidang->getAll()			
			);
		$this->load->view('admin/header');
		$this->load->view('admin/dipa/append',$data);
		$this->load->view('admin/footer');
	}

	public function modify($id)
	{
		if ($this->auth->check('admin#suboutput#modify')===false)show_error('Anda Tidak Memiliki Akses Ke Halaman Ini');

		$data=array(
			'csrf' => array(
		        'name' => $this->security->get_csrf_token_name(),
		        'hash' => $this->security->get_csrf_hash()
			),
			'id'=>$this->encryption->encrypt($id),
			'item'=>$this->mdipa->get($id),
			'databidang' => $this->mbidang->getAll()			
			);
		$this->load->view('admin/header');
		$this->load->view('admin/dipa/modify',$data);
		$this->load->view('admin/footer');
	}

	public function create(){
		$rowDipa=$this->mdipa->get($this->encryption->decrypt($this->input->post('id')));
		if(empty($rowDipa))show_404();
		$this->form_validation->set_rules('tahun_anggaran', 'Tahun Anggaran', array('required','exact_length[4]','numeric'));
		$this->form_validation->set_rules('kode_bidang', 'Bidang', array('required'));
		$this->form_validation->set_rules('kode_kegiatan', 'Kode', array('required'));
		$this->form_validation->set_rules('kegiatan', 'Kegiatan', array('required','min_length[1]','max_length[255]','is_unique[dipa.kegiatan]'));
		if ($this->form_validation->run() == FALSE)
		{
			$this->add($rowDipa['id_dipa']);
		}else{
			$data=array(
				'tahun_anggaran'=>$this->input->post('tahun_anggaran'),
				'kode_bidang'=>$this->input->post('kode_bidang'),
				'kode_kegiatan'=>$this->input->post('kode_kegiatan'),
				'kegiatan'=>$this->input->post('kegiatan'),
				'tipe'=>$this->input->post('tipe')
				);
			$this->mdipa->create($data,$rowDipa['id_dipa']);
			redirect('admin/dipa');
		}
	}

	public function addappend(){
		$rowDipa=$this->mdipa->get($this->encryption->decrypt($this->input->post('id')));
		if(empty($rowDipa))show_404();
		$this->form_validation->set_rules('tahun_anggaran', 'Tahun Anggaran', array('required','exact_length[4]','numeric'));
		$this->form_validation->set_rules('kode_bidang', 'Bidang', array('required'));
		$this->form_validation->set_rules('kode_kegiatan', 'Kode', array('required'));
		$this->form_validation->set_rules('kegiatan', 'Kegiatan', array('required','min_length[1]','max_length[255]','is_unique[dipa.kegiatan]'));
		if ($this->form_validation->run() == FALSE)
		{
			$this->append($rowDipa['id_dipa']);
		}else{
			$data=array(
				'tahun_anggaran'=>$this->input->post('tahun_anggaran'),
				'kode_bidang'=>$this->input->post('kode_bidang'),
				'kode_kegiatan'=>$this->input->post('kode_kegiatan'),
				'kegiatan'=>$this->input->post('kegiatan'),
				'tipe'=>$this->input->post('tipe')
				);
			$this->mdipa->append($data,$rowDipa['id_dipa']);
			redirect('admin/dipa');
		}
	}

	public function update(){
		$rowDipa=$this->mdipa->get($this->encryption->decrypt($this->input->post('id')));
		if(empty($rowDipa))show_404();
		$this->form_validation->set_rules('tahun_anggaran', 'Tahun Anggaran', array('required','exact_length[4]','numeric'));
		$this->form_validation->set_rules('kode_bidang', 'Bidang', array('required'));
		$this->form_validation->set_rules('kode_kegiatan', 'Kode', array('required'));
		$this->form_validation->set_rules('kegiatan', 'Kegiatan', array('required','min_length[1]','max_length[255]'));
		if ($this->form_validation->run() == FALSE)
		{
			$this->modify($rowDipa['id_dipa']);
		}else{
			$data=array(
				'tahun_anggaran'=>$this->input->post('tahun_anggaran'),
				'kode_bidang'=>$this->input->post('kode_bidang'),
				'kode_kegiatan'=>$this->input->post('kode_kegiatan'),
				'kegiatan'=>$this->input->post('kegiatan'),
				'tipe'=>$this->input->post('tipe')
				);
			$this->mdipa->update($data,$rowDipa['id_dipa']);
			redirect('admin/dipa');
		}
	}

	public function delete($id)
	{
		if(empty($id) )return show_404();
		$datauser = $this->mdipa->delete($id);			
		redirect('admin/dipa');		
	}

}
