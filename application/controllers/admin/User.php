<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('form_validation','auth','userdb'));		
	}
	
	public function index()
	{
		if ($this->auth->check('admin#user')===false)show_error('Anda Tidak Memiliki Akses Ke Halaman Ini');

		$data=array(
			'items'=>$this->userdb->getAll()
			);

		$this->load->view('admin/header');
		$this->load->view('admin/user/index',$data);
		$this->load->view('admin/footer');
	}

	public function add()
	{
		if ($this->auth->check('admin#user#add')===false)show_error('Anda Tidak Memiliki Akses Ke Halaman Ini');
		$data=array(
			'csrf' => array(
		        'name' => $this->security->get_csrf_token_name(),
		        'hash' => $this->security->get_csrf_hash()
			)
		);

		$this->load->view('admin/header');
		$this->load->view('admin/user/new',$data);
		$this->load->view('admin/footer');

	}

	public function modify($id)
	{
		if ($this->auth->check('admin#user#modify')===false)show_error('Anda Tidak Memiliki Akses Ke Halaman Ini');
		$data=array(
			'csrf' => array(
		        'name' => $this->security->get_csrf_token_name(),
		        'hash' => $this->security->get_csrf_hash()
			),
			'id'=>$this->encryption->encrypt($id),
			'user'=>$this->userdb->getById($id)
		);
		$this->load->view('admin/header');
		$this->load->view('admin/user/modify',$data);
		$this->load->view('admin/footer');

	}

	public function create()
	{
		if ($this->auth->check('admin#user#add')===false)show_error('Anda Tidak Memiliki Akses Ke Halaman Ini');

		$this->form_validation->set_rules('username', 'Username', array('required','min_length[1]','max_length[100]','alpha_numeric',array('valid_username',function($value){
				$query=$this->db->query('select * from users where username="'.$value.'"');
				$item=$query->row_array();
				if(empty($item))return true;
				return false;
			})),
			array('alpha_numeric'=>'Karakter yang diperbolehkan hanya huruf dan angka, tanpa spasi',
					'valid_username'=>'Username sudah digunakan')
			);

		$this->form_validation->set_rules('email', 'Email', array('required','min_length[1]','max_length[150]','valid_email',array('valid_uniqe_email',function($value){
				$query=$this->db->query('select * from users where email="'.$value.'"');
				$item=$query->row_array();
				if(empty($item))return true;
				return false;
			})),
			array('valid_uniqe_email'=>'Email sudah digunakan','valid_email'=>'Email Tidak Valid')
			);

		$this->form_validation->set_rules('password', 'Password','required');
		$this->form_validation->set_rules('role', 'Role','required');
		$this->form_validation->set_rules('no_hp', 'No HP','required');

		if ($this->form_validation->run() == FALSE)
		{
			$this->add();
		}
		else
		{
			if(empty($this->input->post('is_admin')))$is_admin=0;else $is_admin=1;
			if($this->input->post('role')!='Pemohon'){
				$seksi="-";
			}else{
				$seksi=$this->input->post('seksi');
			}
			$data=array(
				'surname'=>$this->input->post('surname'),
				'nip'=>$this->input->post('nip'),
				'username'=>$this->input->post('username'),
				'email'=>$this->input->post('email'),
				'no_hp'=>$this->input->post('no_hp'),
				'password'=>$this->input->post('password'),
				'role'=>$this->input->post('role'),
				'seksi'=>$seksi,
				'is_admin'=>$is_admin
				);
			$user = $this->userdb->create($data);			
			redirect('admin/user');
		}

	}

	public function update()
	{

		if ($this->auth->check('admin#user#modify')===false)show_error('Anda Tidak Memiliki Akses Ke Halaman Ini');
		$user=$this->userdb->getById($this->encryption->decrypt($this->input->post('id')));
		if(empty($user))show_404();

		$this->form_validation->set_rules('username', 'Username', array('required','min_length[1]','max_length[100]','alpha_numeric',array('valid_username',function($value) use ($user){
				$query=$this->db->query('select * from users where username="'.$value.'" and id !="'.$user['id'].'" ');
				$item=$query->row_array();
				if(empty($item))return true;
				return false;
			})),
			array('alpha_numeric'=>'Karakter yang diperbolehkan hanya huruf dan angka, tanpa spasi',
					'valid_username'=>'Username sudah ada')
			);

		$this->form_validation->set_rules('email', 'Email', array('required','min_length[1]','max_length[100]','valid_email',array('valid_uniqe_email',function($value) use($user) {
				$query=$this->db->query('select * from users where email="'.$value.'" and id !="'.$user['id'].'"');
				$item=$query->row_array();
				if(empty($item))return true;
				return false;
			})),
			array('valid_uniqe_email'=>'Email sudah digunakan','valid_email'=>'Email Tidak Valid')
			);

		$this->form_validation->set_rules('role', 'Role','required');
		$this->form_validation->set_rules('no_hp', 'No HP','required');

		if ($this->form_validation->run() == FALSE)
		{
			$this->modify($user['id']);
		}
		else
		{
			if(empty($this->input->post('is_admin')))$is_admin=0;else $is_admin=1;
			if($this->input->post('role')!='Pemohon'){
				$seksi="-";
			}else{
				$seksi=$this->input->post('seksi');
			}
			$data=array(
				'surname'=>$this->input->post('surname'),
				'username'=>$this->input->post('username'),
				'nip'=>$this->input->post('nip'),
				'email'=>$this->input->post('email'),
				'no_hp'=>$this->input->post('no_hp'),
				'role'=>$this->input->post('role'),
				'seksi'=>$seksi,
				'is_admin'=>$is_admin
				);
			if(!empty($this->input->post('password')))$data['password']=$this->input->post('password');
			$user = $this->userdb->update($data,$user['id']);			
			redirect('admin/user');
		}

	}

	public function delete($kodeUser)
	{
		if(empty($kodeUser) )return show_404();
		$datauser = $this->userdb->deleteById($kodeUser);			
		redirect('admin/user');		
	}

	public function block($kodeUser,$type=1)
	{
		if(empty($kodeUser) )return show_404();
		if($type){			
			$datauser = $this->userdb->blockById($kodeUser);			
		}else{
			$datauser = $this->userdb->activatedById($kodeUser);			
		}
		redirect('admin/user');		
	}

}
