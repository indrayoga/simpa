<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bidang extends CI_Controller {
	public function  __construct(){
		parent::__construct();
		$this->load->model('mbidang');
		$this->load->library('form_validation');
	}

	public function index()
	{
		if ($this->auth->check('admin#bidang')===false)show_error('Anda Tidak Memiliki Akses Ke Halaman Ini');

		$data=array(
			'items'=>$this->mbidang->getAll()
			);
		$this->load->view('admin/header');
		$this->load->view('admin/bidang/index',$data);
		$this->load->view('admin/footer');
	}

	public function add()
	{
		if ($this->auth->check('admin#akses#add')===false)show_error('Anda Tidak Memiliki Akses Ke Halaman Ini');

		$data=array(
			'csrf' => array(
		        'name' => $this->security->get_csrf_token_name(),
		        'hash' => $this->security->get_csrf_hash()
			),
			'datamenu'=>$this->mmenu->getAll()
			);

		$this->load->view('admin/header');
		$this->load->view('admin/akses/new',$data);
		$this->load->view('admin/footer');
	}

	public function modify($akses)
	{
		$akses=str_replace("-", "#", $akses);
		if ($this->auth->check('admin#akses#modify')===false)show_error('Anda Tidak Memiliki Akses Ke Halaman Ini');

		$data=array(
			'csrf' => array(
		        'name' => $this->security->get_csrf_token_name(),
		        'hash' => $this->security->get_csrf_hash()
			),
			'id'=>$this->encryption->encrypt($akses),
			'item'=>$this->makses->get($akses),
			'datamenu'=>$this->mmenu->getAll()
			);
		$this->load->view('admin/header');
		$this->load->view('admin/akses/modify',$data);
		$this->load->view('admin/footer');
	}

	public function create(){
		$this->form_validation->set_rules('access', 'Akses', array('is_unique[permissions.access]','required','min_length[1]','max_length[255]'),array('is_unique'=>'Data Sudah Ada'));
		if ($this->form_validation->run() == FALSE)
		{
			$this->add();
		}else{
			$this->makses->create(array('access'=>$this->input->post('access'),'idmenu'=>$this->input->post('idmenu')));
			redirect('admin/akses');
		}
	}

	public function update(){
		$rowAccess=$this->makses->get($this->encryption->decrypt($this->input->post('id')));
		if(empty($rowAccess))show_404();
		$this->form_validation->set_rules('access', 'Akses', array('required','min_length[1]','max_length[255]',array('is_unique_access',function($value) use ($rowAccess){
				$query=$this->db->query('select * from permissions where access="'.$value.'" and access!="'.$rowAccess['access'].'"');
				$item=$query->row_array();
				if(empty($item))return true;
				return false;
			})),array('is_unique_access','Data Sudah Ada'));
		if ($this->form_validation->run() == FALSE)
		{
			$this->modify($rowAccess['access']);
		}else{
			$this->makses->update(array('access'=>$this->input->post('access'),'idmenu'=>$this->input->post('idmenu')),$rowAccess['access']);
			redirect('admin/akses');
		}
	}

	public function delete($access)
	{
		if(empty($access) )return show_404();
		$access=str_replace("-", "#", $access);
		$datauser = $this->makses->delete($access);			
		redirect('admin/akses');		
	}


}
