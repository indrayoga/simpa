<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller {
	public function  __construct(){
		parent::__construct();
		$this->load->model(array('mmenu'));
		$this->load->library('form_validation');
	}

	public function index()
	{
		if ($this->auth->check('admin#menu')===false)show_error('Anda Tidak Memiliki Akses Ke Halaman Ini');

		$data=array(
			'items'=>$this->mmenu->getAll()
			);

		$this->load->view('admin/header');
		$this->load->view('admin/menu/index',$data);
		$this->load->view('admin/footer');
	}

	public function add()
	{
		if ($this->auth->check('admin#menu#add')===false)show_error('Anda Tidak Memiliki Akses Ke Halaman Ini');

		$data=array(
			'csrf' => array(
		        'name' => $this->security->get_csrf_token_name(),
		        'hash' => $this->security->get_csrf_hash()
			),
			'dataparent'=>$this->mmenu->getAll()
			);

		$this->load->view('admin/header');
		$this->load->view('admin/menu/new',$data);
		$this->load->view('admin/footer');
	}

	public function modify($idmenu)
	{
		if ($this->auth->check('admin#menu#modify')===false)show_error('Anda Tidak Memiliki Akses Ke Halaman Ini');

		$data=array(
			'csrf' => array(
		        'name' => $this->security->get_csrf_token_name(),
		        'hash' => $this->security->get_csrf_hash()
			),
			'id'=>$this->encryption->encrypt($idmenu),
			'item'=>$this->mmenu->get($idmenu)
			);
		$this->load->view('admin/header');
		$this->load->view('admin/menu/modify',$data);
		$this->load->view('admin/footer');
	}

	public function create(){
		$this->form_validation->set_rules('title', 'Nama Menu', array('required','min_length[1]','max_length[100]'));
		$this->form_validation->set_rules('url', 'URL', array('required','min_length[1]','max_length[100]'));
		if ($this->form_validation->run() == FALSE)
		{
			$this->add();
		}else{
			$data=array(
				'IDPARENT'=>$this->input->post('idparent'),
				'TITLE'=>$this->input->post('title'),
				'URL'=>$this->input->post('url'),
				'MENUICON'=>$this->input->post('menuicon'),
				'MENUORDER'=>$this->input->post('menuorder')
				);
			$this->mmenu->create($data);
			redirect('admin/menu');
		}
	}

	public function update(){
		$rowMenu=$this->mmenu->get($this->encryption->decrypt($this->input->post('id')));
		if(empty($rowMenu))show_404();
		$this->form_validation->set_rules('title', 'Nama Menu', array('required','min_length[1]','max_length[100]'));
		$this->form_validation->set_rules('url', 'URL', array('required','min_length[1]','max_length[100]'));

		if ($this->form_validation->run() == FALSE)
		{
			$this->modify($rowMenu['IDMENU']);
		}else{
			$data=array(
				'IDPARENT'=>$this->input->post('idparent'),
				'TITLE'=>$this->input->post('title'),
				'URL'=>$this->input->post('url'),
				'MENUICON'=>$this->input->post('menuicon'),
				'MENUORDER'=>$this->input->post('menuorder')
				);
			$this->mmenu->update($data,$rowMenu['IDMENU']);
			redirect('admin/menu');
		}
	}

	public function delete($idmenu)
	{
		if(empty($idmenu) )return show_404();
		$this->mmenu->delete($idmenu);			
		redirect('admin/menu');		
	}


}
