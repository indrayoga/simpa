<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sms extends CI_Controller {
	public $user;
	public function  __construct(){
		parent::__construct();
		$this->user=$this->auth->getUser();
		$this->load->library('form_validation');
	}

	public function index()
	{
		if ($this->auth->check('admin#sms')===false)show_error('Anda Tidak Memiliki Akses Ke Halaman Ini');

		$data=array(
			'items'=>$this->db->join('users','sms_outbox.id_user=users.id')->join('kalender_kegiatan','sms_outbox.id_kalender_kegiatan=kalender_kegiatan.id')->join('dipa','kalender_kegiatan.id_kegiatan=dipa.id_dipa')->get('sms_outbox')->result_array()
			);
		$this->load->view('admin/header');
		$this->load->view('admin/sms/index',$data);
		$this->load->view('admin/footer');

	}

}
