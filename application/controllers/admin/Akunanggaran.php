<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Akunanggaran extends CI_Controller {
	public function  __construct(){
		parent::__construct();
		$this->load->model(array('makunanggaran','mbidang'));
		$this->load->library('form_validation');
	}

	public function index()
	{
		if ($this->auth->check('admin#akunanggaran')===false)show_error('Anda Tidak Memiliki Akses Ke Halaman Ini');

		$data=array(
			'items'=>$this->makunanggaran->getAll()
			);
		$this->load->view('admin/header');
		$this->load->view('admin/akunanggaran/index',$data);
		$this->load->view('admin/footer');
	}

	public function add($id)
	{
		if ($this->auth->check('admin#akunanggaran#add')===false)show_error('Anda Tidak Memiliki Akses Ke Halaman Ini');

		$data=array(
			'csrf' => array(
		        'name' => $this->security->get_csrf_token_name(),
		        'hash' => $this->security->get_csrf_hash()
			),
			'id'=>$this->encryption->encrypt($id),
			'item'=>$this->makunanggaran->get($id),
			'databidang' => $this->mbidang->getAll(),			
			'dataakun' => $this->db->get('akun')->result_array()			
			);
		$this->load->view('admin/header');
		$this->load->view('admin/akunanggaran/new',$data);
		$this->load->view('admin/footer');
	}

	public function append($id)
	{
		if ($this->auth->check('admin#akunanggaran#append')===false)show_error('Anda Tidak Memiliki Akses Ke Halaman Ini');
		$data=array(
			'csrf' => array(
		        'name' => $this->security->get_csrf_token_name(),
		        'hash' => $this->security->get_csrf_hash()
			),
			'id'=>$this->encryption->encrypt($id),
			'item'=>$this->makunanggaran->get($id),
			'databidang' => $this->mbidang->getAll(),			
			'dataakun' => $this->db->get('akun')->result_array()			
			);
		$this->load->view('admin/header');
		$this->load->view('admin/akunanggaran/append',$data);
		$this->load->view('admin/footer');
	}

	public function modify($id)
	{
		if ($this->auth->check('admin#akunanggaran#modify')===false)show_error('Anda Tidak Memiliki Akses Ke Halaman Ini');

		$data=array(
			'csrf' => array(
		        'name' => $this->security->get_csrf_token_name(),
		        'hash' => $this->security->get_csrf_hash()
			),
			'id'=>$this->encryption->encrypt($id),
			'item'=>$this->makunanggaran->get($id),
			'databidang' => $this->mbidang->getAll(),			
			'dataakun' => $this->db->get('akun')->result_array()			
			);
		$this->load->view('admin/header');
		$this->load->view('admin/akunanggaran/modify',$data);
		$this->load->view('admin/footer');
	}

	public function create(){
		$rowDipa=$this->makunanggaran->get($this->encryption->decrypt($this->input->post('id')));
		if(empty($rowDipa))show_404();
		$this->form_validation->set_rules('tahun_anggaran', 'Tahun Anggaran', array('required','exact_length[4]','numeric'));
		$this->form_validation->set_rules('kode_bidang', 'Bidang', array('required'));
		$this->form_validation->set_rules('kode_kegiatan', 'Kode Akun', array('required'));
		$this->form_validation->set_rules('pagu', 'Jumlah Pagu', array('required','numeric'));
		if ($this->form_validation->run() == FALSE)
		{
			$this->add($rowDipa['id_dipa']);
		}else{
			$akun=$this->db->get_where('akun',array('kode_akun'=>$this->input->post('kode_kegiatan')))->row_array();
			$data=array(
				'tahun_anggaran'=>$this->input->post('tahun_anggaran'),
				'kode_bidang'=>$this->input->post('kode_bidang'),
				'kode_kegiatan'=>$this->input->post('kode_kegiatan'),
				'kegiatan'=>$akun['nama_akun'],
				'is_akun'=>1,
				'pagu'=>$this->input->post('pagu')
				);
			$this->makunanggaran->create($data,$rowDipa['id_dipa']);
			redirect('admin/akunanggaran');
		}
	}

	public function addappend(){
		$rowDipa=$this->makunanggaran->get($this->encryption->decrypt($this->input->post('id')));
		if(empty($rowDipa))show_404();
		$this->form_validation->set_rules('tahun_anggaran', 'Tahun Anggaran', array('required','exact_length[4]','numeric'));
		$this->form_validation->set_rules('kode_bidang', 'Bidang', array('required'));
		$this->form_validation->set_rules('kode_kegiatan', 'Kode Akun', array('required'));
		$this->form_validation->set_rules('pagu', 'Jumlah Pagu', array('required','numeric'));
		if ($this->form_validation->run() == FALSE)
		{
			$this->append($rowDipa['id_dipa']);
		}else{
			$akun=$this->db->get_where('akun',array('kode_akun'=>$this->input->post('kode_kegiatan')))->row_array();
			$data=array(
				'tahun_anggaran'=>$this->input->post('tahun_anggaran'),
				'kode_bidang'=>$this->input->post('kode_bidang'),
				'kode_kegiatan'=>$this->input->post('kode_kegiatan'),
				'kegiatan'=>$akun['nama_akun'],
				'is_akun'=>1,
				'pagu'=>$this->input->post('pagu')
				);
			$this->makunanggaran->append($data,$rowDipa['id_dipa']);
			redirect('admin/akunanggaran');
		}
	}

	public function update(){
		$rowDipa=$this->makunanggaran->get($this->encryption->decrypt($this->input->post('id')));
		if(empty($rowDipa))show_404();
		$this->form_validation->set_rules('tahun_anggaran', 'Tahun Anggaran', array('required','exact_length[4]','numeric'));
		$this->form_validation->set_rules('kode_bidang', 'Bidang', array('required'));
		$this->form_validation->set_rules('kode_kegiatan', 'Kode', array('required'));
		$this->form_validation->set_rules('pagu', 'Jumlah Pagu', array('required','numeric'));
		if ($this->form_validation->run() == FALSE)
		{
			$this->modify($rowDipa['id_dipa']);
		}else{
			$akun=$this->db->get_where('akun',array('kode_akun'=>$this->input->post('kode_kegiatan')))->row_array();
			$data=array(
				'tahun_anggaran'=>$this->input->post('tahun_anggaran'),
				'kode_bidang'=>$this->input->post('kode_bidang'),
				'kode_kegiatan'=>$this->input->post('kode_kegiatan'),
				'kegiatan'=>$akun['nama_akun'],
				'is_akun'=>1,
				'pagu'=>$this->input->post('pagu')
				);
			$this->makunanggaran->update($data,$rowDipa['id_dipa']);
			redirect('admin/akunanggaran');
		}
	}

	public function delete($id)
	{
		if(empty($id) )return show_404();
		$datauser = $this->makunanggaran->delete($id);			
		redirect('admin/akunanggaran');		
	}

}
