<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	public $user;
	
	public function __construct()
	{
		parent::__construct();
		$this->user=$this->auth->getUser();
		$this->load->library(array('form_validation','auth'));	
		$this->load->model(array('mum','mkk','mkpa','mppk','mdipa','mlaporan'));
	}
	
	public function index()
	{
		if ($this->auth->check('admin#dashboard')===false)
		{
			show_error('Anda Tidak Memiliki Akses Ke Halaman Ini');
		}
		$this->user['seksi']="";
		$data=array(
			'datakegiatan'=>$this->mlaporan->getProgressByBidang($this->user['seksi']),
			'dibuat'=>$this->mlaporan->getKegiatan(1,$this->user['seksi']),
			'disetujui'=>$this->mlaporan->getKegiatan(2,$this->user['seksi']),
			'lulus'=>$this->mlaporan->getKegiatan(3,$this->user['seksi']),
			'selesai'=>$this->mlaporan->getKegiatan(4,$this->user['seksi']),
			'totalanggaran'=>$this->db->query('select sum(pagu) as pagu from dipa where tahun_anggaran="'.$this->session->tahun_anggaran.'"')->row_array(),
			'serapanppk'=>$this->mlaporan->getSerapanPPK($this->user['seksi']),
			'itemsrealisasisemuabidang'=>$this->mlaporan->getStatistik('',$this->session->tahun_anggaran),
			'itemsrealisasibidang'=>$this->mlaporan->getStatistikPerBidang($this->user['seksi'],$this->session->tahun_anggaran),
			'serapananggaran'=>$this->mlaporan->getStatistikBidang($this->user['seksi'],''),
			'tahun'=>$this->session->tahun_anggaran,
			'statistik'=>$this->mlaporan->getStatistik()
			);
		$data['serapananggaran']['jumlah']=$data['serapananggaran']['jumlah']+$data['serapanppk']['jumlah'];
		if($data['serapananggaran']['jumlah']==0){
			$data['persen']=0;
		}else{
			if($data['totalanggaran']['pagu']!=0)$data['persen']=round(($data['serapananggaran']['jumlah']/$data['totalanggaran']['pagu'])*100,2); else $data['persen']=0;
		}
		$this->load->view('admin/header');
		$this->load->view('admin/dashboard',$data);
		$this->load->view('admin/footer');
	}

	/**
	 * login form
	 *
	 * @return string form login
	 */
	public function login(){
		if($this->_validasiFormLogin()){
			if($this->auth->login($this->input->post('username'),$this->input->post('password'))){
			$data=$this->db->get_where('tahun_anggaran',array('aktif'=>1))->row_array();
			$this->session->set_userdata('tahun_anggaran', $data['tahun']);
				redirect('admin/dashboard','refresh');
			}		
		}
		$csrf = array(
		        'name' => $this->security->get_csrf_token_name(),
		        'hash' => $this->security->get_csrf_hash()
		);
		$data=array('csrf'=>$csrf);		
		$this->load->view('admin/login',$data);			
	}

	/**
	 * logout
	 *
	 * @return void
	 */
	public function logout(){
		$this->auth->logout();
		redirect('user/login');
	}

	public function _validasiFormLogin(){
        $this->form_validation->set_rules('username', 'Username', 'required',
                array('required' => ' %s Harus Di isi.')
        );

        $this->form_validation->set_rules('password', 'Password', 'required',
                array('required' => ' %s Harus Di isi.')
        );
        if ($this->form_validation->run() == FALSE)
        {
                return false;
        }
        else
        {
                return true;
        }

	}


}
