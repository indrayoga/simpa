<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Um extends CI_Controller {
	public $user;
	public function  __construct(){
		parent::__construct();
		$this->user=$this->auth->getUser();
		$this->load->model(array('mum','mkk','mkpa','mppk','mdipa','mspm','mbendahara'));
		$this->load->library('form_validation');
	}

	public function index()
	{
		$default=array('page','kegiatan','waktu','lokasi','tag_kpa','tag_ppk');
		$search = $this->uri->uri_to_assoc(2,$default);
		$search['lokasi']=urldecode($search['lokasi']);
		$this->load->helper('date');
		$perPage=25;
		if(empty($search['page']))$search['page']=1;
		$offset=($search['page']*$perPage)-$perPage;
		$total_rows = $this->mum->getCountRows(['kegiatan'=>$search['kegiatan'],'waktu'=>convertDate($search['waktu'],'d-m-Y','Y-m-d'),'lokasi'=>$search['lokasi'],'tag_kpa'=>$search['tag_kpa'],'tag_ppk'=>$search['tag_ppk']]);
		$data=array(
			'csrf' => array(
		        'name' => $this->security->get_csrf_token_name(),
		        'hash' => $this->security->get_csrf_hash()
			),
			'offset'=>$offset,
			'search'=>$search,
			'datakegiatan'=>$this->mdipa->getByBidang($this->user['seksi']),
			'items'=>$this->mum->getAll(['kegiatan'=>$search['kegiatan'],'waktu'=>convertDate($search['waktu'],'d-m-Y','Y-m-d'),'lokasi'=>$search['lokasi'],'tag_kpa'=>$search['tag_kpa'],'tag_ppk'=>$search['tag_ppk']])
			);
		array_walk($data['items'],array($this,'_getStatusUM'));
		array_walk($data['items'],array($this,'_getStatusPPK'));
		array_walk($data['items'],array($this,'_getStatusSPM'));
		array_walk($data['items'],array($this,'_getStatusBendahara'));
		array_walk($data['items'],function(&$item){
			if($item['spm']){
				$item['spm_file']='<img src="'.base_url().'/assets/img/sf/sign-check.svg" width="24" height="24" alt="">'."<a href='".base_url('uploads/'.$item['spm_file'])."'>[Download]</a>";
			}
			if($item['tanda_terima']){
				$item['tanda_terima_file']='<img src="'.base_url().'/assets/img/sf/sign-check.svg" width="24" height="24" alt="">'."<a href='".base_url('uploads/'.$item['tanda_terima_file'])."'>[Download]</a>";
			}
			$item['jumlah']=$this->mum->getTotalDetilBelanja($item['id']);
		});
		$search=array_filter($search, function(&$item){
			return(!empty($item));
		});
		unset($search['page']);
		$str = $this->uri->assoc_to_uri($search);        		
		$base_url = base_url()."um/{$str}/page/";
		$data['paging']=$this->_createPaging($base_url,$total_rows,$perPage);		

		$this->load->view('admin/header');
		$this->load->view('admin/um/index',$data);
		$this->load->view('footer');
	}

	public function diajukan()
	{
		if ($this->auth->checkRole('Pemohon')===false)show_error('Anda Tidak Memiliki Akses Ke Halaman Ini');
		$data=array(
			'csrf' => array(
		        'name' => $this->security->get_csrf_token_name(),
		        'hash' => $this->security->get_csrf_hash()
			),
			'datakegiatan'=>$this->mdipa->getByBidang($this->user['seksi']),
			'items'=>$this->mum->getAll(['user'=>$this->user['id'],'tag_kpa'=>0])
			);
		array_walk($data['items'],function(&$item){
			if($item['spm']){
				$item['spm_file']='<img src="'.base_url().'/assets/img/sf/sign-check.svg" width="24" height="24" alt="">'."<a href='".base_url('uploads/'.$item['spm_file'])."'>[Download]</a>";
			}
			if($item['tanda_terima']){
				$item['tanda_terima_file']='<img src="'.base_url().'/assets/img/sf/sign-check.svg" width="24" height="24" alt="">'."<a href='".base_url('uploads/'.$item['tanda_terima_file'])."'>[Download]</a>";
			}
			$item['jumlah']=$this->mum->getTotalDetilBelanja($item['id']);
		});

		$this->load->view('admin/header');
		$this->load->view('um/diajukan',$data);
		$this->load->view('footer');
	}

	public function disetujui()
	{
		if ($this->auth->checkRole('Pemohon')===false)show_error('Anda Tidak Memiliki Akses Ke Halaman Ini');
		$data=array(
			'csrf' => array(
		        'name' => $this->security->get_csrf_token_name(),
		        'hash' => $this->security->get_csrf_hash()
			),
			'datakegiatan'=>$this->mdipa->getByBidang(),
			'items'=>$this->mum->getAll(['tag_kpa'=>1])
			);
		array_walk($data['items'],function(&$item){
			if($item['spm']){
				$item['spm_file']='<img src="'.base_url().'/assets/img/sf/sign-check.svg" width="24" height="24" alt="">'."<a href='".base_url('uploads/'.$item['spm_file'])."'>[Download]</a>";
			}
			if($item['tanda_terima']){
				$item['tanda_terima_file']='<img src="'.base_url().'/assets/img/sf/sign-check.svg" width="24" height="24" alt="">'."<a href='".base_url('uploads/'.$item['tanda_terima_file'])."'>[Download]</a>";
			}
			$item['jumlah']=$this->mum->getTotalDetilBelanja($item['id']);
		});

		$this->load->view('admin/header');
		$this->load->view('um/disetujui',$data);
		$this->load->view('footer');
	}

	public function lulus()
	{
		if ($this->auth->checkRole('Pemohon')===false)show_error('Anda Tidak Memiliki Akses Ke Halaman Ini');
		$data=array(
			'csrf' => array(
		        'name' => $this->security->get_csrf_token_name(),
		        'hash' => $this->security->get_csrf_hash()
			),
			'datakegiatan'=>$this->mdipa->getByBidang($this->user['seksi']),
			'items'=>$this->mum->getAll(['tag_ppk'=>1])
			);
		array_walk($data['items'],function(&$item){
			if($item['spm']){
				$item['spm_file']='<img src="'.base_url().'/assets/img/sf/sign-check.svg" width="24" height="24" alt="">'."<a href='".base_url('uploads/'.$item['spm_file'])."'>[Download]</a>";
			}
			if($item['tanda_terima']){
				$item['tanda_terima_file']='<img src="'.base_url().'/assets/img/sf/sign-check.svg" width="24" height="24" alt="">'."<a href='".base_url('uploads/'.$item['tanda_terima_file'])."'>[Download]</a>";
			}
			$item['jumlah']=$this->mum->getTotalDetilBelanja($item['id']);
		});

		$this->load->view('admin/header');
		$this->load->view('um/lulus',$data);
		$this->load->view('footer');
	}

	public function selesai()
	{
		if ($this->auth->checkRole('Pemohon')===false)show_error('Anda Tidak Memiliki Akses Ke Halaman Ini');
		$data=array(
			'csrf' => array(
		        'name' => $this->security->get_csrf_token_name(),
		        'hash' => $this->security->get_csrf_hash()
			),
			'datakegiatan'=>$this->mdipa->getByBidang($this->user['seksi']),
			'items'=>$this->mum->getAll(['tag_evaluasi'=>2])
			);
		array_walk($data['items'],function(&$item){
			if($item['spm']){
				$item['spm_file']='<img src="'.base_url().'/assets/img/sf/sign-check.svg" width="24" height="24" alt="">'."<a href='".base_url('uploads/'.$item['spm_file'])."'>[Download]</a>";
			}
			if($item['tanda_terima']){
				$item['tanda_terima_file']='<img src="'.base_url().'/assets/img/sf/sign-check.svg" width="24" height="24" alt="">'."<a href='".base_url('uploads/'.$item['tanda_terima_file'])."'>[Download]</a>";
			}
			$item['jumlah']=$this->mum->getTotalDetilBelanja($item['id']);
		});

		$this->load->view('admin/header');
		$this->load->view('um/selesai',$data);
		$this->load->view('footer');
	}

	public function _createPaging($base_url,$total_rows,$per_page=10){
		$this->load->library('pagination');
		$config['base_url'] = $base_url;
		$config['total_rows'] = $total_rows;
		$config['per_page'] = $per_page;
		$config['full_tag_open'] = '<nav><ul class="pagination">';
		$config['first_link'] = FALSE;
		$config['last_link'] = FALSE;
		$config['prev_tag_open'] = '<li>';
		$config['next_tag_open'] = '<li>';
		$config['num_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['next_tag_close'] = '</li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li><a>';
		$config['cur_tag_close'] = '</a></li>';
		$config['full_tag_close'] = '</ul></nsav>';
		$config['use_page_numbers'] = TRUE;
		$this->pagination->initialize($config);
		return $this->pagination->create_links();				
	}

	public function _getStatusUM(&$item){
		switch ($item['tag_kpa']) {
			case 0:
				if($item['tag_revisi']==0){
					$item['status']="Menunggu Persetujuan KPA";
				}else{
					$item['status']="Menunggu Persetujuan KPA (Sudah Revisi)";
				}
				$item['status_code']=0;
				break;
			case 1:
				$item['status']='<img src="'.base_url().'/assets/img/sf/sign-check.svg" width="24" height="24" alt="Di Setujui KPA">';
				$item['status_code']=1;
				break;
			case 2:
				$item['status_code']=2;
				$catatan=$this->mkpa->getCatatanTerbaru($item['id']);
				$item['status']='<img src="'.base_url().'/assets/img/sf/sign-error.svg" width="24" height="24" alt="Tidak Disetujui KPA"> <br/><button type="button" class="btn btn-sm btn-danger" data-toggle="popover" data-trigger="focus" data-placement="top" title="Catatan" data-content="'.$this->security->xss_clean($catatan['catatan']).'">[klik untuk melihat pesan]</button>';
				break;
			
			default:
				$item['status_code']=0;
				$item['status']="Menunggu Persetujuan KPA";
				break;
		}
	}

	public function _getStatusPPK(&$item){
		switch ($item['tag_ppk']) {
			case 0:
				if($item['tag_revisi_ppk']==0){
					$item['status_ppk']="Menunggu Persetujuan PPK";
				}else{
					$item['status_ppk']="Menunggu Persetujuan PPK (Sudah Revisi)";
				}
				$item['status_ppk_code']=0;
				break;
			case 1:
				$item['status_ppk']='<img src="'.base_url().'/assets/img/sf/sign-check.svg" width="24" height="24" alt="Lulus">';
				$item['status_ppk_code']=1;
				break;
			case 2:
				$item['status_ppk_code']=2;
				$catatan=$this->mppk->getCatatanTerbaru($item['id']);
				$item['status_ppk']='<img src="'.base_url().'/assets/img/sf/sign-error.svg" width="24" height="24" alt="Tidak Lulus"> <br/><button type="button" class="btn btn-sm btn-danger" data-toggle="popover" data-trigger="focus" data-placement="top" title="Catatan" data-content="'.$this->security->xss_clean($catatan['catatan']).'">[klik untuk melihat pesan]</button>';
				break;
			
			default:
				$item['status_ppk_code']=0;
				$item['status_ppk']="Menunggu Persetujuan PPK";
				break;
		}
	}

	public function _getStatusSPM(&$item){

		switch ($item['tag_spm']) {
			case 0:
				if($item['tag_revisi_spm']==0){
					$item['status_spm']="Menunggu Persetujuan PPSPM";
				}else{
					$item['status_spm']="Menunggu Persetujuan PPSPM (Sudah Revisi)";
				}
				$item['status_spm_code']=0;
				break;
			case 1:
				$item['status_spm']='<img src="'.base_url().'/assets/img/sf/sign-check.svg" width="24" height="24" alt="Lulus">';
				$item['status_spm_code']=1;
				break;
			case 2:
				$item['status_spm_code']=2;
				$catatan=$this->mspm->getCatatanTerbaru($item['id']);
				$item['status_spm']='<img src="'.base_url().'/assets/img/sf/sign-error.svg" width="24" height="24" alt="Tidak Lulus"> <br/><button type="button" class="btn btn-sm btn-danger" data-toggle="popover" data-trigger="focus" data-placement="top" title="Catatan" data-content="'.$this->security->xss_clean($catatan['catatan']).'">[klik untuk melihat pesan]</button>';
				break;
			
			default:
				$item['status_spm_code']=0;
				$item['status_spm']="Menunggu Persetujuan PPSPM";
				break;
		}
	}

	public function _getStatusBendahara(&$item){

		switch ($item['tag_bendahara']) {
			case 0:
				if($item['tag_revisi_bendahara']==0){
					$item['status_bendahara']="Menunggu Persetujuan Bendahara";
				}else{
					$item['status_bendahara']="Menunggu Persetujuan Bendahara (Sudah Revisi)";
				}
				$item['status_bendahara_code']=0;
				break;
			case 1:
				$item['status_bendahara']='<img src="'.base_url().'/assets/img/sf/sign-check.svg" width="24" height="24" alt="Lulus">';
				$item['status_bendahara_code']=1;
				break;
			case 2:
				$item['status_bendahara_code']=2;
				$catatan=$this->mbendahara->getCatatanTerbaru($item['id']);
				$item['status_bendahara']='<img src="'.base_url().'/assets/img/sf/sign-error.svg" width="24" height="24" alt="Tidak Lulus"> <br/><button type="button" class="btn btn-sm btn-danger" data-toggle="popover" data-trigger="focus" data-placement="top" title="Catatan" data-content="'.$this->security->xss_clean($catatan['catatan']).'">[klik untuk melihat pesan]</button>';
				break;
			
			default:
				$item['status_code']=0;
				$item['status']="Menunggu Persetujuan Bendahara";
				break;
		}
	}

	public function add()
	{
		if ($this->auth->checkRole('Pemohon')===false)show_error('Anda Tidak Memiliki Akses Ke Halaman Ini');
		if(count($this->mum->getAllBelumSelesai($this->user['id']))>=1){
			$this->load->view('header');
			$this->load->view('um/tidakbisabuat');
			$this->load->view('footer');			
		}else{
			$data=array(
				'csrf' => array(
			        'name' => $this->security->get_csrf_token_name(),
			        'hash' => $this->security->get_csrf_hash()
				),
				'dataoutput'=>$this->mdipa->getAllOutput($this->user['seksi']),
				'dataakun'=>$this->db->get('akun')->result_array(),
				'datakegiatan'=>$this->mkk->getAll($this->user['id'])
				);

			$this->load->view('header');
			$this->load->view('um/new',$data);
			$this->load->view('footer');			
		}
	}

	public function modify($id)
	{
		if ($this->auth->checkRole('Pemohon')===false)show_error('Anda Tidak Memiliki Akses Ke Halaman Ini');

		$data=array(
			'csrf' => array(
		        'name' => $this->security->get_csrf_token_name(),
		        'hash' => $this->security->get_csrf_hash()
			),
			'id'=>$this->encryption->encrypt($id),
			'item'=>$this->mum->get($id),
			'dataoutput'=>$this->mdipa->getAllOutput($this->user['seksi']),
			'datarab'=>$this->mum->getDetil($id),
			'dataakun'=>$this->db->get('akun')->result_array(),
			'datakegiatan'=>$this->mkk->getAll($this->user['id'])
			);
		$data['kegiatan']=$this->mum->getTreeKegiatan($data['item']['id_kegiatan']);
		if(count($data['kegiatan'])==3){
			$data['datakomponenkegiatan']=$this->mdipa->getAllSubOutput($data['kegiatan'][0]['id_dipa']);
			$data['datakegiatan']=$this->mdipa->getAllSubOutput($data['kegiatan'][1]['id_dipa']);
		}
		if(count($data['kegiatan'])==4){
			$data['datasuboutput']=$this->mdipa->getAllSubOutput($data['kegiatan'][0]['id_dipa']);
			$data['datakomponenkegiatan']=$this->mdipa->getAllSubOutput($data['kegiatan'][1]['id_dipa']);
			$data['datakegiatan']=$this->mdipa->getAllSubOutput($data['kegiatan'][2]['id_dipa']);
		}

			list($data['item']['tanggal'],$data['item']['jam'])=explode(" ", $data['item']['waktu']);
		$this->load->view('header');
		$this->load->view('um/modify',$data);
		$this->load->view('footer');
	}

	public function revisi($id,$revisi)
	{
		if ($this->auth->checkRole('Pemohon')===false)show_error('Anda Tidak Memiliki Akses Ke Halaman Ini');

		$data=array(
			'csrf' => array(
		        'name' => $this->security->get_csrf_token_name(),
		        'hash' => $this->security->get_csrf_hash()
			),
			'id'=>$this->encryption->encrypt($id),
			'revisi'=>$this->encryption->encrypt($revisi),
			'item'=>$this->mum->get($id),
			'dataoutput'=>$this->mdipa->getAllOutput($this->user['seksi']),
			'datarab'=>$this->mum->getDetil($id),
			'dataakun'=>$this->db->get('akun')->result_array(),
			'datakegiatan'=>$this->mkk->getAll($this->user['id'])
			);
		$data['kegiatan']=$this->mum->getTreeKegiatan($data['item']['id_kegiatan']);
		if(count($data['kegiatan'])==3){
			$data['datakomponenkegiatan']=$this->mdipa->getAllSubOutput($data['kegiatan'][0]['id_dipa']);
			$data['datakegiatan']=$this->mdipa->getAllSubOutput($data['kegiatan'][1]['id_dipa']);
		}
		if(count($data['kegiatan'])==4){
			$data['datasuboutput']=$this->mdipa->getAllSubOutput($data['kegiatan'][0]['id_dipa']);
			$data['datakomponenkegiatan']=$this->mdipa->getAllSubOutput($data['kegiatan'][1]['id_dipa']);
			$data['datakegiatan']=$this->mdipa->getAllSubOutput($data['kegiatan'][2]['id_dipa']);
		}

		if($data['item']['tag_kpa']!=1 && strtoupper($revisi)!='KPA')die('Not Valid Id');
		if($data['item']['tag_kpa']==1 && $data['item']['tag_ppk']!=1 && strtoupper($revisi)!='PPK')die('Not Valid Id');
		if($data['item']['tag_kpa']==1 && $data['item']['tag_ppk']==1 && $data['item']['tag_spm']!=1 && strtoupper($revisi)!='SPM')die('Not Valid Id');
		list($data['item']['tanggal'],$data['item']['jam'])=explode(" ", $data['item']['waktu']);
		$this->load->view('header');
		$this->load->view('um/revisi',$data);
		$this->load->view('footer');
	}

	public function history($id)
	{
		if ($this->auth->checkRole('Pemohon')===false)show_error('Anda Tidak Memiliki Akses Ke Halaman Ini');

		$data=array(
			'id'=>$this->encryption->encrypt($id),
			'item'=>$this->mum->get($id),
			'datakpa'=>$this->mum->getHistoryKegiatan($id)
			);
		$this->load->view('header');
		$this->load->view('um/history',$data);
		$this->load->view('footer');
	}

	public function hasilkegiatan($id,$id_hasil_kegiatan="")
	{
		if ($this->auth->checkRole('Pemohon')===false)show_error('Anda Tidak Memiliki Akses Ke Halaman Ini');

		$data=array(
			'csrf' => array(
		        'name' => $this->security->get_csrf_token_name(),
		        'hash' => $this->security->get_csrf_hash()
			),
			'id'=>$this->encryption->encrypt($id),
			'id_hasil_kegiatan'=>$this->encryption->encrypt($id_hasil_kegiatan),
			'items'=>$this->mum->getAllHasilKegiatan($id),
			'item'=>$this->mum->get($id)
			);
		$this->load->view('header');
		$this->load->view('um/hasilkegiatan',$data);
		$this->load->view('footer');
	}

	public function create(){
		$this->form_validation->set_rules('id_output', 'Output', 'required');
		if($this->input->post('id_komponen_kegiatan')=="" && $this->input->post('id_sub_komponen_kegiatan')==""){
			$this->form_validation->set_rules('id_output', 'Komponen Kegiatan', 'required');			
		}
		$this->form_validation->set_rules('tahun_anggaran', 'Tahun', 'required|exact_length[4]');
		$this->form_validation->set_rules('tanggal', 'Tanggal', array('required','exact_length[10]'));
		$this->form_validation->set_rules('jam', 'Jam', array('exact_length[8]'));
		$this->form_validation->set_rules('lokasi', 'Lokasi', array('required'));
		if ($this->form_validation->run() == FALSE)
		{
			$this->add();
		}else{
			$this->db->trans_start();
			$tanggal = date_create_from_format('d-m-Y', $this->input->post('tanggal'));
			if($this->input->post('jam')=="")$jam="00:00:00"; else $jam=$this->input->post('jam');
			$waktu=date_format($tanggal, 'Y-m-d').' '.$jam;
			if($this->input->post('id_sub_komponen_kegiatan')==""){
				$id_kegiatan=$this->input->post('id_komponen_kegiatan');
			}else{
				$id_kegiatan=$this->input->post('id_sub_komponen_kegiatan');				
			}
			$data=array(
				'tahun_anggaran'=>$this->input->post('tahun_anggaran'),
				'id_kalender_kegiatan'=>$this->input->post('id_kalender_kegiatan'),
				'id_kegiatan'=>$id_kegiatan,
				'waktu'=>$waktu,
				'lokasi'=>$this->input->post('lokasi'),
				'jumlah_peserta'=>$this->input->post('jumlah_peserta'),
				'kode_mata_anggaran'=>$this->input->post('kode_mata_anggaran'),
				'id_user'=>$this->user['id']
				);
			$id_pengajuan_um=$this->mum->create($data);

			$kode_akun=$this->input->post('kode_akun');
			if(!empty($kode_akun)){
				foreach ($this->input->post('kode_akun') as $key => $value) {
					# code...
					$datarab=array(
						'id_pengajuan_um'=>$id_pengajuan_um,
						'kode_akun'=>$value,
						'detil_belanja'=>$this->input->post('detil_belanja')[$key],
						'vol'=>$this->input->post('vol')[$key],
						'satuan'=>$this->input->post('satuan')[$key],
						'harga_satuan'=>$this->input->post('harga')[$key],
						'jumlah'=>$this->input->post('harga')[$key]*$this->input->post('vol')[$key]
						);
					$this->mum->createDetil($datarab);
				}
			}
			$datalog=array(
				'role'=>'KPA',
				'pesan'=>'Pengajuan UM dari Pemohon '.$this->encryption->decrypt($this->session->username)
				);
			$this->db->insert('pesan',$datalog);
			$this->db->trans_complete();
			redirect('um');
		}
	}

	public function update(){
		$rowUM=$this->mum->get($this->encryption->decrypt($this->input->post('id')));
		if(empty($rowUM))show_404();
		$this->form_validation->set_rules('id_output', 'Output', 'required');
		if($this->input->post('id_komponen_kegiatan')=="" && $this->input->post('id_sub_komponen_kegiatan')==""){
			$this->form_validation->set_rules('id_output', 'Komponen Kegiatan', 'required');			
		}
		$this->form_validation->set_rules('tahun_anggaran', 'Tahun', 'required|exact_length[4]');
		$this->form_validation->set_rules('tanggal', 'Tanggal', array('required','exact_length[10]'));
		$this->form_validation->set_rules('jam', 'Jam', array('exact_length[8]'));
		$this->form_validation->set_rules('lokasi', 'Lokasi', array('required'));

		if ($this->form_validation->run() == FALSE)
		{
			$this->modify($rowUM['id']);
		}else{
			$tanggal = date_create_from_format('d-m-Y', $this->input->post('tanggal'));
			if($this->input->post('jam')=="")$jam="00:00:00"; else $jam=$this->input->post('jam');
			$waktu=date_format($tanggal, 'Y-m-d').' '.$jam;
			if($this->input->post('id_sub_komponen_kegiatan')==""){
				$id_kegiatan=$this->input->post('id_komponen_kegiatan');
			}else{
				$id_kegiatan=$this->input->post('id_sub_komponen_kegiatan');				
			}
			$data=array(
				'tahun_anggaran'=>$this->input->post('tahun_anggaran'),
				'id_kalender_kegiatan'=>$this->input->post('id_kalender_kegiatan'),
				'id_kegiatan'=>$id_kegiatan,
				'waktu'=>$waktu,
				'lokasi'=>$this->input->post('lokasi'),
				'jumlah_peserta'=>$this->input->post('jumlah_peserta'),
				'kode_mata_anggaran'=>$this->input->post('kode_mata_anggaran'),
				);
			$this->mum->update($data,$rowUM['id']);
			$kode_akun=$this->input->post('kode_akun');
			$this->db->delete('detil_pengajuan_um',array('id_pengajuan_um'=>$rowUM['id']));
			if(!empty($kode_akun)){
				foreach ($this->input->post('kode_akun') as $key => $value) {
					# code...
					$datarab=array(
						'id_pengajuan_um'=>$rowUM['id'],
						'kode_akun'=>$value,
						'detil_belanja'=>$this->input->post('detil_belanja')[$key],
						'vol'=>$this->input->post('vol')[$key],
						'satuan'=>$this->input->post('satuan')[$key],
						'harga_satuan'=>$this->input->post('harga')[$key],
						'jumlah'=>$this->input->post('harga')[$key]*$this->input->post('vol')[$key]
						);
					$this->mum->createDetil($datarab);
				}

			}
			redirect('um');
		}
	}

	public function updaterevisi(){
		$rowUM=$this->mum->get($this->encryption->decrypt($this->input->post('id')));
		$revisi=$this->encryption->decrypt($this->input->post('revisi'));
		$revisi=strtoupper($revisi);
		if(empty($rowUM) || !in_array($revisi, array('KPA','SPM','PPK','BENDAHARA')))show_404();
		$this->form_validation->set_rules('id_output', 'Output', 'required');
		if($this->input->post('id_komponen_kegiatan')=="" && $this->input->post('id_sub_komponen_kegiatan')==""){
			$this->form_validation->set_rules('id_output', 'Komponen Kegiatan', 'required');			
		}
		$this->form_validation->set_rules('tahun_anggaran', 'Tahun', 'required|exact_length[4]');
		$this->form_validation->set_rules('tanggal', 'Tanggal', array('required','exact_length[10]'));
		$this->form_validation->set_rules('jam', 'Jam', array('exact_length[8]'));
		$this->form_validation->set_rules('lokasi', 'Lokasi', array('required'));

		if ($this->form_validation->run() == FALSE){
			$this->revisi($rowUM['id'],$revisi);
		}else{
			$tanggal = date_create_from_format('d-m-Y', $this->input->post('tanggal'));
			if($this->input->post('jam')=="")$jam="00:00:00"; else $jam=$this->input->post('jam');
			$waktu=date_format($tanggal, 'Y-m-d').' '.$jam;
			if($this->input->post('id_sub_komponen_kegiatan')==""){
				$id_kegiatan=$this->input->post('id_komponen_kegiatan');
			}else{
				$id_kegiatan=$this->input->post('id_sub_komponen_kegiatan');				
			}
			$data=array(
				'tahun_anggaran'=>$this->input->post('tahun_anggaran'),
				'id_kalender_kegiatan'=>$this->input->post('id_kalender_kegiatan'),
				'id_kegiatan'=>$id_kegiatan,
				'waktu'=>$waktu,
				'lokasi'=>$this->input->post('lokasi'),
				'jumlah_peserta'=>$this->input->post('jumlah_peserta'),
				'kode_mata_anggaran'=>$this->input->post('kode_mata_anggaran'),
				);
			$this->mum->revisi($data,$rowUM['id'],$revisi);
			$kode_akun=$this->input->post('kode_akun');
			$this->db->delete('detil_pengajuan_um',array('id_pengajuan_um'=>$rowUM['id']));
			if(!empty($kode_akun)){
				foreach ($this->input->post('kode_akun') as $key => $value) {
					# code...
					$datarab=array(
						'id_pengajuan_um'=>$rowUM['id'],
						'kode_akun'=>$value,
						'detil_belanja'=>$this->input->post('detil_belanja')[$key],
						'vol'=>$this->input->post('vol')[$key],
						'satuan'=>$this->input->post('satuan')[$key],
						'harga_satuan'=>$this->input->post('harga')[$key],
						'jumlah'=>$this->input->post('harga')[$key]*$this->input->post('vol')[$key]
						);
					$this->mum->createDetil($datarab);
				}

			}
			$datalog=array(
				'role'=>$revisi,
				'pesan'=>'Perbaikan Berkas Pengajuan UM dari Pemohon '.$this->encryption->decrypt($this->session->username)
				);
			$this->db->insert('pesan',$datalog);

			redirect('um');
		}
	}

	public function upload(){
		$rowUM=$this->mum->get($this->encryption->decrypt($this->input->post('id')));
		$rowHasilKegiatan=$this->mum->getHasilKegiatan($this->encryption->decrypt($this->input->post('id_hasil_kegiatan')));
		if(empty($rowUM))show_404();
        $config['upload_path']          = './uploads/';
        $config['allowed_types']        = 'gif|jpg|png|pdf|zip';
        $config['file_ext_tolower']		= TRUE;
        $this->load->library('upload', $config);
        if( ! $this->upload->do_upload('userfile')){
			$_SESSION['error_upload'] = $this->upload->display_errors("","");
			$this->session->mark_as_flash('error_upload');
            $this->detail($rowUM['id'],$rowHasilKegiatan['id']);
        }else{
	        $upload_data = $this->upload->data();
			$data=array(
				'id_pengajuan_um'=>$rowUM['id'],
				'namafile'=>$upload_data['file_name'],
				'jenis'=>$this->input->post('jenis'),
				'id_user'=>$this->user['id']
				);
			$this->mum->createEvaluasi($data);
			$dataum=array('tag_evaluasi'=>1);
			$this->mum->update($dataum,$rowUM['id']);			
			$datalog=array(
				'role'=>'KPA',
				'pesan'=>'Data Hasil Evaluasi '.$rowUM['kegiatan']
				);
			$this->db->insert('pesan',$datalog);
			redirect('um');        			
        }
	}

	public function delete($id)
	{
		if(empty($id) )return show_404();
		$datauser = $this->mum->delete($id);			
		redirect('um');		
	}

	public function deletehasilkegiatan($id,$id_pengajuan_um)
	{
		if(empty($id) )return show_404();
		$datauser = $this->mum->deletehasilkegiatan($id);			
		redirect('um/hasilkegiatan/'.$id_pengajuan_um);		
	}


}
