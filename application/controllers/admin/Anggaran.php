<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Anggaran extends CI_Controller {
	public function  __construct(){
		parent::__construct();
		$this->load->model(array('manggaran'));
		$this->load->library('form_validation');
	}

	public function index()
	{
		if ($this->auth->check('admin#anggaran')===false)show_error('Anda Tidak Memiliki Akses Ke Halaman Ini');

		$data=array(
			'items'=>$this->manggaran->getAll()
			);
		$this->load->view('admin/header');
		$this->load->view('admin/anggaran/index',$data);
		$this->load->view('admin/footer');
	}

	public function add()
	{
		if ($this->auth->check('admin#anggaran#add')===false)show_error('Anda Tidak Memiliki Akses Ke Halaman Ini');

		$data=array(
			'csrf' => array(
		        'name' => $this->security->get_csrf_token_name(),
		        'hash' => $this->security->get_csrf_hash()
			)
			);

		$this->load->view('admin/header');
		$this->load->view('admin/anggaran/new',$data);
		$this->load->view('admin/footer');
	}

	public function modify($id)
	{
		if ($this->auth->check('admin#anggaran#modify')===false)show_error('Anda Tidak Memiliki Akses Ke Halaman Ini');

		$data=array(
			'csrf' => array(
		        'name' => $this->security->get_csrf_token_name(),
		        'hash' => $this->security->get_csrf_hash()
			),
			'id'=>$this->encryption->encrypt($id),
			'item'=>$this->manggaran->get($id)
			);
		$this->load->view('admin/header');
		$this->load->view('admin/anggaran/modify',$data);
		$this->load->view('admin/footer');
	}

	public function create(){
		$this->form_validation->set_rules('tahun', 'Tahun', array('is_unique[tahun_anggaran.tahun]','required','exact_length[4]'));
		$this->form_validation->set_rules('copy', 'Tahun', array('exact_length[4]'));
		if ($this->form_validation->run() == FALSE)
		{
			$this->add();
		}else{
			$data=array(
				'tahun'=>$this->input->post('tahun')
				);
			if($this->input->post('aktif')==1){
				$data['aktif']=1;
			}else{
				$data['aktif']=0;
			}
			if($this->input->post('copy')!=""){
				$this->manggaran->copyDipa($this->input->post('tahun'),$this->input->post('copy'));
			}
			$this->manggaran->create($data);
			redirect('admin/anggaran');
		}
	}

	public function update(){
		$rowAnggaran=$this->manggaran->get($this->encryption->decrypt($this->input->post('id')));
		if(empty($rowAnggaran))show_404();
		$this->form_validation->set_rules('tahun', 'Tahun', array('required','exact_length[4]'));
		if ($this->form_validation->run() == FALSE)
		{
			$this->modify($rowAnggaran['id']);
		}else{
			$data=array(
				'tahun'=>$this->input->post('tahun')
				);
			if($this->input->post('aktif')==1){
				$data['aktif']=1;
			}else{
				$data['aktif']=0;
			}
			if($this->input->post('copy')!=""){
				$this->manggaran->copyDipa($this->input->post('tahun'),$this->input->post('copy'));
			}
			$this->manggaran->update($data,$rowAnggaran['id']);
			redirect('admin/anggaran');
		}
	}

	public function activated($id)
	{
		if(empty($id) )return show_404();
		$datauser = $this->manggaran->activated($id);			
		redirect('admin/anggaran');		
	}


}
