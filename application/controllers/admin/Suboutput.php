<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Suboutput extends CI_Controller {
	public function  __construct(){
		parent::__construct();
		$this->load->model(array('msuboutput','mbidang'));
		$this->load->library('form_validation');
	}

	public function index()
	{
		if ($this->auth->check('admin#suboutput')===false)show_error('Anda Tidak Memiliki Akses Ke Halaman Ini');

		$data=array(
			'items'=>$this->msuboutput->getAll()
			);
		//var_dump($data['items']);
		$tree=array();
		$i=0;
		$x=0;
		$lv3=0;
		array_walk($data['items'], function($item) use (&$tree,&$i,&$lv3,&$x){
			if($item['level']==1){
				$tree[$i]['id']=$item['id_dipa'];
				$tree[$i]['level']=$item['level'];
				$tree[$i]['text']=$item['kode_kegiatan']."-".$item['kegiatan']."<span class='pull-right'>".number_format($item['pagu'],0,'','.')."</span>";
				$x=$i;
				$lv3=0;
				$lv4=0;
				$lv5=0;
				$lv6=0;				
				$i=$i+1;
			}
			if($item['level']==2){
				$tree[$x]['nodes'][++$lv3]['text']=$item['kode_kegiatan']."-".$item['kegiatan']."<span class='pull-right'>".number_format($item['pagu'],0,'','.')."</span>";
				$tree[$x]['nodes'][$lv3]['id']=$item['id_dipa'];
				$tree[$x]['nodes'][$lv3]['level']=$item['level'];
			}
		});
		$data['tree']=$tree;

		$this->load->view('admin/header');
		$this->load->view('admin/suboutput/index',$data);
		$this->load->view('admin/footer');
	}

	public function add($id)
	{
		if ($this->auth->check('admin#suboutput#add')===false)show_error('Anda Tidak Memiliki Akses Ke Halaman Ini');

		$data=array(
			'csrf' => array(
		        'name' => $this->security->get_csrf_token_name(),
		        'hash' => $this->security->get_csrf_hash()
			),
			'id'=>$this->encryption->encrypt($id),
			'item'=>$this->msuboutput->get($id),
			'databidang' => $this->mbidang->getAll()			
			);
		$this->load->view('admin/header');
		$this->load->view('admin/suboutput/new',$data);
		$this->load->view('admin/footer');
	}

	public function append($id)
	{
		if ($this->auth->check('admin#suboutput#append')===false)show_error('Anda Tidak Memiliki Akses Ke Halaman Ini');
		$data=array(
			'csrf' => array(
		        'name' => $this->security->get_csrf_token_name(),
		        'hash' => $this->security->get_csrf_hash()
			),
			'id'=>$this->encryption->encrypt($id),
			'item'=>$this->msuboutput->get($id),
			'databidang' => $this->mbidang->getAll()			
			);
		$this->load->view('admin/header');
		$this->load->view('admin/suboutput/append',$data);
		$this->load->view('admin/footer');
	}

	public function modify($id)
	{
		if ($this->auth->check('admin#suboutput#modify')===false)show_error('Anda Tidak Memiliki Akses Ke Halaman Ini');

		$data=array(
			'csrf' => array(
		        'name' => $this->security->get_csrf_token_name(),
		        'hash' => $this->security->get_csrf_hash()
			),
			'id'=>$this->encryption->encrypt($id),
			'item'=>$this->msuboutput->get($id),
			'databidang' => $this->mbidang->getAll()			
			);
		$this->load->view('admin/header');
		$this->load->view('admin/suboutput/modify',$data);
		$this->load->view('admin/footer');
	}

	public function create(){
		$rowDipa=$this->msuboutput->get($this->encryption->decrypt($this->input->post('id')));
		if(empty($rowDipa))show_404();
		$this->form_validation->set_rules('tahun_anggaran', 'Tahun Anggaran', array('required','exact_length[4]','numeric'));
		$this->form_validation->set_rules('kode_bidang', 'Bidang', array('required'));
		$this->form_validation->set_rules('kode_kegiatan', 'Kode', array('required','is_unique[dipa.kode_kegiatan]'));
		$this->form_validation->set_rules('kegiatan', 'Kegiatan', array('required','min_length[1]','max_length[255]','is_unique[dipa.kegiatan]'));
		$this->form_validation->set_rules('tipe', 'Ket. Belanja', array('required','in_list[BP,BBO,BBN,BM]'));
		if ($this->form_validation->run() == FALSE)
		{
			$this->add($rowDipa['id_dipa']);
		}else{
			$data=array(
				'tahun_anggaran'=>$this->input->post('tahun_anggaran'),
				'kode_bidang'=>$this->input->post('kode_bidang'),
				'kode_kegiatan'=>$this->input->post('kode_kegiatan'),
				'kegiatan'=>$this->input->post('kegiatan'),
				'tipe'=>$this->input->post('tipe')
				);
			$this->msuboutput->create($data,$rowDipa['id_dipa']);
			redirect('admin/suboutput');
		}
	}

	public function addappend(){
		$rowDipa=$this->msuboutput->get($this->encryption->decrypt($this->input->post('id')));
		if(empty($rowDipa))show_404();
		$this->form_validation->set_rules('tahun_anggaran', 'Tahun Anggaran', array('required','exact_length[4]','numeric'));
		$this->form_validation->set_rules('kode_bidang', 'Bidang', array('required'));
		$this->form_validation->set_rules('kode_kegiatan', 'Kode', array('required','is_unique[dipa.kode_kegiatan]'));
		$this->form_validation->set_rules('kegiatan', 'Kegiatan', array('required','min_length[1]','max_length[255]','is_unique[dipa.kegiatan]'));
		$this->form_validation->set_rules('tipe', 'Ket. Belanja', array('required','in_list[BP,BBO,BBN,BM]'));
		if ($this->form_validation->run() == FALSE)
		{
			$this->append($rowDipa['id_dipa']);
		}else{
			$data=array(
				'tahun_anggaran'=>$this->input->post('tahun_anggaran'),
				'kode_bidang'=>$this->input->post('kode_bidang'),
				'kode_kegiatan'=>$this->input->post('kode_kegiatan'),
				'kegiatan'=>$this->input->post('kegiatan'),
				'tipe'=>$this->input->post('tipe')
				);
			$this->msuboutput->append($data,$rowDipa['id_dipa']);
			redirect('admin/suboutput');
		}
	}

	public function update(){
		$rowDipa=$this->msuboutput->get($this->encryption->decrypt($this->input->post('id')));
		if(empty($rowDipa))show_404();
		$this->form_validation->set_rules('tahun_anggaran', 'Tahun Anggaran', array('required','exact_length[4]','numeric'));
		$this->form_validation->set_rules('kode_bidang', 'Bidang', array('required'));
		$this->form_validation->set_rules('kode_kegiatan', 'Kode', array('required'));
		$this->form_validation->set_rules('kegiatan', 'Kegiatan', array('required','min_length[1]','max_length[255]'));
		$this->form_validation->set_rules('tipe', 'Ket. Belanja', array('required','in_list[BP,BBO,BBN,BM]'));
		if ($this->form_validation->run() == FALSE)
		{
			$this->modify($rowDipa['id_dipa']);
		}else{
			$data=array(
				'tahun_anggaran'=>$this->input->post('tahun_anggaran'),
				'kode_bidang'=>$this->input->post('kode_bidang'),
				'kode_kegiatan'=>$this->input->post('kode_kegiatan'),
				'kegiatan'=>$this->input->post('kegiatan'),
				'tipe'=>$this->input->post('tipe')
				);
			$this->msuboutput->update($data,$rowDipa['id_dipa']);
			redirect('admin/suboutput');
		}
	}

	public function delete($id)
	{
		if(empty($id) )return show_404();
		$datauser = $this->msuboutput->delete($id);			
		redirect('admin/suboutput');		
	}

}
