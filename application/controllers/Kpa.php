<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kpa extends CI_Controller {
	public $user;
	public function  __construct(){
		parent::__construct();
		$this->user=$this->auth->getUser();
		$this->load->model(array('mum','mkpa','mdipa'));
		$this->load->library('form_validation');
	}

	public function _remap($method,$param=array()){
	        if ($method === 'page'){
	                $this->index();
	        }else if ($method === 'search'){
	        	$data=array();
	        	if($this->input->post('id_kegiatan')!="")$data['kegiatan']=$this->input->post('id_kegiatan');
	        	if($this->input->post('waktu')!="")$data['waktu']=$this->input->post('waktu');
	        	if($this->input->post('lokasi')!="")$data['lokasi']=$this->input->post('lokasi');
	        	if($this->input->post('tag_ppk')!="")$data['tag_ppk']=$this->input->post('tag_ppk');
	        	if($this->input->post('tag_kpa')!="")$data['tag_kpa']=$this->input->post('tag_kpa');
				$str = $this->uri->assoc_to_uri($data);        		
	            redirect('kpa/'.$str.'/page/1/');
	        }else if (method_exists($this, $method)){
                return call_user_func_array(array($this, $method),$param);
	        }else{
	            $this->index();	        	
	        }
	}

	public function index()
	{
		if ($this->auth->checkRole('KPA')===false)show_error('Anda Tidak Memiliki Akses Ke Halaman Ini');
		$default=array('page','kegiatan','waktu','lokasi','tag_kpa','tag_ppk');
		$search = $this->uri->uri_to_assoc(2,$default);
		$search['lokasi']=urldecode($search['lokasi']);
		$this->load->helper('date');
		$perPage=10;
		if(empty($search['page']))$search['page']=1;
		$offset=($search['page']*$perPage)-$perPage;
		$total_rows = $this->mum->getCountRows(['user'=>'','kegiatan'=>$search['kegiatan'],'waktu'=>convertDate($search['waktu'],'d-m-Y','Y-m-d'),'lokasi'=>$search['lokasi'],'tag_kpa'=>$search['tag_kpa'],'tag_ppk'=>$search['tag_ppk']]);

		$data=array(
			'csrf' => array(
		        'name' => $this->security->get_csrf_token_name(),
		        'hash' => $this->security->get_csrf_hash()
			),
			'offset'=>$offset,
			'search'=>$search,
			'datakegiatan'=>$this->mdipa->getByBidang(),
			'items'=>$this->mum->getAll(['user'=>'','kegiatan'=>$search['kegiatan'],'waktu'=>convertDate($search['waktu'],'d-m-Y','Y-m-d'),'lokasi'=>$search['lokasi'],'tag_kpa'=>$search['tag_kpa'],'tag_ppk'=>$search['tag_ppk']])
			);
		array_walk($data['items'],array($this,'_getStatusUM'));
		array_walk($data['items'],function(&$item){
			$item['jumlah']=$this->mum->getTotalDetilBelanja($item['id']);
		});
		$search=array_filter($search, function(&$item){
			return(!empty($item));
		});
		unset($search['page']);
		$str = $this->uri->assoc_to_uri($search);        		
		$base_url = base_url()."kpa/{$str}/page/";
		$data['paging']=$this->_createPaging($base_url,$total_rows,$perPage);		

		$this->load->view('header');
		$this->load->view('kpa/index',$data);
		$this->load->view('footer');
	}

	public function _createPaging($base_url,$total_rows,$per_page=10){
		$this->load->library('pagination');
		$config['base_url'] = $base_url;
		$config['total_rows'] = $total_rows;
		$config['per_page'] = $per_page;
		$config['full_tag_open'] = '<nav><ul class="pagination">';
		$config['first_link'] = FALSE;
		$config['last_link'] = FALSE;
		$config['prev_tag_open'] = '<li>';
		$config['next_tag_open'] = '<li>';
		$config['num_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['next_tag_close'] = '</li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li><a>';
		$config['cur_tag_close'] = '</a></li>';
		$config['full_tag_close'] = '</ul></nsav>';
		$config['use_page_numbers'] = TRUE;
		$this->pagination->initialize($config);
		return $this->pagination->create_links();				
	}

	public function _getStatusUM(&$item){
		switch ($item['tag_kpa']) {
			case 0:
				if($item['tag_revisi']==0){
					$item['status']="Menunggu Persetujuan KPA";
				}else{
					$item['status']="Menunggu Persetujuan KPA (Sudah Revisi)";
				}
				$item['status_code']=0;
				break;
			case 1:
				$item['status']="Di Setujui KPA";
				$item['status_code']=1;
				break;
			case 2:
				$item['status_code']=2;
				$catatan=$this->mkpa->getCatatanTerbaru($item['id']);
				$item['status']='Tidak Disetujui KPA <br/><button type="button" class="btn btn-sm btn-danger" data-toggle="popover" data-trigger="focus" data-placement="top" title="Catatan" data-content="'.$this->security->xss_clean($catatan['catatan']).'">[klik untuk melihat pesan]</button>';
				break;
			
			default:
				$item['status_code']=0;
				$item['status']="Menunggu Persetujuan KPA";
				break;
		}
	}


	public function detail($id)
	{
		if ($this->auth->checkRole('KPA')===false)show_error('Anda Tidak Memiliki Akses Ke Halaman Ini');

		$data=array(
			'csrf' => array(
		        'name' => $this->security->get_csrf_token_name(),
		        'hash' => $this->security->get_csrf_hash()
			),
			'datakegiatan'=>$this->mdipa->getByBidang(),
			'datarab'=>$this->mum->getDetil($id),
			'dataakun'=>$this->db->get('akun')->result_array(),
			'id'=>$this->encryption->encrypt($id),
			'item'=>$this->mum->get($id)
			);
			list($data['item']['tanggal'],$data['item']['jam'])=explode(" ", $data['item']['waktu']);
		$this->load->view('header');
		$this->load->view('kpa/detail',$data);
		$this->load->view('footer');
	}

	public function approval(){
		$rowUM=$this->mum->get($this->encryption->decrypt($this->input->post('id')));
		if(empty($rowUM))show_404();
		if($this->input->post('status')==2){
			$this->form_validation->set_rules('catatan', 'Catatan', 'required');
		}else{
			$this->form_validation->set_rules('catatan', 'Catatan','max_length[10000]');			
		}
		if ($this->form_validation->run() == FALSE)
		{
			$this->detail($rowUM['id']);
		}else{
			$data=array(
				'id_pengajuan_um'=>$rowUM['id'],
				'status'=>$this->input->post('status'),
				'catatan'=>$this->input->post('catatan'),
				'id_user'=>$this->user['id']
				);
			$this->mkpa->create($data);
			$dataum=array('tag_kpa'=>$this->input->post('status'));
			$this->mum->update($dataum,$rowUM['id']);
			if($this->input->post('status')==1){
				$pesan="Pengajuan UM untuk Kegiatan ".$rowUM['kegiatan']." disetujui oleh KPA";
			}else{
				$pesan="Pengajuan UM untuk Kegiatan ".$rowUM['kegiatan']." Tidak disetujui oleh KPA";				
			}
			$datalog=array(
				'id_user'=>$rowUM['id_user'],
				'role'=>'Pemohon',
				'pesan'=>$pesan
				);
			$this->db->insert('pesan',$datalog);

			redirect('kpa');
		}
	}


}
