<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	public $user;
	
	public function __construct()
	{
		parent::__construct();
		$this->user=$this->auth->getUser();
		$this->load->library(array('form_validation','auth'));	
		$this->load->model(array('mum','mkk','mkpa','mppk','mdipa','mlaporan'));
	}
	
	public function index()
	{
		if($this->user['seksi']=="-")$this->user['seksi']="";
		$data=array(
			'datakegiatan'=>$this->mlaporan->getProgressByBidang($this->user['seksi']),
			'dibuat'=>$this->mlaporan->getKegiatan(1,$this->user['seksi']),
			'disetujui'=>$this->mlaporan->getKegiatan(2,$this->user['seksi']),
			'lulus'=>$this->mlaporan->getKegiatan(3,$this->user['seksi']),
			'selesai'=>$this->mlaporan->getKegiatan(4,$this->user['seksi']),
			'totalanggaran'=>$this->db->query('select sum(pagu) as pagu from dipa where tahun_anggaran="'.$this->session->tahun_anggaran.'"')->row_array(),
			'serapanppk'=>$this->mlaporan->getSerapanPPK($this->user['seksi']),
			'serapananggaran'=>$this->mlaporan->getStatistikBidang($this->user['seksi'],''),
			'itemsrealisasisemuabidang'=>$this->mlaporan->getStatistik('',$this->session->tahun_anggaran),
			'itemsrealisasibidang'=>$this->mlaporan->getStatistikPerBidang($this->user['seksi'],$this->session->tahun_anggaran),
			'persenrealisasi'=>$this->mlaporan->getPersenRealisasi($this->user['seksi'],''),
			'tahun'=>$this->session->tahun_anggaran,
			'statistik'=>$this->mlaporan->getStatistik()
			);
		//die(print_r($data['itemsrealisasibidang']));
		$data['serapananggaran']['jumlah']=$data['serapananggaran']['jumlah']+$data['serapanppk']['jumlah'];
		if($data['serapananggaran']['jumlah']==0){
			$data['persen']=0;
		}else{
			$data['persen']=round(($data['serapananggaran']['jumlah']/$data['totalanggaran']['pagu'])*100,2);
		}
		$this->load->view('header');
		$this->load->view('dashboard',$data);
		$this->load->view('footer');
	}

	public function kalender()
	{
		$data=array();
		$this->load->view('header');
		$this->load->view('kalender',$data);
		$this->load->view('footer');
	}

	/**
	 * login form
	 *
	 * @return string form login
	 */
	public function login(){
		if($this->_validasiFormLogin()){
			if($this->auth->login($this->input->post('username'),$this->input->post('password'))){
			$data=$this->db->get_where('tahun_anggaran',array('aktif'=>1))->row_array();
			$this->session->set_userdata('tahun_anggaran', $data['tahun']);
				redirect('dashboard','refresh');
			}		
		}
		$csrf = array(
		        'name' => $this->security->get_csrf_token_name(),
		        'hash' => $this->security->get_csrf_hash()
		);
		$data=array('csrf'=>$csrf);		
		$this->load->view('user/login',$data);			
		
	}

	/**
	 * logout
	 *
	 * @return void
	 */
	public function logout(){
		$this->auth->logout();
		redirect('user/login');
	}

	public function _validasiFormLogin(){
        $this->form_validation->set_rules('username', 'Username', 'required',
                array('required' => ' %s Harus Di isi.')
        );

        $this->form_validation->set_rules('password', 'Password', 'required',
                array('required' => ' %s Harus Di isi.')
        );
        if ($this->form_validation->run() == FALSE)
        {
                return false;
        }
        else
        {
                return true;
        }

	}


}
