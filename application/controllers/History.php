<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class History extends CI_Controller {
	public $user;
	public function  __construct(){
		parent::__construct();
		$this->user=$this->auth->getUser();
		$this->load->model(array('mum','mkk','mkpa','mppk','mdipa'));
		$this->load->library('form_validation');
	}

	public function kegiatan($id)
	{

		$data=array(
			'id'=>$this->encryption->encrypt($id),
			'item'=>$this->mum->get($id),
			'datakpa'=>$this->mum->getHistoryKegiatan($id)
			);
		$this->load->view('header');
		$this->load->view('um/history',$data);
		$this->load->view('footer');
	}

	public function pesan()
	{

		$data=array();
		if ($this->auth->checkRole('Pemohon')===false){
			$data['items']=$this->db->order_by('tgl_dibuat','desc')->get_where('pesan',array('role'=>$this->user['role']))->result_array();
		}else{
			$data['items']=$this->db->order_by('tgl_dibuat','desc')->get_where('pesan',array('id_user'=>$this->user['id']))->result_array();		
		}
		$this->load->view('header');
		$this->load->view('um/pesan',$data);
		$this->load->view('footer');
	}

}
