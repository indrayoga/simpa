<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kk extends CI_Controller {
	public $user;
	public function  __construct(){
		parent::__construct();
		$this->user=$this->auth->getUser();
		$this->load->model(array('mkk','mdipa'));
		$this->load->library('form_validation');
	}

	public function _remap($method,$param=array()){
	        if ($method === 'page'){
	                $this->index();
	        }else if ($method === 'search'){
	        	$data=array();
	        	if($this->input->post('id_kegiatan')!="")$data['kegiatan']=$this->input->post('id_kegiatan');
	        	if($this->input->post('tanggal_mulai')!="")$data['tanggal_mulai']=$this->input->post('tanggal_mulai');
	        	if($this->input->post('minggu_mulai')!="")$data['minggu_mulai']=$this->input->post('minggu_mulai');
				$str = $this->uri->assoc_to_uri($data);        		
	            redirect('kk/'.$str.'/page/1/');
	        }else if (method_exists($this, $method)){
                return call_user_func_array(array($this, $method),$param);
	        }else{
	            $this->index();	        	
	        }
	}

	public function kalender()
	{
		if($this->user['seksi']=="-")$this->user['seksi']="";
		$data=array(
			'datakegiatan'=>$this->mdipa->getByBidangKalender($this->user['seksi']),
			);
		$this->load->view('header');
		$this->load->view('kk/kalenderkerja',$data);
		$this->load->view('footer');
	}

	public function eventkk()
	{
		if ($this->auth->checkRole('Pemohon')===false)show_error('Anda Tidak Memiliki Akses Ke Halaman Ini');
		$items=$this->mkk->getAll($this->user['id']);
		$data=array();
		foreach ($items as $item) {
			# code...
			$hitungminggu=(($item['bulan_berakhir']-1)*4)+$item['minggu_terakhir'];
			$date = new DateTime();
			$date->setISODate($item['tahun_anggaran'], $hitungminggu, 7);
			$data[]=array(
				'title'=>$item['kegiatan'],
				'start'=>$item['tgl_mulai'],
				'end'=>$date->format('Y-m-d')
				);
		}
		echo json_encode($data);
	}

	public function index()
	{
		if ($this->auth->checkRole('Pemohon')===false)show_error('Anda Tidak Memiliki Akses Ke Halaman Ini');
		$default=array('page','kegiatan','tanggal_mulai','minggu_mulai');
		$search = $this->uri->uri_to_assoc(3,$default);
		$this->load->helper('date');
		$perPage=25;
		if(empty($search['page']))$search['page']=1;
		$offset=($search['page']*$perPage)-$perPage;
		$total_rows = $this->mkk->getCountRows($this->user['id'],['kegiatan'=>$search['kegiatan'],'tanggal_mulai'=>convertDate($search['tanggal_mulai'],'d-m-Y','Y-m-d'),'minggu_mulai'=>$search['minggu_mulai']]);
		$data=array(
			'csrf' => array(
		        'name' => $this->security->get_csrf_token_name(),
		        'hash' => $this->security->get_csrf_hash()
			),
			'offset'=>$offset,
			'datakegiatan'=>$this->mdipa->getByBidang(),
			'search'=>$search,
			'items'=>$this->mkk->getAll('',['kegiatan'=>$search['kegiatan'],'tanggal_mulai'=>convertDate($search['tanggal_mulai'],'d-m-Y','Y-m-d'),'minggu_mulai'=>$search['minggu_mulai']],$perPage,$offset)
			);
		$search=array_filter($search, function(&$item){
			return(!empty($item));
		});
		unset($search['page']);
		$str = $this->uri->assoc_to_uri($search);        		
		$base_url = base_url()."kk/{$str}/page/";
		$data['paging']=$this->_createPaging($base_url,$total_rows,$perPage);		
		$this->load->view('header');
		$this->load->view('kk/index',$data);
		$this->load->view('footer');
	}

	public function _createPaging($base_url,$total_rows,$per_page=10){
		$this->load->library('pagination');
		$config['base_url'] = $base_url;
		$config['total_rows'] = $total_rows;
		$config['per_page'] = $per_page;
		$config['full_tag_open'] = '<nav><ul class="pagination">';
		$config['first_link'] = FALSE;
		$config['last_link'] = FALSE;
		$config['prev_tag_open'] = '<li>';
		$config['next_tag_open'] = '<li>';
		$config['num_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['next_tag_close'] = '</li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li><a>';
		$config['cur_tag_close'] = '</a></li>';
		$config['full_tag_close'] = '</ul></nsav>';
		$config['use_page_numbers'] = TRUE;
		$this->pagination->initialize($config);
		return $this->pagination->create_links();				
	}

	public function _getStatusUM(&$item){
		switch ($item['tag_kpa']) {
			case 0:
				if($item['tag_revisi']==0){
					$item['status']="Menunggu Persetujuan KPA";
				}else{
					$item['status']="Menunggu Persetujuan KPA (Sudah Revisi)";
				}
				$item['status_code']=0;
				break;
			case 1:
				$item['status']="Di Setujui KPA";
				$item['status_code']=1;
				break;
			case 2:
				$item['status_code']=2;
				$catatan=$this->mkpa->getCatatanTerbaru($item['id']);
				$item['status']='Tidak Disetujui KPA <br/><button type="button" class="btn btn-sm btn-danger" data-toggle="popover" data-trigger="focus" data-placement="top" title="Catatan" data-content="'.$this->security->xss_clean($catatan['catatan']).'">[klik untuk melihat pesan]</button>';
				break;
			
			default:
				$item['status_code']=0;
				$item['status']="Menunggu Persetujuan KPA";
				break;
		}
	}

	public function _getStatusPPK(&$item){
		switch ($item['tag_ppk']) {
			case 0:
				if($item['tag_revisi_ppk']==0){
					$item['status_ppk']="Menunggu Persetujuan PPK";
				}else{
					$item['status_ppk']="Menunggu Persetujuan PPK (Sudah Revisi)";
				}
				$item['status_ppk_code']=0;
				break;
			case 1:
				$item['status_ppk']="Lulus";
				$item['status_ppk_code']=1;
				break;
			case 2:
				$item['status_ppk_code']=2;
				$catatan=$this->mppk->getCatatanTerbaru($item['id']);
				$item['status_ppk']='Tidak Lulus <br/><button type="button" class="btn btn-sm btn-danger" data-toggle="popover" data-trigger="focus" data-placement="top" title="Catatan" data-content="'.$this->security->xss_clean($catatan['catatan']).'">[klik untuk melihat pesan]</button>';
				break;
			
			default:
				$item['status_ppk_code']=0;
				$item['status_ppk']="Menunggu Persetujuan PPK";
				break;
		}
	}

	public function add()
	{
		if ($this->auth->checkRole('Pemohon')===false)show_error('Anda Tidak Memiliki Akses Ke Halaman Ini');

		$data=array(
			'csrf' => array(
		        'name' => $this->security->get_csrf_token_name(),
		        'hash' => $this->security->get_csrf_hash()
			),
			'dataoutput'=>$this->mdipa->getAllOutput(),
			'datakegiatan'=>$this->mdipa->getByBidang(),
			'datapejabat'=>$this->db->get_where('users',array('role'=>'Pemohon'))->result_array()
			);

		$this->load->view('header');
		$this->load->view('kk/new',$data);
		$this->load->view('footer');
	}

	public function modify($id)
	{
		if ($this->auth->checkRole('Pemohon')===false)show_error('Anda Tidak Memiliki Akses Ke Halaman Ini');
		//$this->user['seksi']="-";

		$data=array(
			'csrf' => array(
		        'name' => $this->security->get_csrf_token_name(),
		        'hash' => $this->security->get_csrf_hash()
			),
			'id'=>$this->encryption->encrypt($id),
			'item'=>$this->mkk->get($id),
			'dataoutput'=>$this->mdipa->getAllOutput(),
			'datakegiatan'=>$this->mdipa->getByBidang(),
			'datapejabat'=>$this->db->get_where('users',array('role'=>'Pemohon'))->result_array()
			);
		$this->load->view('header');
		$this->load->view('kk/modify',$data);
		$this->load->view('footer');
	}

	public function create(){
		$this->form_validation->set_rules('id_output', 'Output', 'required');
		if($this->input->post('id_komponen_kegiatan')=="" && $this->input->post('id_sub_komponen_kegiatan')==""){
			$this->form_validation->set_rules('id_output', 'Komponen Kegiatan', 'required');			
		}
		$this->form_validation->set_rules('tgl_mulai', 'Tanggal Mulai', array('required','exact_length[10]'));
		$this->form_validation->set_rules('bulan_mulai', 'Bulan Mulai', array('required','exact_length[2]'));
		$this->form_validation->set_rules('minggu_mulai', 'Minggu Mulai', array('integer','less_than_equal_to[4]','greater_than_equal_to[1]'));
		$this->form_validation->set_rules('bulan_berakhir', 'Bulan Berakhir', array('required','exact_length[2]'));
		$this->form_validation->set_rules('minggu_terakhir', 'Minggu Berakhir', array('integer','less_than_equal_to[4]','greater_than_equal_to[1]'));
		if ($this->form_validation->run() == FALSE)
		{
			$this->add();
		}else{
			if($this->input->post('id_sub_komponen_kegiatan')==""){
				$id_kegiatan=$this->input->post('id_komponen_kegiatan');
			}else{
				$id_kegiatan=$this->input->post('id_sub_komponen_kegiatan');				
			}
			$tgl_mulai = date_create_from_format('d-m-Y', $this->input->post('tgl_mulai'));
			$tgl_berakhir = date_create_from_format('d-m-Y', $this->input->post('tgl_berakhir'));
			$data=array(
				'id_kegiatan'=>$id_kegiatan,
				'id_user'=>$this->input->post('id_user'),
				'tahun_anggaran'=>$this->input->post('tahun_anggaran'),
				'tgl_mulai'=>date_format($tgl_mulai, 'Y-m-d'),
				//'tgl_berakhir'=>date_format($tgl_berakhir, 'Y-m-d'),
				'bulan_mulai'=>$this->input->post('bulan_mulai'),
				'bulan_berakhir'=>$this->input->post('bulan_berakhir'),
				'minggu_mulai'=>$this->input->post('minggu_mulai'),
				'minggu_terakhir'=>$this->input->post('minggu_terakhir'),
				'keterangan'=>$this->input->post('keterangan')
				);
			$this->mkk->create($data);
			redirect('kk');
		}
	}

	public function update(){
		$rowKK=$this->mkk->get($this->encryption->decrypt($this->input->post('id')));
		if(empty($rowKK))show_404();
		$this->form_validation->set_rules('id_output', 'Output', 'required');
		if($this->input->post('id_komponen_kegiatan')=="" && $this->input->post('id_sub_komponen_kegiatan')==""){
			$this->form_validation->set_rules('id_output', 'Komponen Kegiatan', 'required');			
		}
		$this->form_validation->set_rules('tgl_mulai', 'Tanggal Mulai', array('required','exact_length[10]'));
		$this->form_validation->set_rules('bulan_mulai', 'Bulan Mulai', array('required','exact_length[2]'));
		$this->form_validation->set_rules('minggu_mulai', 'Minggu Mulai', array('integer','less_than_equal_to[4]','greater_than_equal_to[1]'));
		$this->form_validation->set_rules('bulan_berakhir', 'Bulan Berakhir', array('required','exact_length[2]'));
		$this->form_validation->set_rules('minggu_terakhir', 'Minggu Berakhir', array('integer','less_than_equal_to[4]','greater_than_equal_to[1]'));

		if ($this->form_validation->run() == FALSE)
		{
			$this->modify($rowKK['id']);
		}else{
			if($this->input->post('id_sub_komponen_kegiatan')==""){
				$id_kegiatan=$this->input->post('id_komponen_kegiatan');
			}else{
				$id_kegiatan=$this->input->post('id_sub_komponen_kegiatan');				
			}
			$tgl_mulai = date_create_from_format('d-m-Y', $this->input->post('tgl_mulai'));
			$tgl_berakhir = date_create_from_format('d-m-Y', $this->input->post('tgl_berakhir'));
			$data=array(
				'id_kegiatan'=>$id_kegiatan,
				'id_user'=>$this->input->post('id_user'),
				'tahun_anggaran'=>$this->input->post('tahun_anggaran'),
				'tgl_mulai'=>date_format($tgl_mulai, 'Y-m-d'),
				'tgl_berakhir'=>date_format($tgl_berakhir, 'Y-m-d'),
				'bulan_mulai'=>$this->input->post('bulan_mulai'),
				//'bulan_berakhir'=>$this->input->post('bulan_berakhir'),
				'minggu_mulai'=>$this->input->post('minggu_mulai'),
				'minggu_terakhir'=>$this->input->post('minggu_terakhir'),
				'keterangan'=>$this->input->post('keterangan')
				);
			$this->mkk->update($data,$rowKK['id']);
			redirect('kk');
		}
	}

	public function delete($id)
	{
		if(empty($id) )return show_404();
		$datauser = $this->mkk->delete($id);			
		redirect('kk');		
	}


}
