    <div id="global">
      <div class="container-fluid">
        <div class="row cm-fix-height">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">Upload Tanda Terima</div>
                <div class="panel-body">
                      <dl class="dl-horizontal">
                        <dt>Tahun Anggaran</dt>
                        <dd>: <?=$item['tahun_anggaran'] ?></dd>
                        <dt>Kegiatan</dt>
                        <dd>: <?=$item['kegiatan']?></dd>
                        <dt>Waktu</dt>
                        <dd>: <?=$item['waktu'] ?></dd>
                        <dt>Lokasi</dt>
                        <dd>: <?=$item['lokasi'] ?></dd>
                        <dt>Jumlah Peserta</dt>
                        <dd>: <?=$item['jumlah_peserta'] ?></dd>
                        <dt>Pejabat Terkait</dt>
                        <dd>: <?=$item['surname'] ?></dd>
                        <dt>Tanggal</dt>
                        <dd>: <?=$item['tanggal_pengajuan'] ?></dd>
                      </dl>
                        <hr></hr>                
                    <form class="form-horizontal" enctype="multipart/form-data" method="post" action="<?=base_url('bendahara/upload') ?>">
                        <input type="hidden" name="<?=$csrf['name'] ?>" value="<?=$csrf['hash'] ?>">
                        <input type="hidden" name="id" value="<?=$id?>">
                        <input type="hidden" name="id_tanda_terima" value="<?=$id_tanda_terima?>">
                        <div class="form-group ">
                            <label for="userfile" class="col-sm-2 control-label">File SPM (JPG,PNG,PDF)</label>
                            <div class="col-sm-10">
                                <input type="file" name="userfile" id="userfile" />
                                <span class="help-block has-error"><?php if(!empty($this->session->flashdata('error_upload')))echo $this->session->flashdata('error_upload'); ?></span>
                            </div>
                        </div>
                        <div class="form-group" style="margin-bottom:0">
                            <div class="col-sm-offset-2 col-sm-10 text-right">
                                <button type="reset" class="btn btn-default">Cancel</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
      </div>
      <footer class="cm-footer"><span class="pull-right">&copy;</span></footer>
    </div>
    </div>