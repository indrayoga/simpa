    <div id="global">
      <div class="container-fluid">
        <div class="row cm-fix-height">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">Edit Pengajuan UM Kegiatan</div>
                <div class="panel-body">
                    <form class="form-horizontal" method="post" action="<?=base_url('bendahara/update') ?>">
                        <input type="hidden" name="<?=$csrf['name'] ?>" value="<?=$csrf['hash'] ?>">
                        <input type="hidden" name="id" value="<?=$id?>">
                        <div class="form-group <?php if(!empty(form_error('tahun_anggaran')))echo "has-error"; ?>">
                            <label for="tahun_anggaran" class="col-sm-2 control-label">Tahun Anggaran</label>
                            <div class="col-sm-10">
                                <input type="text" maxlength="4" readonly class="form-control" required id="tahun_anggaran" name="tahun_anggaran" placeholder="tahun" value="<?php if(!empty($item['tahun_anggaran']))echo $item['tahun_anggaran']; else echo date('Y'); ?>">
                                <span class="help-block has-error"><?php echo form_error('tahun_anggaran'); ?></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="program" class="col-sm-2 control-label">Program</label>
                            <div class="col-sm-10">
                                <input type="text" disabled class="form-control" value="PROGRAM PENINGKATAN PELAYANAN DAN PENEGEKAN HUKUM KEIMIGRASIAN">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="program" class="col-sm-2 control-label">Satker</label>
                            <div class="col-sm-10">
                                <input type="text" disabled class="form-control" value="KANTOR IMIGRASI KELAS I KHUSUS JAKARTA BARAT">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="program" class="col-sm-2 control-label">Bidang</label>
                            <div class="col-sm-10">
                                <input type="text" disabled class="form-control" value="<?=$this->user['seksi']?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="id_output" class="col-sm-2 control-label">Output</label>
                            <div class="col-sm-10">
                                <select name="id_output" class="form-control" id="id_output">
                                    <option value="">Pilih Output</option>
                                <?php
                                    foreach ($dataoutput as $output) {
                                        if($output['id_dipa']==$kegiatan[0]['id_dipa'])$sel="selected=selected"; else $sel="";
                                    ?>
                                    <option value="<?=$output['id_dipa']?>" <?=$sel?>><?=$output['kegiatan']?></option>
                                    <?php
                                    }
                                ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group" id="divsuboutput" style="<?=(!isset($datasuboutput)?"display:none;":"display:block;")?>">
                            <label for="id_sub_output" class="col-sm-2 control-label">Sub Output</label>
                            <div class="col-sm-10">
                                <select name="id_sub_output" class="form-control" id="id_sub_output">
                                    <option value="">Pilih Sub Output</option>
                                    <?php
                                    if(isset($datasuboutput)){
                                        foreach ($datasuboutput as $suboutput){
                                        if($suboutput['id_dipa']==$kegiatan[1]['id_dipa'])$sel="selected=selected"; else $sel="";
                                        ?>
                                        <option value="<?=$suboutput['id_dipa']?>" <?=$sel?> ><?=$suboutput['kegiatan']?></option>
                                        <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group" id="divkomponenkegiatan" style="<?=(!isset($datakomponenkegiatan)?"display:none;":"display:block;")?>">
                            <label for="id_komponen_kegiatan" class="col-sm-2 control-label">Komponen Kegiatan</label>
                            <div class="col-sm-10">
                                <select name="id_komponen_kegiatan" class="form-control" id="id_komponen_kegiatan">
                                    <option value="">Pilih Komponen Kegiatan</option>
                                    <?php
                                    if(isset($datakomponenkegiatan)){
                                        foreach ($datakomponenkegiatan as $komponenkegiatan){
                                        if($komponenkegiatan['id_dipa']==$kegiatan[1]['id_dipa'] || $komponenkegiatan['id_dipa']==$kegiatan[2]['id_dipa'])$sel="selected=selected"; else $sel="";
                                        ?>
                                        <option value="<?=$komponenkegiatan['id_dipa']?>" <?=$sel?>><?=$komponenkegiatan['kegiatan']?></option>
                                        <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group" id="divsubkomponenkegiatan" style="<?=(!isset($datakegiatan)?"display:none;":"display:block;")?>">
                            <label for="id_sub_komponen_kegiatan" class="col-sm-2 control-label">Kegiatan</label>
                            <div class="col-sm-10">
                                <select name="id_sub_komponen_kegiatan" class="form-control" id="id_sub_komponen_kegiatan">
                                    <?php
                                    if(isset($datakegiatan)){
                                        if(!isset($kegiatan[3]))$kegiatan[3]['id_dipa']="";
                                        foreach ($datakegiatan as $keg){
                                        if($keg['id_dipa']==$kegiatan[2]['id_dipa'] || $keg['id_dipa']==$kegiatan[3]['id_dipa'])$sel="selected=selected"; else $sel="";
                                        ?>
                                        <option value="<?=$keg['id_dipa']?>" <?=$sel?>><?=$keg['kegiatan']?></option>
                                        <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group <?php if(!empty(form_error('tanggal')))echo "has-error"; ?>">
                            <label for="tanggal" class="col-sm-2 control-label">Tanggal</label>
                            <div class="col-sm-10">
                                <input type="date" maxlength="10" data-mask="99-99-9999" class="form-control" required id="tanggal" name="tanggal" placeholder="tanggal" value="<?php echo $item['tanggal']; ?>">
                                <span class="help-block has-error"><?php echo form_error('tanggal'); ?></span>
                            </div>
                        </div>
                        <div class="form-group <?php if(!empty(form_error('jam')))echo "has-error"; ?>">
                            <label for="jam" class="col-sm-2 control-label">Jam</label>
                            <div class="col-sm-10">
                                <input type="text" maxlength="8" class="form-control" id="jam" name="jam" placeholder="tanggal" value="<?php echo $item['jam']; ?>">
                                <span class="help-block has-error"><?php echo form_error('jam'); ?></span>
                            </div>
                        </div>
                        <div class="form-group <?php if(!empty(form_error('lokasi')))echo "has-error"; ?>">
                            <label for="lokasi" class="col-sm-2 control-label">Lokasi</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" required id="lokasi" name="lokasi" placeholder="lokasi" value="<?php echo $item['lokasi']; ?>">
                                <span class="help-block has-error"><?php echo form_error('lokasi'); ?></span>
                            </div>
                        </div>
                        <div class="form-group <?php if(!empty(form_error('jumlah_peserta')))echo "has-error"; ?>">
                            <label for="jumlah_peserta" class="col-sm-2 control-label">Jumlah Peserta</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="jumlah_peserta" name="jumlah_peserta" placeholder="jumlah_peserta" value="<?php echo $item['jumlah_peserta']; ?>">
                                <span class="help-block has-error"><?php echo form_error('jumlah_peserta'); ?></span>
                            </div>
                        </div>
                        <div class="form-group <?php if(!empty(form_error('jumlah_peserta')))echo "has-error"; ?>">
                            <div class="col-sm-12">
                                <table class="table table-bordered table-hover table-striped md-data-table " id="tableAkun">
                                    <thead>
                                        <tr>
                                            <th>Akun</th>
                                            <th>Detil Belanja</th>
                                            <th>Vol</th>
                                            <th>Satuan</th>
                                            <th>Harga</th>
                                            <th>#</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach ($datarab as $rab) {
                                        # code...
                                    ?>
                                        <tr>
                                            <td>
                                                <select name="kode_akun[]" class="form-control akun" >
                                                <?php
                                                    foreach ($dataakun as $akun) {
                                                        if($rab['kode_akun']==$akun['kode_akun'])$sel="selected=selected"; else $sel="";
                                                    ?>
                                                    <option value="<?=$akun['kode_akun']?>" <?php echo $sel; ?>><?=$akun['nama_akun']?></option>
                                                    <?php
                                                    }
                                                ?>
                                                </select>
                                            </td>
                                            <td class=" col-sm-6"><input type="text" class="form-control detil_belanja" name="detil_belanja[]" placeholder="" value="<?=$rab['detil_belanja']?>"></td>
                                            <td class=" col-sm-1"><input type="number" class="form-control vol" name="vol[]" placeholder="" value="<?=$rab['vol']?>"></td>
                                            <td><input type="text" class="form-control satuan" name="satuan[]" placeholder="" value="<?=$rab['satuan']?>"></td>
                                            <td>
                                                <input type="text" class="form-control harga" name="harga[]" placeholder="" value="<?=$rab['harga_satuan']?>">
                                                <input type="hidden" class="form-control total" name="total[]" placeholder="" value="<?=$rab['vol']*$rab['harga_satuan']?>">
                                            </td>
                                            <td><img src="<?=base_url()?>assets/img/sf/sign-error.svg" class="hapusBarisAkun" width="24" height="24" alt="hapus"></td>
                                        </tr>                                        

                                    <?php
                                    }
                                    ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th colspan="4">T O T A L</th>
                                            <th id="totalAll">Rp. 0</th>
                                        </tr>                                    
                                        <tr>
                                            <th colspan="6" style="text-align: center;">
                                                <button type="button" id="tambahBarisAkun" class="btn btn-info">Tambah</button>
                                            </th>
                                        </tr>
                                    </tfoot>                                    
                                </table>
                            </div>
                        </div>
                        <div class="form-group" style="margin-bottom:0">
                            <div class="col-sm-offset-2 col-sm-10 text-right">
                                <button type="reset" class="btn btn-default">Cancel</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
      </div>
      <footer class="cm-footer"><span class="pull-right">&copy;</span></footer>
    </div>
    </div>
<script type="text/javascript">
$(document).ready(function(){
  $('#tahun_anggaran').mask('0000');
  $('#tanggal').mask('00-00-0000');
  $('#jam').mask('00:00:00');
    $('#tambahBarisAkun').click(function(){
        $('#tableAkun tbody tr').last().clone().appendTo('#tableAkun tbody');
        $('#tableAkun tbody tr').last().find('input').val('');
    });

    $('#tableAkun').on('change','.harga', function() {
        total=$(this).val()*$(this).parent("td").siblings('td').children('.vol').val();
        total=parseInt(total);
        $(this).siblings('.total').val(total);
        $('.total').trigger('change');
    });

    $('#tableAkun').on('change','.vol', function() {
        total=$(this).val()*$(this).parent("td").siblings('td').children('.harga').val();
        total=parseInt(total);
        $(this).parent("td").siblings('td').children('.total').val(total);
        $('.total').trigger('change');
    });

    $('#tableAkun').on('change','.total', function() {
        total=0;
        $('.total').each(function(index, el) {
            total=total+parseInt($(this).val());
        });
        total=number_format(total,0,'','.');
        $('#totalAll').text(total);
    });

    $('#tableAkun').on('click','.hapusBarisAkun', function() {
        $(this).parent().parent().remove();
    });

    $('#id_output').change(function(){
        $.ajax({
            url: base_url+'ajax/suboutput',
            type: 'POST',
            dataType: 'json',
            data: {id_output: $(this).val()},

        })
        .done(function(data) {
            $.each(data, function(index, val) {
                 /* iterate through array or object */
                if(val['kode_kegiatan'].length>3){
                    $('#id_sub_output').append('<option value="'+val['id_dipa']+'">'+val['kegiatan']+'</option>');
                    $('#divsuboutput').show();
                }else{
                    $('#id_komponen_kegiatan').append('<option value="'+val['id_dipa']+'">'+val['kegiatan']+'</option>');
                    $('#divkomponenkegiatan').show();
                }
            });
        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });
        
    });

    $('#id_sub_output, #id_komponen_kegiatan').change(function(){
        $.ajax({
            url: base_url+'ajax/subkomponen',
            type: 'POST',
            dataType: 'json',
            data: {id_induk: $(this).val()},

        })
        .done(function(data) {
            if(data.length==0)$('#divsubkomponenkegiatan').hide();
            $.each(data, function(index, val) {
                 /* iterate through array or object */
                if(val['kode_kegiatan'].length>3){
                    $('#id_komponen_kegiatan').append('<option value="'+val['id_dipa']+'">'+val['kegiatan']+'</option>');
                    $('#divkomponenkegiatan').show();
                }else{
                    $('#id_sub_komponen_kegiatan').append('<option value="'+val['id_dipa']+'">'+val['kegiatan']+'</option>');
                    $('#divsubkomponenkegiatan').show();
                }
            });
        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });
        
    });

    $('#id_komponen_kegiatan, #id_sub_komponen_kegiatan').change(function(){
        $.ajax({
            url: base_url+'ajax/akunkegiatan',
            type: 'POST',
            dataType: 'json',
            data: {id: $(this).val()},

        })
        .done(function(data) {
            $('.akun').empty();
            $.each(data, function(index, val) {
                $('.akun').append('<option value="'+val['kode_kegiatan']+'">'+val['kegiatan']+'</option>');
            });
        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });
        
    });
    $('.total').trigger('change');
});
</script>