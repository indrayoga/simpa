    <div id="global">
      <div class="container-fluid">
        <div class="row cm-fix-height">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">Cetak Kwintasi Kegiatan</div>
                <div class="panel-body">
                    <form class="form-horizontal" method="post" action="<?=base_url('bendahara/simpankwitansi') ?>">
                        <input type="hidden" name="<?=$csrf['name'] ?>" value="<?=$csrf['hash'] ?>">
                        <input type="hidden" name="id" value="<?=$id?>">
                        <div class="form-group <?php if(!empty(form_error('tahun_anggaran')))echo "has-error"; ?>">
                            <label for="tahun_anggaran" class="col-sm-2 control-label">Tahun Anggaran</label>
                            <div class="col-sm-10">
                                <input type="text" maxlength="4" disabled class="form-control" required id="tahun_anggaran" name="tahun_anggaran" placeholder="tahun" value="<?php if(!empty($item['tahun_anggaran']))echo $item['tahun_anggaran']; else echo date('Y'); ?>">
                                <span class="help-block has-error"><?php echo form_error('tahun_anggaran'); ?></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="program" class="col-sm-2 control-label">Program</label>
                            <div class="col-sm-10">
                                <input type="text" disabled class="form-control" value="PROGRAM PENINGKATAN PELAYANAN DAN PENEGEKAN HUKUM KEIMIGRASIAN">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="program" class="col-sm-2 control-label">Satker</label>
                            <div class="col-sm-10">
                                <input type="text" disabled class="form-control" value="KANTOR IMIGRASI KELAS I KHUSUS JAKARTA BARAT">
                            </div>
                        </div>                        
                        <div class="form-group <?php if(!empty(form_error('id_kegiatan')))echo "has-error"; ?>">
                            <label for="id_kalender_kegiatan" class="col-sm-2 control-label">Kegiatan</label>
                            <div class="col-sm-10">
                                <select name="id_kalender_kegiatan" disabled required class="form-control" id="id_kalender_kegiatan">
                                    <option value="">Pilih Kegiatan</option>
                                <?php
                                    foreach ($datakegiatan as $kegiatan) {
                                        # code...
                                        if($kegiatan['id_dipa']==$item['id_dipa'])$sel="selected=selected"; else $sel="";
                                    ?>
                                    <option value="<?=$kegiatan['id_dipa']?>" <?=$sel;?> ><?=$kegiatan['kegiatan']?></option>
                                    <?php
                                    }
                                ?>
                                </select>
                                <span class="help-block has-error"><?php echo form_error('id_kalender_kegiatan'); ?></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="tanggal" class="col-sm-2 control-label">Tanggal</label>
                            <div class="col-sm-10">
                                <input type="date" disabled maxlength="10" data-mask="99-99-9999" class="form-control" required id="tanggal" placeholder="tanggal" value="<?php echo $item['tanggal']; ?>">
                                <span class="help-block has-error"></span>
                            </div>
                        </div>
                        <div class="form-group <?php if(!empty(form_error('jam')))echo "has-error"; ?>">
                            <label for="jam" class="col-sm-2 control-label">Jam</label>
                            <div class="col-sm-10">
                                <input type="text" disabled maxlength="8" class="form-control" id="jam" name="jam" placeholder="tanggal" value="<?php echo $item['jam']; ?>">
                                <span class="help-block has-error"><?php echo form_error('jam'); ?></span>
                            </div>
                        </div>
                        <div class="form-group <?php if(!empty(form_error('lokasi')))echo "has-error"; ?>">
                            <label for="lokasi" class="col-sm-2 control-label">Lokasi</label>
                            <div class="col-sm-10">
                                <input type="text" disabled class="form-control" required id="lokasi" name="lokasi" placeholder="lokasi" value="<?php echo $item['lokasi']; ?>">
                                <span class="help-block has-error"><?php echo form_error('lokasi'); ?></span>
                            </div>
                        </div>
                        <div class="form-group <?php if(!empty(form_error('jumlah_peserta')))echo "has-error"; ?>">
                            <label for="jumlah_peserta" class="col-sm-2 control-label">Jumlah Peserta</label>
                            <div class="col-sm-10">
                                <input type="text" disabled class="form-control" id="jumlah_peserta" name="jumlah_peserta" placeholder="jumlah_peserta" value="<?php echo $item['jumlah_peserta']; ?>">
                                <span class="help-block has-error"><?php echo form_error('jumlah_peserta'); ?></span>
                            </div>
                        </div>
                        <div class="form-group <?php if(!empty(form_error('jumlah_peserta')))echo "has-error"; ?>">
                            <div class="col-sm-12">
                                <table class="table table-bordered table-hover table-striped md-data-table " id="tableAkun">
                                    <thead>
                                        <tr>
                                            <th>Akun</th>
                                            <th>Detil Belanja</th>
                                            <th>Vol</th>
                                            <th>Satuan</th>
                                            <th>Harga</th>
                                            <th>Jumlah</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $total=0;
                                    foreach ($datarab as $rab) {
                                        # code...
                                        $total=$total+$rab['jumlah'];
                                    ?>
                                        <tr>
                                            <td><?=$rab['kode_akun']?></td>
                                            <td class=" col-sm-6"><?=$rab['detil_belanja']?></td>
                                            <td class=" col-sm-1"><?=$rab['vol']?></td>
                                            <td><?=$rab['satuan']?></td>
                                            <td style="text-align: right;"><?=number_format($rab['harga_satuan'],0,'','.')?></td>
                                            <td style="text-align: right;"><?=number_format($rab['jumlah'],0,'','.')?></td>
                                        </tr>                                        

                                    <?php
                                    }
                                    ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th colspan="5" style="text-align: right;">
                                                T O T A L
                                            </th>
                                            <th style="text-align: right;"><?=number_format($total,0,'','.')?></th>
                                        </tr>
                                    </tfoot>                                    
                                </table>
                            </div>
                        </div>                        
                        <div class="form-group <?php if(!empty(form_error('tanggal')))echo "has-error"; ?>">
                            <label for="tanggal" class="col-sm-2 control-label">Tanggal</label>
                            <div class="col-sm-10">
                                <input type="text" data-toggle="datepicker" name="tanggal" id="tanggal" value="" placeholder="" class="form-control">
                                <span class="help-block has-error"><?php echo form_error('tanggal'); ?></span>
                            </div>
                        </div>                        
                        <div class="form-group <?php if(!empty(form_error('tanggal_disetujui')))echo "has-error"; ?>">
                            <label for="tanggal_disetujui" class="col-sm-2 control-label">Tanggal disetujui</label>
                            <div class="col-sm-10">
                                <input type="text" data-toggle="datepicker" name="tanggal_disetujui" id="tanggal_disetujui" value="" placeholder="" class="form-control">
                                <span class="help-block has-error"><?php echo form_error('tanggal_disetujui'); ?></span>
                            </div>
                        </div>                        
                        <div class="form-group <?php if(!empty(form_error('tanggal_diterima')))echo "has-error"; ?>">
                            <label for="tanggal_diterima" class="col-sm-2 control-label">Tanggal diterima</label>
                            <div class="col-sm-10">
                                <input type="text" data-toggle="datepicker" name="tanggal_diterima" id="tanggal_diterima" value="" placeholder="" class="form-control">
                                <span class="help-block has-error"><?php echo form_error('tanggal_diterima'); ?></span>
                            </div>
                        </div>                        
                        <div class="form-group <?php if(!empty(form_error('nomor')))echo "has-error"; ?>">
                            <label for="nomor" class="col-sm-2 control-label">Nomor</label>
                            <div class="col-sm-10">
                                <input type="text" name="nomor" id="nomor" value="" placeholder="" class="form-control">
                                <span class="help-block has-error"><?php echo form_error('nomor'); ?></span>
                            </div>
                        </div>                        
                        <div class="form-group <?php if(!empty(form_error('kepada')))echo "has-error"; ?>">
                            <label for="kepada" class="col-sm-2 control-label">Kepada</label>
                            <div class="col-sm-10">
                                <input type="hidden" name="kepada" id="kepada" value="<?=$item['id_user']?>" placeholder="" class="form-control">
                                <input type="text" readonly value="<?=$item['surname']?>" placeholder="" class="form-control">
                                <span class="help-block has-error"><?php echo form_error('kepada'); ?></span>
                            </div>
                        </div>                        
                        <div class="form-group <?php if(!empty(form_error('untuk_pembayaran')))echo "has-error"; ?>">
                            <label for="untuk_pembayaran" class="col-sm-2 control-label">Untuk Pembayaran</label>
                            <div class="col-sm-10">
                                <input type="text" name="untuk_pembayaran" id="untuk_pembayaran" value="" placeholder="" class="form-control">
                                <span class="help-block has-error"><?php echo form_error('untuk_pembayaran'); ?></span>
                            </div>
                        </div>                        
                        <div class="form-group <?php if(!empty(form_error('jumlah')))echo "has-error"; ?>">
                            <label for="status" class="col-sm-2 control-label">Jumlah</label>
                            <div class="col-sm-10">
                                <input type="text" name="jumlah" value="<?=$total?>" placeholder="" class="form-control">
                                <span class="help-block has-error"><?php echo form_error('jumlah'); ?></span>
                            </div>
                        </div>                        
                        <div class="form-group <?php if(!empty(form_error('bendahara')))echo "has-error"; ?>">
                            <label for="bendahara" class="col-sm-2 control-label">Bendahara</label>
                            <div class="col-sm-10">
                                <input type="hidden" name="bendahara" value="<?=$this->user['id']?>" placeholder="" class="form-control">
                                <input type="text" value="<?=$this->user['surname']?>" placeholder="" class="form-control">
                                <span class="help-block has-error"><?php echo form_error('bendahara'); ?></span>
                            </div>
                        </div>                        
                        <div class="form-group <?php if(!empty(form_error('ppk')))echo "has-error"; ?>">
                            <label for="ppk" class="col-sm-2 control-label">PPK</label>
                            <div class="col-sm-10">
                            <select name="ppk" id="ppk" class="form-control">
                                <?php
                                foreach ($datappk as $ppk) {
                                    # code...
                                ?>
                                <option value="<?=$ppk['id']?>"><?=$ppk['surname']?></option>
                                <?php
                                }
                                ?>
                            </select>
                                <span class="help-block has-error"><?php echo form_error('ppk'); ?></span>
                            </div>
                        </div>                        
                        <div class="form-group" style="margin-bottom:0">
                            <div class="col-sm-offset-2 col-sm-10 text-right">
                                <button type="submit" class="btn btn-primary">Simpan &amp; Download Kwitansi</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
      </div>
      <footer class="cm-footer"><span class="pull-right">&copy;</span></footer>
    </div>
    </div>
<script type="text/javascript">
$(document).ready(function(){
  $('#tanggal, #tanggal_disetujui, #tanggal_diterima').mask('00-00-0000');
});
$(function () {
    $('[data-toggle="datepicker"]').datepicker({
      format: 'dd-mm-yyyy'
    });
});
</script>
