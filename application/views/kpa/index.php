    <div id="global">
      <div class="container-fluid cm-container-white">
        <hr></hr>
          <form class="form-horizontal" action="<?php echo base_url('kpa/search'); ?>" method="post" accept-charset="utf-8">
            <input type="hidden" name="<?=$csrf['name'] ?>" value="<?=$csrf['hash'] ?>">
            <div class="form-group <?php if(!empty(form_error('id_kegiatan')))echo "has-error"; ?>">
                <label for="id_kegiatan" class="col-sm-2 control-label">Kegiatan</label>
                <div class="col-sm-10">
                    <select name="id_kegiatan" class="form-control" id="id_kegiatan">
                        <option value="">Pilih Kegiatan</option>
                    <?php
                        foreach ($datakegiatan as $kegiatan) {
                            # code...
                            if($kegiatan['id_dipa']==$search['kegiatan'])$sel="selected=selected"; else $sel="";
                        ?>
                        <option value="<?=$kegiatan['id_dipa']?>" <?=$sel;?> ><?=$kegiatan['kegiatan']?></option>
                        <?php
                        }
                    ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="waktu" class="col-sm-2 control-label">Waktu</label>
                <div class="col-sm-10">
                    <input type="date" maxlength="10" data-mask="99-99-9999" class="form-control" id="waktu" name="waktu" placeholder="waktu" value="<?php echo $search['waktu']; ?>">
                    <span class="help-block has-error"></span>
                </div>
            </div>
            <div class="form-group">
                <label for="lokasi" class="col-sm-2 control-label">Lokasi</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="lokasi" name="lokasi" placeholder="lokasi" value="<?php echo $search['lokasi']; ?>">
                    <span class="help-block has-error"></span>
                </div>
            </div>
            <div class="form-group">
                <label for="lokasi" class="col-sm-2 control-label">Status</label>
                <div class="col-sm-10">
                    <select name="tag_kpa" id="tag_kpa" class="form-control">
                      <option value="">Status</option>
                      <option value="1" <?=($search['tag_kpa']==1)?"selected=selected":"";?>>Disetujui</option>
                      <option value="2" <?=($search['tag_kpa']==2)?"selected=selected":"";?>>Tidak disetujui</option>
                    </select>
                </div>
            </div>
            <div class="form-group" style="margin-bottom:0">
                <div class="col-sm-offset-2 col-sm-10 text-right">
                    <button type="reset" class="btn btn-default">Reset</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
          </form>        
        <hr></hr>
        <table class="table table-bordered table-hover table-striped md-data-table " id="tableKecamatan">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Kode</th>
                    <th>Kegiatan</th>
                    <th>Pejabat</th>
                    <th>Waktu</th>
                    <th>Lokasi</th>
                    <th>Peserta</th>
                    <th>Biaya</th>
                    <th>Status</th>
                    <th>Pilihan</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $no=1;
                foreach ($items as $item) {
                    if($item['status_code']==1){
                      $class="info";
                    }else if($item['status_code']==2){
                      $class="danger";
                    }else{
                      $class="";
                    }
                  ?>
                  <tr class="<?=$class?>">
                      <td style="text-align:center"><?=$no?></td>
                      <td><?=$item['kode_kegiatan']?></td>
                      <td><?=$item['kegiatan']?></td>
                      <td><?=$item['surname']?></td>
                      <td><?=$item['waktu']?></td>
                      <td><?=$item['lokasi']?></td>
                      <td><?=$item['jumlah_peserta']?></td>
                      <td><?=number_format($item['jumlah'],0,'','.')?></td>
                      <td><?=$item['status']?></td>
                      <td>
                        <div class="dropdown">
                        <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Pilihan
                        <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                          <li><a href="<?=base_url("kpa/detail/{$item['id']}")?>">Detail</a></li>
                          <li><a href="<?=base_url("history/kegiatan/{$item['id']}")?>">History</a></li>
                        </ul>
                        </div>
                      </td>
                  </tr>
                  <?php
                  $no++;
                }
                ?>
            </tbody>
            <tfoot>
              <tr colspan="12"><?=$paging?></tr>
            </tfoot>            
        </table>
      </div>
      <footer class="cm-footer"><span class="pull-right">&copy;</span></footer>
    </div>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/jquery.dataTables.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/dataTables.material.min.css') ?>">
    <script src="<?php echo base_url('assets/js/jquery.dataTables.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/dataTables.material.min.js'); ?>"></script>
