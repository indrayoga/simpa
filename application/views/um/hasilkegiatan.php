    <div id="global">
      <div class="container-fluid">
        <div class="row cm-fix-height">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">Upload Hasil Kegiatan</div>
                <div class="panel-body">
                      <dl class="dl-horizontal">
                        <dt>Tahun Anggaran</dt>
                        <dd>: <?=$item['tahun_anggaran'] ?></dd>
                        <dt>Kegiatan</dt>
                        <dd>: <?=$item['kegiatan']?></dd>
                        <dt>Waktu</dt>
                        <dd>: <?=$item['waktu'] ?></dd>
                        <dt>Lokasi</dt>
                        <dd>: <?=$item['lokasi'] ?></dd>
                        <dt>Jumlah Peserta</dt>
                        <dd>: <?=$item['jumlah_peserta'] ?></dd>
                        <dt>Pejabat Terkait</dt>
                        <dd>: <?=$item['surname'] ?></dd>
                        <dt>Tanggal</dt>
                        <dd>: <?=$item['tanggal_pengajuan'] ?></dd>
                      </dl>
                        <hr></hr>                
                    <form class="form-horizontal" enctype="multipart/form-data" method="post" action="<?=base_url('um/upload') ?>">
                        <input type="hidden" name="<?=$csrf['name'] ?>" value="<?=$csrf['hash'] ?>">
                        <input type="hidden" name="id" value="<?=$id?>">
                        <input type="hidden" name="id_hasil_kegiatan" value="<?=$id_hasil_kegiatan?>">
                        <div class="form-group ">
                            <label for="jenis" class="col-sm-2 control-label">Jenis Laporan</label>
                            <div class="col-sm-10">
                                <select name="jenis">
                                    <option value="1">Resume</option>
                                    <option value="2">Kwitansi</option>
                                    <option value="3">Tanda Terima</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="userfile" class="col-sm-2 control-label">(JPG,PNG,PDF,zip)</label>
                            <div class="col-sm-10">
                                <input type="file" name="userfile" id="userfile" />
                                <span class="help-block has-error"><?php if(!empty($this->session->flashdata('error_upload')))echo $this->session->flashdata('error_upload'); ?></span>
                            </div>
                        </div>
                        <div class="form-group" style="margin-bottom:0">
                            <div class="col-sm-offset-2 col-sm-10 text-right">
                                <button type="reset" class="btn btn-default">Cancel</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
      </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Daftar File Hasil kegiatan</div>
                            <table class="table table-bordered table-hover table-striped md-data-table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Jenis</th>
                                        <th>File</th>
                                        <th>Pilihan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no=1;
                                    foreach ($items as $hasil){
                                    ?>
                                    <tr>
                                        <th scope="row"><?=$no?></th>
                                        <td><?php if($hasil['jenis']==1)echo "Resume";else if($hasil['jenis']==2)echo "Kwitansi"; else echo "Tanda Terima"; ?></td>
                                        <td><?=$hasil['namafile']?></td>
                                        <td>
                                            <div class="dropdown">
                                            <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Pilihan
                                            <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu" aria-labelledby="dLabel">
                                            <li>
                                            <a target="_newtab" href="<?=base_url("uploads/{$hasil['namafile']}")?>">Download</a>
                                            </li>
                                            <li>
                                            <a href="#" onclick="app_confirm('<?=base_url('um/deletehasilkegiatan/'.$hasil['id'].'/'.$hasil['id_pengajuan_um'])?>','Apakah Yakin Ingin Menghapus Data Ini?')" >Hapus</a>
                                            </li>
                                            </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $no++;
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>                    

      <footer class="cm-footer"><span class="pull-right">&copy;</span></footer>
    </div>
    </div>