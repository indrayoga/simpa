    <div id="global">
      <div class="container-fluid">
        <div class="row cm-fix-height">
            <div class="alert alert-info alert-dismissible fade in shadowed" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                <i class="fa fa-fw fa-info-circle"></i>Anda tidak bisa membuat pengajuan UM baru, dikarenakan masih ada kegiatan anda yang belum dinyatakan <strong>SELESAI</strong> oleh PPK
            </div>
      </div>
      <footer class="cm-footer"><span class="pull-right">&copy;</span></footer>
    </div>
    </div>
