    <div id="global">
      <div class="container-fluid cm-container-white">
        <hr></hr>
        <table class="table table-bordered table-hover table-striped md-data-table " id="tableKecamatan">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Kode</th>
                    <th>Kegiatan</th>
                    <th>Pejabat</th>
                    <th>Waktu</th>
                    <th>Lokasi</th>
                    <th>Peserta</th>
                    <th>Biaya</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $no=1;
                foreach ($items as $item) {
                  ?>
                  <tr>
                      <td style="text-align:center"><?=$no?></td>
                      <td><?=$item['kode_kegiatan']?></td>
                      <td><?=$item['kegiatan']?></td>
                      <td><?=$item['surname']?></td>
                      <td><?=$item['waktu']?></td>
                      <td><?=$item['lokasi']?></td>
                      <td><?=$item['jumlah_peserta']?></td>
                      <td><?=number_format($item['jumlah'],0,'','.')?></td>
                  </tr>
                  <?php
                  $no++;
                }
                ?>
            </tbody>
        </table>
      </div>
      <footer class="cm-footer"><span class="pull-right">&copy;</span></footer>
    </div>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/jquery.dataTables.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/dataTables.material.min.css') ?>">
    <script src="<?php echo base_url('assets/js/jquery.dataTables.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/dataTables.material.min.js'); ?>"></script>
      <script src="<?php echo base_url('assets/js/demo/popovers-tooltips.js'); ?>"></script>

