    <div id="global">
      <div class="container-fluid">
        <div class="row cm-fix-height">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">History Pesan</div>
                <div class="panel-body">
                    <hr></hr>
                    <table class="table table-bordered table-hover table-striped md-data-table " id="tableKecamatan">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Tanggal</th>
                                <th>Pesan</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no=1;
                            foreach ($items as $item) {
                              ?>
                              <tr class="">
                                  <td style="text-align:center"><?=$no?></td>
                                  <td><?=$item['tgl_dibuat']?></td>
                                  <td><?=$item['pesan']?></td>
                              </tr>
                              <?php
                              $no++;
                            }
                            ?>
                        </tbody>
                    </table>                    
                </div>
            </div>
        </div>
      </div>
      <footer class="cm-footer"><span class="pull-right">&copy;</span></footer>
    </div>
    </div>
<script type="text/javascript">
$(document).ready(function(){
  $('#tahun_anggaran').mask('0000');
  $('#tanggal').mask('00-00-0000');
  $('#jam').mask('00:00:00');
});
</script>
