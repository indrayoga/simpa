    <div id="global">
      <div class="container-fluid">
        <div class="row cm-fix-height">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">History Pengajuan UM Kegiatan <?=$item['kegiatan']?> Tahun Anggaran <?=$item['tahun_anggaran']?></div>
                <div class="panel-body">
                  <dl class="dl-horizontal">
                    <dt>Tahun Anggaran</dt>
                    <dd>: <?=$item['tahun_anggaran'] ?></dd>
                    <dt>Kegiatan</dt>
                    <dd>: <?=$item['kegiatan']?></dd>
                    <dt>Waktu</dt>
                    <dd>: <?=$item['waktu'] ?></dd>
                    <dt>Lokasi</dt>
                    <dd>: <?=$item['lokasi'] ?></dd>
                    <dt>Jumlah Peserta</dt>
                    <dd>: <?=$item['jumlah_peserta'] ?></dd>
                    <dt>Pejabat Terkait</dt>
                    <dd>: <?=$item['surname'] ?></dd>
                    <dt>Tanggal</dt>
                    <dd>: <?=$item['tanggal_pengajuan'] ?></dd>
                  </dl>
                    <hr></hr>
                    <table class="table table-bordered table-hover table-striped md-data-table " id="tableKecamatan">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Tanggal</th>
                                <th>Level</th>
                                <th>Persetujuan</th>
                                <th>Catatan</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no=1;
                            foreach ($datakpa as $kpa) {
                              ?>
                              <tr class="">
                                  <td style="text-align:center"><?=$no?></td>
                                  <td><?=$kpa['tanggal']?></td>
                                  <td><?=$kpa['level']?></td>
                                  <td><?=$kpa['status_pesan']?></td>
                                  <td><?=$kpa['catatan']?></td>
                              </tr>
                              <?php
                              $no++;
                            }
                            ?>
                        </tbody>
                    </table>                    
                </div>
            </div>
        </div>
      </div>
      <footer class="cm-footer"><span class="pull-right">&copy;</span></footer>
    </div>
    </div>
<script type="text/javascript">
$(document).ready(function(){
  $('#tahun_anggaran').mask('0000');
  $('#tanggal').mask('00-00-0000');
  $('#jam').mask('00:00:00');
});
</script>
