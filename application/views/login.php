<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/bootstrap-clearmin.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/roboto.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/font-awesome.min.css">
    <title>Login</title>
    <style></style>
  </head>
<!--
<link href='http://fonts.googleapis.com/css?family=Raleway:500' rel='stylesheet' type='text/css'>
-->
<style type="text/css">
@charset "UTF-8";
/* CSS Document */

body {
    width:100px;
  height:100px;
  background: -webkit-linear-gradient(90deg, #16222A 10%, #3A6073 90%); /* Chrome 10+, Saf5.1+ */
  background:    -moz-linear-gradient(90deg, #16222A 10%, #3A6073 90%); /* FF3.6+ */
  background:     -ms-linear-gradient(90deg, #16222A 10%, #3A6073 90%); /* IE10 */
  background:      -o-linear-gradient(90deg, #16222A 10%, #3A6073 90%); /* Opera 11.10+ */
  background:         linear-gradient(90deg, #16222A 10%, #3A6073 90%); /* W3C */
font-family: 'Raleway', sans-serif;
}

p {
  color:#CCC;
}

.spacing {
  padding-top:7px;
  padding-bottom:7px;
}
.middlePage {
  width: 680px;
    height: 500px;
    position: absolute;
    top:0;
    bottom: 0;
    left: 0;
    right: 0;
    margin: auto;
}

.logo {
  color:#CCC;
}  
</style>
<body>
<div class="middlePage">
<div class="page-header">
  <h3 class="logo"></h3>
</div>

<div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">Please Sign In</h3>
  </div>
  <div class="panel-body">
  
  <div class="row">
  
<div class="col-md-4" style="text-align: center;">
  <img src="<?=base_url()?>assets/images/lambang_kab_garut.png" width="185" height="150">
</div>

  <div class="col-md-7" style="border-left:1px solid #ccc;height:160px">
      <form method="post" action="<?php echo base_url('index.php/admin/dashboard/proseslogin') ?>">
  <div class="col-md-12">
          <div class="form-group">
      <div class="input-group">
        <div class="input-group-addon"><i class="fa fa-fw fa-user"></i></div>
        <input type="text" name="username" class="form-control" placeholder="Username">
      </div>
          </div>
          <div class="form-group">
      <div class="input-group">
        <div class="input-group-addon"><i class="fa fa-fw fa-lock"></i></div>
        <input type="password" name="password" class="form-control" placeholder="Password">
      </div>
          </div>
        </div>
      <div class="col-xs-6"></div>
  <div class="col-xs-6">
          <button type="submit" class="btn btn-block btn-primary">Sign in</button>
        </div>
      </form>
</div>
    
</div>
    
</div>
</div>
</div>
  </body>
</html>
