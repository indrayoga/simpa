    <div id="global">
      <div class="container-fluid">
        <div class="row cm-fix-height">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">Tambah pengeluaran di luar serapan</div>
                <div class="panel-body">
                    <form class="form-horizontal" method="post" action="<?=base_url('ppk/createpengeluaran') ?>">
                        <input type="hidden" name="<?=$csrf['name'] ?>" value="<?=$csrf['hash'] ?>">
                        <div class="form-group <?php if(!empty(form_error('tahun_anggaran')))echo "has-error"; ?>">
                            <label for="tahun_anggaran" class="col-sm-2 control-label">Tahun Anggaran</label>
                            <div class="col-sm-10">
                                <input type="text" maxlength="4" readonly class="form-control" required id="tahun_anggaran" name="tahun_anggaran" placeholder="tahun" value="<?php echo $this->session->tahun_anggaran; ?>">
                                <span class="help-block has-error"><?php echo form_error('tahun_anggaran'); ?></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="program" class="col-sm-2 control-label">Program</label>
                            <div class="col-sm-10">
                                <input type="text" disabled class="form-control" value="PROGRAM PENINGKATAN PELAYANAN DAN PENEGEKAN HUKUM KEIMIGRASIAN">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="program" class="col-sm-2 control-label">Satker</label>
                            <div class="col-sm-10">
                                <input type="text" disabled class="form-control" value="KANTOR IMIGRASI KELAS I KHUSUS JAKARTA BARAT">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="id_output" class="col-sm-2 control-label">Bidang</label>
                            <div class="col-sm-10">
                                <select name="kode_bidang" class="form-control" id="kode_bidang">
                                    <option value="">Pilih Bidang</option>
                                <?php
                                    foreach ($databidang as $bidang) {
                                    ?>
                                    <option value="<?=$bidang['kode_bidang']?>" ><?=$bidang['nama_bidang']?></option>
                                    <?php
                                    }
                                ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group <?php if(!empty(form_error('tanggal')))echo "has-error"; ?>">
                            <label for="tanggal" class="col-sm-2 control-label">Tanggal</label>
                            <div class="col-sm-10">
                                <input type="text" data-toggle="datepicker" maxlength="10" data-mask="99-99-9999" class="form-control" required id="tanggal" name="tanggal" placeholder="tanggal" value="<?php echo set_value('tanggal'); ?>">
                                <span class="help-block has-error"><?php echo form_error('tanggal'); ?></span>
                            </div>
                        </div>
                        <div class="form-group <?php if(!empty(form_error('uraian')))echo "has-error"; ?>">
                            <label for="lokasi" class="col-sm-2 control-label">Uraian</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" required id="uraian" name="uraian" placeholder="uraian" value="<?php echo set_value('uraian'); ?>">
                                <span class="help-block has-error"><?php echo form_error('uraian'); ?></span>
                            </div>
                        </div>
                        <div class="form-group <?php if(!empty(form_error('jumlah')))echo "has-error"; ?>">
                            <label for="jumlah" class="col-sm-2 control-label">Jumlah</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="jumlah" name="jumlah" placeholder="jumlah" value="<?php echo set_value('jumlah'); ?>">
                                <span class="help-block has-error"><?php echo form_error('jumlah'); ?></span>
                            </div>
                        </div>
                        <div class="form-group" style="margin-bottom:0">
                            <div class="col-sm-offset-2 col-sm-10 text-right">
                                <button type="reset" class="btn btn-default">Reset</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
      </div>
      <footer class="cm-footer"><span class="pull-right">&copy;</span></footer>
    </div>
    </div>
<script type="text/javascript">
$(function () {
    $('[data-toggle="datepicker"]').datepicker({
      format: 'dd-mm-yyyy'
    });
});
</script>