    <div id="global">
      <div class="container-fluid">
        <div class="row cm-fix-height">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">Evaluasi Kegiatan</div>
                <div class="panel-body">
                    <dl class="dl-horizontal">
                        <dt>Tahun Anggaran</dt>
                        <dd>: <?=$item['tahun_anggaran'] ?></dd>
                        <dt>Kegiatan</dt>
                        <dd>: <?=$item['kegiatan']?></dd>
                        <dt>Waktu</dt>
                        <dd>: <?=$item['waktu'] ?></dd>
                        <dt>Lokasi</dt>
                        <dd>: <?=$item['lokasi'] ?></dd>
                        <dt>Jumlah Peserta</dt>
                        <dd>: <?=$item['jumlah_peserta'] ?></dd>
                        <dt>Pejabat Terkait</dt>
                        <dd>: <?=$item['surname'] ?></dd>
                        <dt>Tanggal</dt>
                        <dd>: <?=$item['tanggal_pengajuan'] ?></dd>
                        <dt>Tanda Terima</dt>
                        <dd>: <a href='<?=base_url('uploads/'.$item['tanda_terima_file'])?>'>[Download]</a></dd>
                    </dl>
                    <hr></hr>  
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Daftar File Hasil kegiatan</div>
                            <table class="table table-bordered table-hover table-striped md-data-table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Jenis</th>
                                        <th>File</th>
                                        <th>Pilihan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no=1;
                                    foreach ($items as $hasil){
                                    ?>
                                    <tr>
                                        <th scope="row"><?=$no?></th>
                                        <td><?php if($hasil['jenis']==1)echo "Resume";else if($hasil['jenis']==2)echo "Kwitansi"; else echo "Tanda Terima"; ?></td>
                                        <td><?=$hasil['namafile']?></td>
                                        <td>
                                            <div class="dropdown">
                                            <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Pilihan
                                            <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu" aria-labelledby="dLabel">
                                            <li>
                                            <a target="_newtab" href="<?=base_url("uploads/{$hasil['namafile']}")?>">Download</a>
                                            </li>
                                            <li>
                                            <a href="#" onclick="app_confirm('<?=base_url('um/deletehasilkegiatan/'.$hasil['id'].'/'.$hasil['id_pengajuan_um'])?>','Apakah Yakin Ingin Menghapus Data Ini?')" >Hapus</a>
                                            </li>
                                            </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $no++;
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                                                          
                    <form class="form-horizontal" method="post" action="<?=base_url('ppk/selesai') ?>">
                        <input type="hidden" name="<?=$csrf['name'] ?>" value="<?=$csrf['hash'] ?>">
                        <input type="hidden" name="id" value="<?=$id?>">
                        <div class="form-group" style="margin-bottom:0">
                            <div class="col-sm-offset-2 col-sm-10 text-right">
                                <button type="submit" class="btn btn-primary">SELESAI</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
      </div>
      <footer class="cm-footer"><span class="pull-right">&copy;</span></footer>
    </div>
    </div>
<script type="text/javascript">
$(document).ready(function(){
  $('#tahun_anggaran').mask('0000');
  $('#tanggal').mask('00-00-0000');
  $('#jam').mask('00:00:00');
});
</script>
