    <div id="global">
      <div class="container-fluid cm-container-white">
        <div class="bs-example">
          <a href="<?php echo base_url('ppk/addpengeluaran'); ?>" class="btn btn-info"><i class="fa-plus"></i> Tambah</a>
        </div>
        <hr></hr>
        <table class="table table-bordered table-hover table-striped md-data-table " id="tableKecamatan">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Tahun</th>
                    <th>Tanggal</th>
                    <th>Bidang</th>
                    <th>Uraian</th>
                    <th>Jumlah</th>
                    <th>Pilihan</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $no=1;
                foreach ($items as $item) {
                  ?>
                  <tr class="">
                      <td style="text-align:center"><?=$no?></td>
                      <td><?=$item['tahun_anggaran']?></td>
                      <td><?=$item['tanggal']?></td>
                      <td><?=strtoupper($item['kode_bidang'])?></td>
                      <td><?=$item['uraian']?></td>
                      <td><?=number_format($item['jumlah'],0,'','.')?></td>
                      <td>
                        <div class="dropdown">
                        <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Pilihan
                        <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                          <li><a href="<?=base_url("ppk/editpengeluaran/{$item['id']}")?>">Edit</a></li>
                          <li><a href="<?=base_url("ppk/hapuspengeluaran/{$item['id']}")?>">Hapus</a></li>
                        </ul>
                        </div>
                      </td>
                  </tr>
                  <?php
                  $no++;
                }
                ?>
            </tbody>
        </table>
      </div>
      <footer class="cm-footer"><span class="pull-right">&copy;</span></footer>
    </div>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/jquery.dataTables.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/dataTables.material.min.css') ?>">
    <script src="<?php echo base_url('assets/js/jquery.dataTables.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/dataTables.material.min.js'); ?>"></script>
