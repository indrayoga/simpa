    <script type="text/javascript">
    window.onload = function () {
        var chart = new CanvasJS.Chart("chartContainer",
        {

            title:{
                text: "Grafik (%) Statistik Penyerapan Anggaran",
                fontSize: 30
            },
                        animationEnabled: true,
            axisX:{

                gridColor: "Silver",
                tickColor: "silver",
                valueFormatString: "MMM"

            },                        
                        toolTip:{
                          shared:true
                        },
            theme: "theme2",
            axisY: {
                gridColor: "Silver",
                tickColor: "silver"
            },
            legend:{
                verticalAlign: "center",
                horizontalAlign: "right"
            },
            data: [
            {        
                type: "line",
                showInLegend: true,
                name: "WASDAKIM",
                color: "#20B2AA",
                lineThickness: 2,

                dataPoints: [
                <?php
                foreach ($items as $item) {
                    # code...
                ?>
                    { x: new Date(<?=$tahun?>,<?=$item['kode_bulan']?>), y: <?=$item['persenwasdakim']?> },
                <?php
                }
                ?>
                ]
            },
            {        
                type: "line",
                showInLegend: true,
                name: "FORSAKIM",
                color: "#2020AA",
                lineThickness: 2,

                dataPoints: [
                <?php
                foreach ($items as $item) {
                    # code...
                ?>
                    { x: new Date(<?=$tahun?>,<?=$item['kode_bulan']?>), y: <?=$item['persenforsakim']?> },
                <?php
                }
                ?>
                ]
            },
            {        
                type: "line",
                showInLegend: true,
                name: "TU",
                color: "#ffff00",
                lineThickness: 2,

                dataPoints: [
                <?php
                foreach ($items as $item) {
                    # code...
                ?>
                    { x: new Date(<?=$tahun?>,<?=$item['kode_bulan']?>), y: <?=$item['persentu']?> },
                <?php
                }
                ?>
                ]
            },
            {        
                type: "line",
                showInLegend: true,
                name: "LALINTUSKIM",
                color: "#AAffff",
                lineThickness: 2,

                dataPoints: [
                <?php
                foreach ($items as $item) {
                    # code...
                ?>
                    { x: new Date(<?=$tahun?>,<?=$item['kode_bulan']?>), y: <?=$item['persenlalintuskim']?> },
                <?php
                }
                ?>
                ]
            },
            {        
                type: "line",
                showInLegend: true,
                name: "DARINSUK",
                color: "#20B200",
                lineThickness: 2,

                dataPoints: [
                <?php
                foreach ($items as $item) {
                    # code...
                ?>
                    { x: new Date(<?=$tahun?>,<?=$item['kode_bulan']?>), y: <?=$item['persendazinim']?> },
                <?php
                }
                ?>
                ]
            }

            ],
          legend:{
            cursor:"pointer",
            itemclick:function(e){
              if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                e.dataSeries.visible = false;
              }
              else{
                e.dataSeries.visible = true;
              }
              chart.render();
            }
          }
        });

chart.render();
}
</script>
    <script src="<?php echo base_url('assets/js/canvasjs.min.js'); ?>"></script>

    <div id="global">
      <div class="container-fluid cm-container-white">
        <hr></hr>
          <form class="form-horizontal" action="" method="post" accept-charset="utf-8">
            <input type="hidden" name="<?=$csrf['name'] ?>" value="<?=$csrf['hash'] ?>">
            <div class="form-group">
                <label for="tahun" class="col-sm-2 control-label">Tahun</label>
                <div class="col-sm-10">
                    <input type="date" maxlength="4" value="<?=$tahun?>" data-mask="9999" class="form-control" id="tahun" name="tahun" placeholder="tahun" value="">
                    <span class="help-block has-error"></span>
                </div>
            </div>
            <div class="form-group" style="margin-bottom:0">
                <div class="col-sm-offset-2 col-sm-10 text-right">
                    <button type="reset" class="btn btn-default">Reset</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
          </form>        
        <hr></hr>
<div class="row">
    <div class="col-md-12">
        <div> &nbsp;</div> 
        <div class="panel panel-primary">
            <div class="panel-heading">
                <span class="glyphicon glyphicon-signal"></span> 
                <h3 class="panel-title" style="display: inline;">Grafik Statistik Penyerapan Anggaran Antar Bidang</h3>
                <div class="clearfix"></div>
            </div>
            <div class="panel-body" id="chartContainer" style="height: 300px; width: 100%;"></div>
        </div>
    </div>
</div>                

        <table class="table table-bordered table-hover table-striped md-data-table " id="tableKecamatan">
            <thead>
                <tr>
                    <th rowspan="2">BULAN</th>
                    <th colspan="2">DARINSUK</th>
                    <th colspan="2">FORSAKIM</th>
                    <th colspan="2">LALINTUSKIM</th>
                    <th colspan="2">TU</th>
                    <th colspan="2">WASDAKIM</th>
                </tr>
                <tr>
                  <td>Rp</td>
                  <td>%</td>
                  <td>Rp</td>
                  <td>%</td>
                  <td>Rp</td>
                  <td>%</td>
                  <td>Rp</td>
                  <td>%</td>
                  <td>Rp</td>
                  <td>%</td>
                </tr>
            </thead>
            <tbody>
                <?php
                $no=1;
                $totaldazinim=0;
                $persentotaldazinim=0;
                $totalforsakim=0;
                $persentotalforsakim=0;
                $totallalintuskim=0;
                $persentotallalintuskim=0;
                $totaltu=0;
                $persentotaltu=0;
                $totalwasdakim=0;
                $persentotalwasdakim=0;
                foreach ($items as $item) {
                  ?>
                  <tr class="">
                      <td><?=strtoupper($item['nama_bulan'])?></td>
                      <td style="text-align:right;"><?=number_format($item['dazinim'], 0, "", ".")?></td>
                      <td style="text-align:right;"><?=number_format($item['persendazinim'], 2, ",", ".")?></td>
                      <td style="text-align:right;"><?=number_format($item['forsakim'], 0, "", ".")?></td>
                      <td style="text-align:right;"><?=number_format($item['persenforsakim'], 2, ",", ".")?></td>
                      <td style="text-align:right;"><?=number_format($item['lalintuskim'], 0, "", ".")?></td>
                      <td style="text-align:right;"><?=number_format($item['persenlalintuskim'], 2, ",", ".")?></td>
                      <td style="text-align:right;"><?=number_format($item['tu'], 0, "", ".")?></td>
                      <td style="text-align:right;"><?=number_format($item['persentu'], 2, ",", ".")?></td>
                      <td style="text-align:right;"><?=number_format($item['wasdakim'], 0, "", ".")?></td>
                      <td style="text-align:right;"><?=number_format($item['persenwasdakim'], 2, ",", ".")?></td>
                  </tr>
                  <?php
                  $totaldazinim=$totaldazinim+$item['dazinim'];
                  $persentotaldazinim=$persentotaldazinim+$item['persendazinim'];
                  $totalforsakim=$totalforsakim+$item['forsakim'];
                  $persentotalforsakim=$persentotalforsakim+$item['persenforsakim'];
                  $totallalintuskim=$totallalintuskim+$item['lalintuskim'];
                  $persentotallalintuskim=$persentotallalintuskim+$item['persenlalintuskim'];
                  $totaltu=$totaltu+$item['tu'];
                  $persentotaltu=$persentotaltu+$item['persentu'];
                  $totalwasdakim=$totalwasdakim+$item['wasdakim'];
                  $persentotalwasdakim=$persentotalwasdakim+$item['persenwasdakim'];
                  $no++;
                }
                ?>
            </tbody>
            <tfoot>
              <tr>
                  <td>T O T A L</td>
                  <td style="text-align:right;"><?=number_format($totaldazinim, 0, "", ".")?></td>
                  <td style="text-align:right;"><?=number_format($persentotaldazinim, 2, ",", ".")?></td>
                  <td style="text-align:right;"><?=number_format($totalforsakim, 0, "", ".")?></td>
                  <td style="text-align:right;"><?=number_format($persentotalforsakim, 2, ",", ".")?></td>
                  <td style="text-align:right;"><?=number_format($totallalintuskim, 0, "", ".")?></td>
                  <td style="text-align:right;"><?=number_format($persentotallalintuskim, 2, ",", ".")?></td>
                  <td style="text-align:right;"><?=number_format($totaltu, 0, "", ".")?></td>
                  <td style="text-align:right;"><?=number_format($persentotaltu, 2, ",", ".")?></td>
                  <td style="text-align:right;"><?=number_format($totalwasdakim, 0, "", ".")?></td>
                  <td style="text-align:right;"><?=number_format($persentotalwasdakim, 2, ",", ".")?></td>
              </tr>
            </tfoot>

        </table>
      </div>
      <footer class="cm-footer"><span class="pull-right">&copy;</span></footer>
    </div>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/jquery.dataTables.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/dataTables.material.min.css') ?>">
    <script src="<?php echo base_url('assets/js/jquery.dataTables.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/dataTables.material.min.js'); ?>"></script>
      <script src="<?php echo base_url('assets/js/demo/popovers-tooltips.js'); ?>"></script>

