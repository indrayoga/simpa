<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
    <style> .demo-icons .link{ padding:10px; border:1px solid transparent; overflow:hidden; text-overflow:ellipsis; white-space:nowrap; border-radius:2px; }
    .link:hover{background:#f5f5f5;border-color:#eee;}
.info-box {
    display: block;
    min-height: 90px;
    background: #fff;
    width: 100%;
    box-shadow: 0 1px 1px rgba(0,0,0,0.1);
    border-radius: 2px;
    margin-bottom: 15px;
}
.bg-aqua, .callout.callout-info, .alert-info, .label-info, .modal-info .modal-body {
    background-color: #00c0ef !important;
}
.info-box-icon {
    border-top-left-radius: 2px;
    border-top-right-radius: 0;
    border-bottom-right-radius: 0;
    border-bottom-left-radius: 2px;
    display: block;
    float: left;
    height: 90px;
    width: 90px;
    text-align: center;
    font-size: 45px;
    line-height: 90px;
    background: rgba(0,0,0,0.2);
        background-color: rgba(0, 0, 0, 0.2);
}
.info-box-content {
    padding: 5px 10px;
    margin-left: 90px;
}
 .info-box-text {
    text-transform: uppercase;
}
.info-box-number {
    display: block;
    font-weight: bold;
    font-size: 18px;
}
.panel {
    margin-bottom: 20px;
    background-color: #fff;
    border: 1px solid transparent;
    border-radius: 4px;
    -webkit-box-shadow: 0 1px 1px rgba(0,0,0,.05);
    box-shadow: 0 1px 1px rgba(0,0,0,.05);
}

.board .panel {
    padding: 25px 15px;
    border: none;
    background: #fff;
}
.panel-primary {
    display: inline-block;
    margin-bottom: 30px;
    width: 100%;
}
.panel {
    border-radius: 0px;
}
.board .number {
    float: left;
}

.number h3 {
    font-size: 32px;
    color: #676767;
}
.number small {
    font-size: 14px;
    color: #AAB5BC;
    font-weight: 600;
    text-transform: uppercase;
    margin: 8px 5px;
    display: inline-block;
}
.board .icon {
    float: right;
}

.info-box {
    padding: 25px 20px;
        padding-bottom: 25px;
    padding-bottom: 30px;
    color: #ffffff;
}
.info-box .title {
    font-size: 16px;
    margin-bottom: 3px;
}

.info-box .content {
    text-align: left;
    font-size: 22px;
    line-height: 22px;
}

    </style>
    <script type="text/javascript">
    window.onload = function () {
        var chart = new CanvasJS.Chart("chartContainer",
        {

            title:{
                text: "Grafik Penyerapan Anggaran",
                fontSize: 30
            },
                        animationEnabled: true,
            axisX:{

                gridColor: "Silver",
                tickColor: "silver",
                valueFormatString: "MMM"

            },                        
                        toolTip:{
                          shared:true
                        },
            theme: "theme2",
            axisY: {
                gridColor: "Silver",
                tickColor: "silver"
            },
            legend:{
                verticalAlign: "center",
                horizontalAlign: "right"
            },
            data: [
            {        
                type: "line",
                showInLegend: true,
                name: "Realisasi Penyerapan Anggaran Bidang",
                color: "#20B2AA",
                lineThickness: 2,

                dataPoints: [
                <?php
                foreach ($itemsrealisasibidang as $item) {
                    # code...
                ?>
                    { x: new Date(<?=$tahun?>,<?=$item['kode_bulan']?>), y: <?=$item['persenjumlah']?> },
                <?php
                }
                ?>
                ]
            },
            
            ],
          legend:{
            cursor:"pointer",
            itemclick:function(e){
              if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                e.dataSeries.visible = false;
              }
              else{
                e.dataSeries.visible = true;
              }
              chart.render();
            }
          }
        });

chart.render();

        var chart2 = new CanvasJS.Chart("chartContainer2",
        {

            title:{
                text: "Grafik Perbandingan (%) Statistik Penyerapan Anggaran Semua Bidang",
                fontSize: 30
            },
                        animationEnabled: true,
            axisX:{

                gridColor: "Silver",
                tickColor: "silver",
                valueFormatString: "MMM"

            },                        
                        toolTip:{
                          shared:true
                        },
            theme: "theme2",
            axisY: {
                gridColor: "Silver",
                tickColor: "silver"
            },
            legend:{
                verticalAlign: "center",
                horizontalAlign: "right"
            },
            data: [
            {        
                type: "line",
                showInLegend: true,
                name: "WASDAKIM",
                color: "#20B2AA",
                lineThickness: 2,

                dataPoints: [
                <?php
                foreach ($itemsrealisasisemuabidang as $item) {
                    # code...
                ?>
                    { x: new Date(<?=$tahun?>,<?=$item['kode_bulan']?>), y: <?=$item['persenwasdakim']?> },
                <?php
                }
                ?>
                ]
            },
            {        
                type: "line",
                showInLegend: true,
                name: "FORSAKIM",
                color: "#2020AA",
                lineThickness: 2,

                dataPoints: [
                <?php
                foreach ($itemsrealisasisemuabidang as $item) {
                    # code...
                ?>
                    { x: new Date(<?=$tahun?>,<?=$item['kode_bulan']?>), y: <?=$item['persenforsakim']?> },
                <?php
                }
                ?>
                ]
            },
            {        
                type: "line",
                showInLegend: true,
                name: "TU",
                color: "#ffff00",
                lineThickness: 2,

                dataPoints: [
                <?php
                foreach ($itemsrealisasisemuabidang as $item) {
                    # code...
                ?>
                    { x: new Date(<?=$tahun?>,<?=$item['kode_bulan']?>), y: <?=$item['persentu']?> },
                <?php
                }
                ?>
                ]
            },
            {        
                type: "line",
                showInLegend: true,
                name: "LALINTUSKIM",
                color: "#AAffff",
                lineThickness: 2,

                dataPoints: [
                <?php
                foreach ($itemsrealisasisemuabidang as $item) {
                    # code...
                ?>
                    { x: new Date(<?=$tahun?>,<?=$item['kode_bulan']?>), y: <?=$item['persenlalintuskim']?> },
                <?php
                }
                ?>
                ]
            },
            {        
                type: "line",
                showInLegend: true,
                name: "DARINSUK",
                color: "#20B200",
                lineThickness: 2,

                dataPoints: [
                <?php
                foreach ($itemsrealisasisemuabidang as $item) {
                    # code...
                ?>
                    { x: new Date(<?=$tahun?>,<?=$item['kode_bulan']?>), y: <?=$item['persendazinim']?> },
                <?php
                }
                ?>
                ]
            }

            ],
          legend:{
            cursor:"pointer",
            itemclick:function(e){
              if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                e.dataSeries.visible = false;
              }
              else{
                e.dataSeries.visible = true;
              }
              chart.render();
            }
          }
        });

chart2.render();

}
</script>
    <script src="<?php echo base_url('assets/js/canvasjs.min.js'); ?>"></script>
    <div id="global">
            <div class="container-fluid">
                <div class="row">
    <div class="col-md-3">
        
        <div class="panel panel-info">
          <div class="panel-heading">
            <div class="row">
              <div class="col-xs-5">
                <i class="fa fa-list-ol fa-5x"></i>
              </div>
              <div class="col-xs-7 text-right">
                <p class="announcement-heading"><?=$dibuat?></p>
                <p class="announcement-text">Dokumen dibuat</p>
              </div>
            </div>
          </div>
          <a href="<?=base_url('um/dibuat')?>">
            <div class="panel-footer announcement-bottom">
              <div class="row">
                <div class="col-xs-6">
                  View
                </div>
                <div class="col-xs-6 text-right">
                  <i class="fa fa-arrow-circle-right"></i>
                </div>
              </div>
            </div>
          </a>
        </div>
        
    </div>
    
    
    

    <div class="col-md-3">
        
        <div class="panel panel-default">
          <div class="panel-heading">
            <div class="row">
              <div class="col-xs-5">
                <i class="fa fa-list fa-5x"></i>
              </div>
              <div class="col-xs-7 text-right">
                <p class="announcement-heading"> <?=$disetujui?></p>
                <p class="announcement-text">DOKUMEN DISETUJUI</p>
              </div>
            </div>
          </div>
          <a href="<?=base_url('um/disetujui')?>">
            <div class="panel-footer announcement-bottom">
              <div class="row">
                <div class="col-xs-6">
                  Details
                </div>
                <div class="col-xs-6 text-right">
                  <i class="fa fa-arrow-circle-right"></i>
                </div>
              </div>
            </div>
          </a>
        </div>
        
    </div>  
    
    

    <div class="col-md-3">
        
        <div class="panel panel-warning">
          <div class="panel-heading">
            <div class="row">
              <div class="col-xs-5">
                <i class="fa fa-list-alt fa-5x"></i>
              </div>
              <div class="col-xs-7 text-right">
                <p class="announcement-heading"> <?=$lulus?></p>
                <p class="announcement-text">DOKUMEN LULUS</p>
              </div>
            </div>
          </div>
          <a href="<?=base_url('um/lulus')?>">
            <div class="panel-footer announcement-bottom">
              <div class="row">
                <div class="col-xs-6">
                  Details
                </div>
                <div class="col-xs-6 text-right">
                  <i class="fa fa-arrow-circle-right"></i>
                </div>
              </div>
            </div>
          </a>
        </div>
        
    </div>      
    
    


    <div class="col-md-3">
        
        <div class="panel panel-success">
          <div class="panel-heading">
            <div class="row">
              <div class="col-xs-5">
                <i class="fa fa-check fa-5x"></i>
              </div>
              <div class="col-xs-7 text-right">
                <p class="announcement-heading"> <?=$selesai?></p>
                <p class="announcement-text">DOKUMEN SELESAI <i class=""></i>  </p>
              </div>
            </div>
          </div>
          <a href="<?=base_url('um/selesai')?>">
            <div class="panel-footer announcement-bottom">
              <div class="row">
                <div class="col-xs-6">
                  Details
                </div>
                <div class="col-xs-6 text-right">
                  <i class="fa fa-arrow-circle-right"></i>
                </div>
              </div>
            </div>
          </a>
        </div>
        
    </div> 
<div class="row">
    <div class="col-md-4">
        <div class="info-box" style="background:#348FE2">
              <div class="col-xs-4">
                <i class="fa fa-money fa-4x"></i>
              </div>
            <div class="title"> Total Anggaran</div>
            <div class="content">Rp <span id="total-jumlah"><?=number_format($totalanggaran['pagu'],0,'','.')?></span></div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="info-box" style="background:#DE8083">
              <div class="col-xs-4">
                <i class="fa fa-tag fa-4x"></i>
              </div>
            <div class="title"> Total Serapan</div>
            <div class="content">Rp <span id="total-pengeluaran"><?=number_format($serapananggaran['jumlah']+$serapanppk['jumlah'],0,'','.')?></span></div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="info-box" style="background:#5CB85C">
              <div class="col-xs-4">
                <i class="fa fa-arrow-right fa-4x"></i>
              </div>
            <div class="title"> Saldo</div>
            <div class="content">Rp <span id="total-saldo"><?=number_format($totalanggaran['pagu']-($serapananggaran['jumlah']+$serapanppk['jumlah']),0,'','.')?></span></div>
        </div>
    </div>     
</div>             

                <div class="row">
                    <div class="col-sm-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">Total Anggaran</div>
                            <div class="panel-body">
                                <div id="d1-c1" style="height:150px"><?=$totalanggaran['pagu']?></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">Total Serapan</div>
                            <div class="panel-body">
                                <input type="hidden" id="persentasirealisasi" value="<?=$persen?>" >
                                <div id="d1-c2" style="height:150px"></div>
                            </div>
                        </div>
                    </div>
                </div>
<div class="row" <?php if($this->auth->checkRole('Pemohon')===false){ echo "style='display:none'"; } ?> >
    <div class="col-md-12">
        <div> &nbsp;</div> 
        <div class="panel panel-primary">
            <div class="panel-heading">
                <span class="glyphicon glyphicon-signal"></span> 
                <h3 class="panel-title" style="display: inline;">Grafik Penyerapan Anggaran Bidang <?=$this->user['seksi']?></h3>
                <div class="clearfix"></div>
            </div>
            <div class="panel-body" id="chartContainer" style="height: 300px; width: 100%;"></div>
        </div>
    </div>
</div>                
<div class="row">
    <div class="col-md-12">
        <div> &nbsp;</div> 
        <div class="panel panel-primary">
            <div class="panel-heading">
                <span class="glyphicon glyphicon-signal"></span> 
                <h3 class="panel-title" style="display: inline;">Grafik Perbandingan Penyerapan Anggaran Antar Bidang</h3>
                <div class="clearfix"></div>
            </div>
            <div class="panel-body" id="chartContainer2" style="height: 300px; width: 100%;"></div>
        </div>
    </div>
</div>                
                <div class="panel panel-default">
                    <div class="panel-heading">Progress Pelaksanaan Kegiatan</div>
                    <table class="table table-bordered">
                    <?php
                    $progress=0;
                    foreach ($datakegiatan as $kegiatan){
                    ?>
                        <tr>
                            <td style="white-space:nowrap;width:10% !important;"><?=$kegiatan['kegiatan']?></td>
                            <?php
                            if($kegiatan['id_kegiatan']){
                                if($kegiatan['tag_evaluasi']==2){
                                    $progress=100;
                                }else if($kegiatan['tag_tanda_terima']){
                                    $progress=80;
                                }else if($kegiatan['tag_spm']){
                                    $progress=60;
                                }else if($kegiatan['tag_ppk']){
                                    $progress=40;
                                }else if($kegiatan['tag_kpa']){
                                    $progress=20;
                                }else if($kegiatan['id_kegiatan']){
                                    $progress=10;
                                }else{
                                    $progress=0;
                                }
                            ?>
                            <td style="width:80% !important;">
                                <div class="progress">
                                    <div class="progress-bar progress-bar-danger" style="width:<?=$progress?>%"></div>
                                </div>
                            </td>
                            <td><?=$progress?>%</td>
                            <?php } ?>
                        </tr>

                    <?php
                    $progress=$progress+10;
                    }
                    ?>
                    </table>
                </div>
            </div>
        <footer class="cm-footer"><span class="pull-right">&copy;</span></footer>
    </div>
        <script type="text/javascript">
            labelx=['x'];
            labely=['Serapan Anggaran'];
            <?php
            //foreach ($statistik as $stat) {
                # code...
            ?>
            //labelx.push('<?php //echo $stat['kode_bidang']?>');
            //labely.push('<?php //echo $stat['jumlah']?>');
            <?php
            //}
            ?>
        </script>
        <script src="<?php echo base_url() ?>assets/js/lib/d3.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/lib/c3.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/demo/dashboard.js"></script>
