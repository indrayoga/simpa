<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
    <style> .demo-icons .link{ padding:10px; border:1px solid transparent; overflow:hidden; text-overflow:ellipsis; white-space:nowrap; border-radius:2px; }
    .link:hover{background:#f5f5f5;border-color:#eee;}
    </style>
    <div id="global">
        <div class="container-fluid cm-container-white">
            <div class="row">
              <div class="col-sm-12">
                  <div id="calendar"></div>
              </div>
            </div>
        </div>

        <footer class="cm-footer"><span class="pull-right">&copy;</span></footer>
    </div>

<script type="text/javascript">
    
$(document).ready(function(){

        var calendar = $('#calendar').fullCalendar({  // assign calendar

            header:{

                left: 'prev,next today',

                center: 'title',

                right: 'month,agendaWeek,agendaDay'

            },
            defaultView: 'month',
            allDaySlot: false,

            events: base_url+"events",  // request to load current events

            eventClick:  function(event, jsEvent, view) {  // when some one click on any event

                alert('Event: ' + event.title);
            },

            
        });
    });



</script>