<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php
  $items=array();
  $role=$this->auth->getRole();
  if($role=='KPA'){
    $items=$this->db->order_by('tgl_dibuat','desc')->get_where('pesan',array('role'=>'KPA'))->result_array();
  }                    
  if($role=='PPK'){
    $items=$this->db->order_by('tgl_dibuat','desc')->get_where('pesan',array('role'=>'PPK'))->result_array();
  }                    
  if($role=='PPSPM'){
    $items=$this->db->order_by('tgl_dibuat','desc')->get_where('pesan',array('role'=>'PPSPM'))->result_array();
  }                    
  if($role=='BENDAHARA'){
    $items=$this->db->order_by('tgl_dibuat','desc')->get_where('pesan',array('role'=>'BENDAHARA'))->result_array();
  }
  if($role=='Pemohon'){
    $items=$this->db->order_by('tgl_dibuat','desc')->get_where('pesan',array('role'=>'Pemohon','id_user'=>$this->encryption->decrypt($this->session->id)))->result_array();
  }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap-clearmin.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/roboto.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/material-design.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/small-n-flat.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/font-awesome.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datepicker.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/select2.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/select2-bootstrap.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/fullcalendar.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/fullcalendar.print.css') ?>" media="print">
    <script src="<?php echo base_url('assets/js/lib/jquery-2.1.3.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/datepicker.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/moment.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/fullcalendar.min.js'); ?>"></script>
    <title>Dashboard</title>
    <script type="text/javascript">
      var base_url="<?php echo base_url() ?>";
    </script>
  </head>
  <body class="cm-no-transition <?php echo (isset($navbar2))?"cm-2-navbar":"cm-1-navbar"; ?> ">
    <div id="cm-menu">
      <nav class="cm-navbar cm-navbar-primary">
        <div class="cm-flex"></div>
        <div class="btn btn-primary md-menu-white" data-toggle="cm-menu"></div>
      </nav>
      <div id="cm-menu-content">
        <div id="cm-menu-items-wrapper">
          <div id="cm-menu-scroller">
            <ul class="cm-menu-items">
              <li><a class="sf-house" href="<?php echo base_url('dashboard') ?>">Dashboard</a></li>
                <li class="cm-submenu">
                    <a class="sf-calendar">Kalender Kegiatan <span class="caret"></span></a>
                    <ul>
                        <li><a href="<?php echo base_url('kk/kalender') ?>">Kalender Kegiatan</a></li>
                    </ul>
                </li>              
              <?php
                if($role=='KPA'){
                ?>
                <li class="cm-submenu">
                    <a class="sf-window-layout">KPA <span class="caret"></span></a>
                    <ul>
                        <li><a href="<?php echo base_url('kpa') ?>">Daftar</a></li>
                    </ul>
                </li>              
                <?php
                }                    
                if($role=='PPK'){
                ?>
                <li class="cm-submenu">
                    <a class="sf-window-layout">PPK <span class="caret"></span></a>
                    <ul>
                        <li><a href="<?php echo base_url('ppk') ?>">Daftar</a></li>
                        <li><a href="<?php echo base_url('ppk/pengeluaran') ?>">Pengeluaran</a></li>
                    </ul>
                </li>              
                <?php
                }                    
                if($role=='PPSPM'){
                ?>
                <li class="cm-submenu">
                    <a class="sf-window-layout">PPSPM <span class="caret"></span></a>
                    <ul>
                        <li><a href="<?php echo base_url('spm') ?>">Daftar</a></li>
                    </ul>
                </li>              
                <?php
                }                    
                if($role=='BENDAHARA'){
                ?>
                <li class="cm-submenu">
                    <a class="sf-window-layout">Bendahara <span class="caret"></span></a>
                    <ul>
                        <li><a href="<?php echo base_url('bendahara') ?>">Daftar</a></li>
                    </ul>
                </li>              
                <?php
                }
                if($role=='Pemohon'){
                ?>
                <li class="cm-submenu">
                    <a class="sf-window-layout">Permohonan UM <span class="caret"></span></a>
                    <ul>
                        <li><a href="<?php echo base_url('um/add') ?>">Pengajuan UM</a></li>
                        <li><a href="<?php echo base_url('um') ?>">Daftar</a></li>
                    </ul>
                </li>              
                <?php
                }
              ?>
                <li class="cm-submenu">
                    <a class="sf-window-layout">Laporan <span class="caret"></span></a>
                    <ul>
                        <li><a href="<?php echo base_url('um/diajukan') ?>">Pengajuan Yang di Ajukan</a></li>
                        <li><a href="<?php echo base_url('um/disetujui') ?>">Pengajuan Yang di Setujui</a></li>
                        <li><a href="<?php echo base_url('um/lulus') ?>">Pengajuan Yang Lulus</a></li>
                        <li><a href="<?php echo base_url('um/selesai') ?>">Pengajuan Yang Selesai</a></li>
                        <li><a href="<?php echo base_url('realisasi') ?>">Realisasi Anggaran</a></li>
                        <li><a href="<?php echo base_url('statistik') ?>">Statistik</a></li>
                    </ul>
                </li>              
            </ul>
          </div>
        </div>
      </div>
    </div>
    <header id="cm-header">
      <nav class="cm-navbar cm-navbar-primary">
        <div class="btn btn-primary md-menu-white hidden-md hidden-lg" data-toggle="cm-menu"></div>
        <div class="cm-flex"><h1>Selamat Datang <?=strtoupper($this->encryption->decrypt($this->session->username))?> di aplikasi SIMPA, Anda Login Sebagai <?=$this->auth->getRole()?>.</h1></div>
        <div class="dropdown pull-right">
            <button class="btn btn-primary md-notifications-white" data-toggle="dropdown"> <span class="label label-danger"><?=count($items)?></span> </button>
            <div class="popover cm-popover bottom">
                <div class="arrow"></div>
                <div class="popover-content">
                    <div class="list-group">
                    <?php
                      foreach ($items as $item) {
                        # code...
                      ?>
                        <a href="<?=base_url('history/pesan/')?>" class="list-group-item">
                            <h4 class="list-group-item-heading text-overflow">
                                <i class="fa fa-fw fa-envelope"></i> Pesan
                            </h4>
                            <p class="list-group-item-text text-overflow"><?=$item['pesan']?></p>
                        </a>
                      <?php
                      }                    
                    ?>
                    </div>
                    <div style="padding:10px"><a class="btn btn-success btn-block" href="#">Lihat Selengkapnya...</a></div>
                </div>
            </div>
        </div>
        <div class="dropdown pull-right">
            <button class="btn btn-primary md-account-circle-white" data-toggle="dropdown"></button>
            <ul class="dropdown-menu">
                <li class="disabled text-center">
                    <a style="cursor:default;"><strong><?=$this->encryption->decrypt($this->session->username)?></strong></a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="<?=base_url('logout') ?>"><i class="fa fa-fw fa-sign-out"></i> Sign out</a>
                </li>
            </ul>
        </div>
      </nav>
      <?php
      if($this->uri->segment(1)=='dashboard'){

      ?>
            <script type="text/javascript">
              $('.cm-no-transition').addClass('cm-2-navbar');
            </script>
            <nav class="cm-navbar cm-navbar-default cm-navbar-slideup">
                <div class="cm-flex">
                    <div class="nav-tabs-container">
                        <ul class="nav nav-tabs">
                            <li class="<?=($this->uri->segment(2)!='kalender')?'active':''?>"><a href="<?=base_url('dashboard')?>">Statistik</a></li>
                            <li class="<?=($this->uri->segment(2)=='kalender')?'active':''?>"><a href="<?=base_url('dashboard/kalender')?>">Kalender</a></li>
                        </ul>
                    </div>
                </div>
            </nav>
      <?php
      }
      ?>
    </header>
