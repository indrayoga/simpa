    <div id="global">
      <div class="container-fluid cm-container-white">
        <hr></hr>
        <table class="table table-bordered table-hover table-striped md-data-table " id="tableKecamatan">
          <thead>
            <tr>
              <th>Kode</th>
              <th>Kegiatan</th>
              <th>Nilai Pagu</th>
              <th>Pilihan</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $level=1;
            $parentId="";
            foreach ($items as $item) {
              # code...
              ?>
              <tr data-tt-id="<?=$item['id_dipa']?>" data-tt-parent-id="<?=$item['parentid']?>">
                <td><?=$item['kode_kegiatan']?></td>
                <td><?=$item['kegiatan']?></td>
                <td style="text-align: right;"><?=number_format($item['pagu'],0,'','.')?></td>
                <?php
                if($item['level']==1){
                ?>
                <td><a href="<?=base_url('admin/suboutput/add/'.$item['id_dipa'])?>">Tambah</a> || <a href="<?=base_url('admin/dipa/append/'.$item['id_dipa'])?>">Sisipkan</a> || <a href="<?=base_url('admin/suboutput/modify/'.$item['id_dipa'])?>">Edit</a> || <a href="#" onclick="app_confirm('<?=base_url('admin/suboutput/delete/'.$item['id_dipa'])?>','Apakah Yakin Ingin Menghapus Data Ini?')">Hapus</a></td>
                <?php
                }else{
                ?>
                <td><a href="<?=base_url('admin/dipa/append/'.$item['id_dipa'])?>">Sisipkan</a> || <a href="<?=base_url('admin/suboutput/modify/'.$item['id_dipa'])?>">Edit</a> || <a href="#" onclick="app_confirm('<?=base_url('admin/suboutput/delete/'.$item['id_dipa'])?>','Apakah Yakin Ingin Menghapus Data Ini?')">Hapus</a></td>
                <?php
                }
                ?>
              </tr>
              <?php
            }
            ?>
          </tbody>
        </table>        

      </div>
      <footer class="cm-footer"><span class="pull-right">&copy;</span></footer>
    </div>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/jquery-treetable/jquery.treetable.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/jquery-treetable/jquery.treetable.theme.default.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/jquery-treetable/screen.css') ?>">
    <script src="<?php echo base_url('assets/js/jquery.treetable.js'); ?>"></script>
    <script type="text/javascript">
        $('#tableKecamatan').treetable({ expandable: true });      
    </script>
