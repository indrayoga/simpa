    <div id="global">
      <div class="container-fluid cm-container-white">
        <hr></hr>
          <form class="form-horizontal" action="" method="post" accept-charset="utf-8">
            <input type="hidden" name="<?=$csrf['name'] ?>" value="<?=$csrf['hash'] ?>">
            <!--
            <div class="form-group <?php if(!empty(form_error('value')))echo "has-error"; ?>">
                <label for="value" class="col-sm-2 control-label">Kegiatan</label>
                <div class="col-sm-10">
                    <select name="bulan" class="form-control" id="value">
                        <option value="">Pilih Bulan</option>
                        <option value="01" <?=($bulan=='01')?'selected=selected':''?>>Januari</option>
                        <option value="02" <?=($bulan=='02')?'selected=selected':''?>>Februari</option>
                        <option value="03" <?=($bulan=='03')?'selected=selected':''?>>Maret</option>
                        <option value="04" <?=($bulan=='04')?'selected=selected':''?>>April</option>
                        <option value="05" <?=($bulan=='05')?'selected=selected':''?>>Mei</option>
                        <option value="06" <?=($bulan=='06')?'selected=selected':''?>>Juni</option>
                        <option value="07" <?=($bulan=='07')?'selected=selected':''?>>Juli</option>
                        <option value="08" <?=($bulan=='08')?'selected=selected':''?>>Agustus</option>
                        <option value="09" <?=($bulan=='09')?'selected=selected':''?>>September</option>
                        <option value="10" <?=($bulan=='10')?'selected=selected':''?>>Oktober</option>
                        <option value="11" <?=($bulan=='11')?'selected=selected':''?>>November</option>
                        <option value="12" <?=($bulan=='12')?'selected=selected':''?>>Desember</option>
                    </select>
                </div>
            </div>
            -->
            <div class="form-group">
                <label for="tahun" class="col-sm-2 control-label">Tahun</label>
                <div class="col-sm-10">
                    <input type="text" maxlength="4" value="<?=$tahun?>" data-mask="9999" class="form-control" id="tahun" name="tahun" placeholder="tahun" value="">
                    <span class="help-block has-error"></span>
                </div>
            </div>
            <div class="form-group" style="margin-bottom:0">
                <div class="col-sm-offset-2 col-sm-10 text-right">
                    <button type="reset" class="btn btn-default">Reset</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
          </form>        
        <hr></hr>
        <h2>Laporan Realisasi Anggaran Hingga bulan <?=monthToString(date('m'))?></h2>
        <table class="table table-bordered table-hover table-striped md-data-table " id="tableKecamatan">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Kode</th>
                    <th>Kegiatan</th>
                    <th>Vol</th>
                    <th>Satuan</th>
                    <th>Harga</th>
                    <th>Pagu</th>
                    <th>UM dibayar</th>
                    <th>Sisa</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $no=1;
                foreach ($items as $item) {
                  $next=next($items);
                  $sisa=$item['pagu'];
                  if(empty($item['pagu'])){
                      $sisa=0;
                  }
                  ?>
                  <tr class="">
                      <td style="text-align:center"><?=$no?></td>
                      <td><?=$item['kode']?></td>
                      <td><?=$item['keterangan']?></td>
                      <td><?=$item['vol']?></td>
                      <td><?=$item['satuan']?></td>
                      <td style="text-align:right;"><?=(empty($item['harga']))?"":number_format($item['harga'], 0, "", ".")?></td>
                      <td style="text-align:right;"><?=(empty($item['pagu']))?"":number_format($item['pagu'], 0, "", ".")?></td>
                      <?php
                      if(strlen($item['kode'])==6 && empty($item['harga']) && $item['kode']==$next['kode']){
                      ?>
                        <td style="text-align:right;"><?=number_format($item['jumlah'], 0, "", ".")?></td>
                      <?php
                      $sisa=$sisa-$item['jumlah'];
                      }else if($item['harga']>0){
                      ?>
                        <td style="text-align:right;"><?=number_format($item['jumlah'], 0, "", ".")?></td>
                      <?php
                      $sisa=$sisa-$item['jumlah'];
                      }else if(strlen($item['kode'])!=6){
                      ?>
                        <td style="text-align:right;"><?=number_format($item['jumlah'], 0, "", ".")?></td>
                      <?php
                      $sisa=$sisa-$item['jumlah'];
                      }else{
                      ?>
                      <td style="text-align:right;"><?=number_format(0, 0, "", ".")?></td>
                      <?php
                      }
                      ?>
                      <td style="text-align:right;"><?=($sisa<0)?"":number_format($sisa, 0, "", ".")?></td>
                  </tr>
                  <?php
                  $no++;
                }
                ?>
            </tbody>
            <tfoot>
            </tfoot>

        </table>
      </div>
      <footer class="cm-footer"><span class="pull-right">&copy;</span></footer>
    </div>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/jquery.dataTables.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/dataTables.material.min.css') ?>">
    <script src="<?php echo base_url('assets/js/jquery.dataTables.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/dataTables.material.min.js'); ?>"></script>
      <script src="<?php echo base_url('assets/js/demo/popovers-tooltips.js'); ?>"></script>

