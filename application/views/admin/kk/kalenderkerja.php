    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap-clearmin.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.css') ?>">
    <div id="global">
      <div class="container-fluid">
        <hr></hr>
          <dl class="dl-horizontal">
            <dt>Tahun Anggaran</dt>
            <dd>: <?=$this->session->tahun_anggaran ?></dd>
            <dt>Program</dt>
            <dd>: PROGRAM PENINGKATAN PELAYANAN DAN PENEGEKAN HUKUM KEIMIGRASIAN</dd>
            <dt>Satker</dt>
            <dd>: KANTOR IMIGRASI KELAS I KHUSUS JAKARTA BARAT</dd>
            <dt>Bidang</dt>
            <dd>: <?=$this->user['seksi']?></dd>
          </dl>
        <hr></hr>
        <table class="table table-bordered table-hover table-striped md-data-table " id="tableKecamatan">
            <thead>
                <tr>
                    <th rowspan="3">Kode</th>
                    <th rowspan="3">Kegiatan</th>
                    <th colspan="12">Triwulan I</th>
                    <th colspan="12">Triwulan II</th>
                    <th colspan="12">Triwulan III</th>
                    <th colspan="12">Triwulan IV</th>
                </tr>
                <tr>
                    <th colspan="4">Januari</th>
                    <th colspan="4">Februari</th>
                    <th colspan="4">Maret</th>
                    <th colspan="4">April</th>
                    <th colspan="4">Mei</th>
                    <th colspan="4">Juni</th>
                    <th colspan="4">Juli</th>
                    <th colspan="4">Agustus</th>
                    <th colspan="4">September</th>
                    <th colspan="4">Oktober</th>
                    <th colspan="4">November</th>
                    <th colspan="4">Desember</th>
                </tr>
                <tr>
                    <th>1</th>
                    <th>2</th>
                    <th>3</th>
                    <th>4</th>
                    <th>1</th>
                    <th>2</th>
                    <th>3</th>
                    <th>4</th>
                    <th>1</th>
                    <th>2</th>
                    <th>3</th>
                    <th>4</th>
                    <th>1</th>
                    <th>2</th>
                    <th>3</th>
                    <th>4</th>
                    <th>1</th>
                    <th>2</th>
                    <th>3</th>
                    <th>4</th>
                    <th>1</th>
                    <th>2</th>
                    <th>3</th>
                    <th>4</th>
                    <th>1</th>
                    <th>2</th>
                    <th>3</th>
                    <th>4</th>
                    <th>1</th>
                    <th>2</th>
                    <th>3</th>
                    <th>4</th>
                    <th>1</th>
                    <th>2</th>
                    <th>3</th>
                    <th>4</th>
                    <th>1</th>
                    <th>2</th>
                    <th>3</th>
                    <th>4</th>
                    <th>1</th>
                    <th>2</th>
                    <th>3</th>
                    <th>4</th>
                    <th>1</th>
                    <th>2</th>
                    <th>3</th>
                    <th>4</th>
                </tr>
            </thead>
            <tbody>
            <?php
                foreach ($datakegiatan as $kegiatan) {
                  ?>
                    <tr class="">
                      <td><?=$kegiatan['kode_kegiatan']?></td>
                      <td><?=$kegiatan['kegiatan']?></td>
                      <?php
                        $this->user['seksi']="";

                      $query=$this->db->query('select * from kalender_kegiatan where id_kegiatan="'.$kegiatan['id_dipa'].'" ');
                      $items=array();
                      $items=$query->result_array();
                      $count=1;
                      $bulan=1;
                      if(!empty($items)){
                      foreach ($items as $item) {
                        $skip=0;
                          # code...
                        if($item['bulan_mulai']=='01'){
                            if($item['bulan_berakhir']!=="01"){
                                for($x=1;$x<=((($item['bulan_berakhir']-$item['bulan_mulai'])+1)*4);$x++){
                                    echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";
                                }
                            }else{
                                   if($item['minggu_mulai']==1) echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";   else echo "<td></td>";                                 
                                   if($item['minggu_mulai']<=2 && $item['minggu_terakhir']>=2) echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";   else echo "<td></td>";                                 
                                   if($item['minggu_mulai']<=3 && $item['minggu_terakhir']>=3) echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";   else echo "<td></td>";                                 
                                   if($item['minggu_terakhir']==4) echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";   else echo "<td></td>";                                 
                            }
                        }else{
                            $skip++;                            
                        }
                        $feb=0;
                        if($item['bulan_mulai']=='02'){
                            for($sk=1;$sk<=$skip;$sk++){
                                echo "<td></td><td></td><td></td><td></td>";
                            }
                            $skip=0;
                            $bulan=2;
                            if($item['bulan_berakhir']!=="02"){
                                for($x=1;$x<=((($item['bulan_berakhir']-$item['bulan_mulai'])+1)*4);$x++){
                                    echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";
                                }
                            }else{
                                   if($item['minggu_mulai']==1) echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";   else echo "<td></td>";                                 
                                   if($item['minggu_mulai']<=2 && $item['minggu_terakhir']>=2) echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";   else echo "<td></td>";                                 
                                   if($item['minggu_mulai']<=3 && $item['minggu_terakhir']>=3) echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";   else echo "<td></td>";                                 
                                   if($item['minggu_terakhir']==4) echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";   else echo "<td></td>";                                 
                            }
                        }else{
                            $skip++;                            
                        }
                        if($item['bulan_mulai']=='03'){
                            for($sk=$bulan;$sk<=$skip;$sk++){
                                echo "<td></td><td></td><td></td><td></td>";
                            }
                            $skip=0;
                            $bulan=3;
                            if($item['bulan_berakhir']!=="03"){
                                for($x=1;$x<=((($item['bulan_berakhir']-$item['bulan_mulai'])+1)*4);$x++){
                                    echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";
                                }
                            }else{
                                   if($item['minggu_mulai']==1) echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";   else echo "<td></td>";                                 
                                   if($item['minggu_mulai']<=2 && $item['minggu_terakhir']>=2) echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";   else echo "<td></td>";                                 
                                   if($item['minggu_mulai']<=3 && $item['minggu_terakhir']>=3) echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";   else echo "<td></td>";                                 
                                   if($item['minggu_terakhir']==4) echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";   else echo "<td></td>";                                 
                            }
                        }else{
                            $skip++;                            
                        }
                        if($item['bulan_mulai']=='04'){
                            for($sk=$bulan;$sk<=$skip;$sk++){
                                echo "<td></td><td></td><td></td><td></td>";
                            }
                            $skip=0;
                            $bulan=4;                            
                            if($item['bulan_berakhir']!=="04"){
                                for($x=1;$x<=((($item['bulan_berakhir']-$item['bulan_mulai'])+1)*4);$x++){
                                    echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";
                                }
                            }else{
                                   if($item['minggu_mulai']==1) echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";   else echo "<td></td>";                                 
                                   if($item['minggu_mulai']<=2 && $item['minggu_terakhir']>=2) echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";   else echo "<td></td>";                                 
                                   if($item['minggu_mulai']<=3 && $item['minggu_terakhir']>=3) echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";   else echo "<td></td>";                                 
                                   if($item['minggu_terakhir']==4) echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";   else echo "<td></td>";                                 
                            }
                        }else{
                            $skip++;                            
                        }
                        if($item['bulan_mulai']=='05'){
                            for($sk=$bulan;$sk<=$skip;$sk++){
                                echo "<td></td><td></td><td></td><td></td>";
                            }
                            $bulan=5;                            
                            $skip=0;
                            if($item['bulan_berakhir']!=="05"){
                                for($x=1;$x<=((($item['bulan_berakhir']-$item['bulan_mulai'])+1)*4);$x++){
                                    echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";
                                }
                            }else{
                                for($y=$item['minggu_mulai'];$y<=$item['minggu_terakhir'];$y++){
                                   if($y==1) echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";   else echo "<td></td>";                                 
                                   if($y==2) echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";   else echo "<td></td>";                                 
                                   if($y==3) echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";   else echo "<td></td>";                                 
                                   if($y==4) echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";   else echo "<td></td>";                                 
                                }
                            }
                        }else{
                            $skip++;                            
                        }
                        if($item['bulan_mulai']=='06'){
                            for($sk=$bulan;$sk<=$skip;$sk++){
                                echo "<td></td><td></td><td></td><td></td>";
                            }
                            $bulan=6;                            
                            $skip=0;
                            if($item['bulan_berakhir']!=="06"){
                                for($x=1;$x<=((($item['bulan_berakhir']-$item['bulan_mulai'])+1)*4);$x++){
                                    echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";
                                }
                            }else{
                                for($y=$item['minggu_mulai'];$y<=$item['minggu_terakhir'];$y++){
                                   if($y==1) echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";   else echo "<td></td>";                                 
                                   if($y==2) echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";   else echo "<td></td>";                                 
                                   if($y==3) echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";   else echo "<td></td>";                                 
                                   if($y==4) echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";   else echo "<td></td>";                                 
                                }
                            }
                        }else{
                            $skip++;                            
                        }
                        if($item['bulan_mulai']=='07'){
                            for($sk=$bulan;$sk<=$skip;$sk++){
                                echo "<td></td><td></td><td></td><td></td>";
                            }
                            $skip=0;
                            $bulan=7;                            
                            if($item['bulan_berakhir']!=="07"){
                                for($x=1;$x<=((($item['bulan_berakhir']-$item['bulan_mulai'])+1)*4);$x++){
                                    echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";
                                }
                            }else{
                                   if($item['minggu_mulai']==1) echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";   else echo "<td></td>";                                 
                                   if($item['minggu_mulai']<=2 && $item['minggu_terakhir']>=2) echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";   else echo "<td></td>";                                 
                                   if($item['minggu_mulai']<=3 && $item['minggu_terakhir']>=3) echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";   else echo "<td></td>";                                 
                                   if($item['minggu_terakhir']==4) echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";   else echo "<td></td>";                                 
                            }
                        }else{
                            $skip++;                            
                        }
                        if($item['bulan_mulai']=='08'){                            
                            for($sk=$bulan;$sk<=$skip;$sk++){
                                echo "<td></td><td></td><td></td><td></td>";
                            }
                            $skip=0;
                            $bulan=8;
                            if($item['bulan_berakhir']!=="08"){
                                for($x=1;$x<=((($item['bulan_berakhir']-$item['bulan_mulai'])+1)*4);$x++){
                                    echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";
                                }
                            }else{
                                /*for($y=$item['minggu_mulai'];$y<=$item['minggu_terakhir'];$y++){
                                   if($y==1) echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";   else echo "<td></td>";                                 
                                   if($y==2) echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";   else echo "<td></td>";                                 
                                   if($y==3) echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";   else echo "<td></td>";                                 
                                   if($y==4) echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";   else echo "<td></td>";                                 
                                }*/
                               // for($y=1;$y<=4;$y++){
                                   if($item['minggu_mulai']==1) echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";   else echo "<td></td>";                                 
                                   if($item['minggu_mulai']<=2 && $item['minggu_terakhir']>=2) echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";   else echo "<td></td>";                                 
                                   if($item['minggu_mulai']<=3 && $item['minggu_terakhir']>=3) echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";   else echo "<td></td>";                                 
                                   if($item['minggu_terakhir']==4) echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";   else echo "<td></td>";                                 
                               // }
                            }
                        }else{
                            $skip++;                            
                        }
                        if($item['bulan_mulai']=='09'){
                            for($sk=$bulan;$sk<=$skip;$sk++){
                                echo "<td></td><td></td><td></td><td></td>";
                            }
                            $bulan=9;
                            $skip=0;
                            if($item['bulan_berakhir']!=="09"){
                                for($x=1;$x<=((($item['bulan_berakhir']-$item['bulan_mulai'])+1)*4);$x++){
                                    echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";
                                }
                            }else{
                                   if($item['minggu_mulai']==1) echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";   else echo "<td></td>";                                 
                                   if($item['minggu_mulai']<=2 && $item['minggu_terakhir']>=2) echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";   else echo "<td></td>";                                 
                                   if($item['minggu_mulai']<=3 && $item['minggu_terakhir']>=3) echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";   else echo "<td></td>";                                 
                                   if($item['minggu_terakhir']==4) echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";   else echo "<td></td>";                                 
                            }
                        }else{
                            $skip++;                            
                        }
                        if($item['bulan_mulai']=='10'){
                            for($sk=$bulan;$sk<=$skip;$sk++){
                                echo "<td></td><td></td><td></td><td></td>";
                            }
                            $bulan=10;
                            $skip=0;
                            if($item['bulan_berakhir']!=="10"){
                                for($x=1;$x<=((($item['bulan_berakhir']-$item['bulan_mulai'])+1)*4);$x++){
                                    echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";
                                }
                            }else{
                                   if($item['minggu_mulai']==1) echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";   else echo "<td></td>";                                 
                                   if($item['minggu_mulai']<=2 && $item['minggu_terakhir']>=2) echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";   else echo "<td></td>";                                 
                                   if($item['minggu_mulai']<=3 && $item['minggu_terakhir']>=3) echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";   else echo "<td></td>";                                 
                                   if($item['minggu_terakhir']==4) echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";   else echo "<td></td>";                                 
                            }
                        }else{
                            $skip++;                            
                        }
                        if($item['bulan_mulai']=='11'){
                            for($sk=$bulan;$sk<=$skip;$sk++){
                                echo "<td></td><td></td><td></td><td></td>";
                            }
                            $bulan=11;                            
                            $skip=0;
                            if($item['bulan_berakhir']!=="11"){
                                for($x=1;$x<=((($item['bulan_berakhir']-$item['bulan_mulai'])+1)*4);$x++){
                                    echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";
                                }
                            }else{
                                   if($item['minggu_mulai']==1) echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";   else echo "<td></td>";                                 
                                   if($item['minggu_mulai']<=2 && $item['minggu_terakhir']>=2) echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";   else echo "<td></td>";                                 
                                   if($item['minggu_mulai']<=3 && $item['minggu_terakhir']>=3) echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";   else echo "<td></td>";                                 
                                   if($item['minggu_terakhir']==4) echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";   else echo "<td></td>";                                 
                            }
                        }else{
                            $skip++;                            
                        }
                        if($item['bulan_mulai']=='12'){
                            for($sk=$bulan;$sk<=$skip;$sk++){
                                echo "<td></td><td></td><td></td><td></td>";
                            }
                            $bulan=12;
                            $skip=0;
                            if($item['bulan_berakhir']!=="12"){
                                for($x=1;$x<=((($item['bulan_berakhir']-$item['bulan_mulai'])+1)*4);$x++){
                                    echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";
                                }
                            }else{
                                   if($item['minggu_mulai']==1) echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";   else echo "<td></td>";                                 
                                   if($item['minggu_mulai']<=2 && $item['minggu_terakhir']>=2) echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";   else echo "<td></td>";                                 
                                   if($item['minggu_mulai']<=3 && $item['minggu_terakhir']>=3) echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";   else echo "<td></td>";                                 
                                   if($item['minggu_terakhir']==4) echo "<td><img src='".base_url()."/assets/img/sf/sign-check.svg' width='24' height='24'></td>";   else echo "<td></td>";                                 
                            }
                        }else{
                            $skip++;                            
                        }

                      }
                    }else{
                    ?>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <?php
                    }
                      ?>
                    </tr>
            <?php
                }
            ?>
            </tbody>
        </table>
      </div>
      <footer class="cm-footer"><span class="pull-right">&copy;</span></footer>
    </div>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/jquery.dataTables.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/dataTables.material.min.css') ?>">
    <script src="<?php echo base_url('assets/js/jquery.dataTables.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/dataTables.material.min.js'); ?>"></script>
    <script type="text/javascript">
    $(document).ready(function(){
      $('#tanggal_mulai').mask('00-00-0000');
    });
    </script>
