    <div id="global">
      <div class="container-fluid cm-container-white">
        <div class="bs-example">
          <a href="<?php echo base_url('admin/kk/add'); ?>" class="btn btn-info"><i class="fa-plus"></i> Tambah</a>
        </div>
        <hr></hr>
          <form class="form-horizontal" action="<?php echo base_url('admin/kk/search'); ?>" method="post" accept-charset="utf-8">
            <input type="hidden" name="<?=$csrf['name'] ?>" value="<?=$csrf['hash'] ?>">
            <div class="form-group <?php if(!empty(form_error('id_kegiatan')))echo "has-error"; ?>">
                <label for="id_kegiatan" class="col-sm-2 control-label">Kegiatan</label>
                <div class="col-sm-10">
                    <select name="id_kegiatan" class="form-control" id="id_kegiatan">
                        <option value="">Pilih Kegiatan</option>
                    <?php
                        foreach ($datakegiatan as $kegiatan) {
                            # code...
                            if($kegiatan['id_dipa']==$search['kegiatan'])$sel="selected=selected"; else $sel="";
                        ?>
                        <option value="<?=$kegiatan['id_dipa']?>" <?=$sel;?> ><?=$kegiatan['kegiatan']?></option>
                        <?php
                        }
                    ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="tanggal_mulai" class="col-sm-2 control-label">Tanggal Mulai</label>
                <div class="col-sm-10">
                    <input type="date" maxlength="10" data-mask="99-99-9999" class="form-control" id="tanggal_mulai" name="tanggal_mulai" placeholder="tanggal mulai" value="<?php echo $search['tanggal_mulai']; ?>">
                    <span class="help-block has-error"></span>
                </div>
            </div>
            <div class="form-group">
                <label for="minggu_mulai" class="col-sm-2 control-label">Minggu</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="minggu_mulai" name="minggu_mulai" placeholder="minggu mulai" value="<?php echo $search['minggu_mulai']; ?>">
                    <span class="help-block has-error"></span>
                </div>
            </div>
            <div class="form-group" style="margin-bottom:0">
                <div class="col-sm-offset-2 col-sm-10 text-right">
                    <button type="reset" class="btn btn-default">Cancel</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
          </form>
        <hr></hr>
        <table class="table table-bordered table-hover table-striped md-data-table " id="tableKecamatan">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Kode</th>
                    <th>Kegiatan</th>
                    <th>Tgl Mulai</th>
                    <th>Waktu Kegiatan</th>
                    <th>Pejabat Terkait</th>
                    <th>Pilihan</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $no=$offset+1;
                foreach ($items as $item) {
                  ?>
                  <tr class="">
                      <td style="text-align:center"><?=$no?></td>
                      <td><?=$item['kode_kegiatan']?></td>
                      <td><?=$item['kegiatan']?></td>
                      <td><?=$item['tgl_mulai']?></td>
                      <td><?php echo "minggu ke ".$item['minggu_mulai']." bulan ".monthToString($item['bulan_mulai'])." Sampai minggu ke ".$item['minggu_terakhir']." bulan ".monthToString($item['bulan_berakhir']); ?></td>
                      <td><?=$item['surname']?></td>
                      <td>
                        <div class="dropdown">
                        <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Pilihan
                        <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                        <li><a href="<?=base_url("admin/kk/modify/{$item['id']}")?>">Edit</a></li>
                        <li><a href="#" onclick="app_confirm('<?=base_url('admin/kk/delete/'.$item['id'])?>','Apakah Yakin Ingin Menghapus Data Ini?')" >Hapus</a></li>

                        </ul>
                        </div>
                      </td>
                  </tr>
                  <?php
                  $no++;
                }
                ?>
            </tbody>
        </table>
        <tfoot>
          <tr colspan="7"><?=$paging?></tr>
        </tfoot>
      </div>
      <footer class="cm-footer"><span class="pull-right">&copy;</span></footer>
    </div>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/jquery.dataTables.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/dataTables.material.min.css') ?>">
    <script src="<?php echo base_url('assets/js/jquery.dataTables.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/dataTables.material.min.js'); ?>"></script>
    <script type="text/javascript">
    $(document).ready(function(){
      $('#tanggal_mulai').mask('00-00-0000');
    });
    </script>
