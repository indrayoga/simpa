    <div id="global">
      <div class="container-fluid">
        <div class="row cm-fix-height">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">Tambah Kalender Kegiatan</div>
                <div class="panel-body">
                    <form class="form-horizontal" method="post" action="<?=base_url('admin/kk/create') ?>">
                        <input type="hidden" name="<?=$csrf['name'] ?>" value="<?=$csrf['hash'] ?>">
                        <div class="form-group <?php if(!empty(form_error('tahun_anggaran')))echo "has-error"; ?>">
                            <label for="tahun_anggaran" class="col-sm-2 control-label">Tahun Anggaran</label>
                            <div class="col-sm-10">
                                <input type="text" readonly class="form-control" id="tahun_anggaran" name="tahun_anggaran" placeholder="tahun anggaran" value="<?php echo $this->session->tahun_anggaran; ?>">
                                <span class="help-block has-error"><?php echo form_error('tahun_anggaran'); ?></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="program" class="col-sm-2 control-label">Program</label>
                            <div class="col-sm-10">
                                <input type="text" disabled class="form-control" value="PROGRAM PENINGKATAN PELAYANAN DAN PENEGEKAN HUKUM KEIMIGRASIAN">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="program" class="col-sm-2 control-label">Satker</label>
                            <div class="col-sm-10">
                                <input type="text" disabled class="form-control" value="KANTOR IMIGRASI KELAS I KHUSUS JAKARTA BARAT">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="id_output" class="col-sm-2 control-label">Output</label>
                            <div class="col-sm-10">
                                <select name="id_output" class="form-control" id="id_output">
                                    <option value="">Pilih Output</option>
                                <?php
                                    foreach ($dataoutput as $output) {
                                    ?>
                                    <option value="<?=$output['id_dipa']?>" ><?=$output['kegiatan']?></option>
                                    <?php
                                    }
                                ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group" id="divsuboutput" style="display:none;">
                            <label for="id_sub_output" class="col-sm-2 control-label">Sub Output</label>
                            <div class="col-sm-10">
                                <select name="id_sub_output" class="form-control" id="id_sub_output">
                                    <option value="">Pilih Sub Output</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group" id="divkomponenkegiatan" style="display:none;">
                            <label for="id_komponen_kegiatan" class="col-sm-2 control-label">Komponen Kegiatan</label>
                            <div class="col-sm-10">
                                <select name="id_komponen_kegiatan" class="form-control" id="id_komponen_kegiatan">
                                    <option value="">Pilih Komponen Kegiatan</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group" id="divsubkomponenkegiatan" style="display:none;">
                            <label for="id_sub_komponen_kegiatan" class="col-sm-2 control-label">Kegiatan</label>
                            <div class="col-sm-10">
                                <select name="id_sub_komponen_kegiatan" class="form-control" id="id_sub_komponen_kegiatan">
                                </select>
                            </div>
                        </div>
                        <div class="form-group <?php if(!empty(form_error('tgl_mulai')))echo "has-error"; ?>">
                            <label for="tgl_mulai" class="col-sm-2 control-label">Tanggal Mulai</label>
                            <div class="col-sm-10">
                                <input type="text" data-toggle="datepicker" maxlength="10" data-mask="99-99-9999" class="form-control" required id="tgl_mulai" name="tgl_mulai" placeholder="tanggal mulai" value="<?php echo set_value('tgl_mulai'); ?>">
                                <span class="help-block has-error"><?php echo form_error('tgl_mulai'); ?></span>
                            </div>
                        </div>
                        <div class="form-group <?php if(!empty(form_error('bulan_mulai')) || !empty(form_error('minggu_mulai')) )echo "has-error"; ?>">
                            <label for="bulan_mulai" class="col-sm-2 control-label">Bulan Mulai</label>
                            <div class="col-sm-4">
                                <select name="bulan_mulai" id="bulan_mulai" class="form-control">
                                    <option value="01">Januari</option>
                                    <option value="02">Februari</option>
                                    <option value="03">Maret</option>
                                    <option value="04">April</option>
                                    <option value="05">Mei</option>
                                    <option value="06">Juni</option>
                                    <option value="07">Juli</option>
                                    <option value="08">Agustus</option>
                                    <option value="09">September</option>
                                    <option value="10">Oktober</option>
                                    <option value="11">November</option>
                                    <option value="12">Desember</option>
                                </select>
                                <span class="help-block has-error"><?php echo form_error('bulan_mulai'); ?></span>
                            </div>
                            <label for="minggu_mulai" class="col-sm-2 control-label">Minggu Mulai</label>
                            <div class="col-sm-4">
                                <input type="number" min=1 max=4 class="form-control" id="minggu_mulai" name="minggu_mulai" placeholder="minggu mulai" value="<?php echo set_value('minggu_mulai'); ?>">
                                <span class="help-block has-error"><?php echo form_error('minggu_mulai'); ?></span>
                            </div>
                        </div>
                        <div class="form-group <?php if(!empty(form_error('bulan_berakhir')))echo "has-error"; ?>">
                            <label for="bulan_berakhir" class="col-sm-2 control-label">Bulan Berakhir</label>
                            <div class="col-sm-4">
                                <select name="bulan_berakhir" id="bulan_berakhir" class="form-control">
                                    <option value="01">Januari</option>
                                    <option value="02">Februari</option>
                                    <option value="03">Maret</option>
                                    <option value="04">April</option>
                                    <option value="05">Mei</option>
                                    <option value="06">Juni</option>
                                    <option value="07">Juli</option>
                                    <option value="08">Agustus</option>
                                    <option value="09">September</option>
                                    <option value="10">Oktober</option>
                                    <option value="11">November</option>
                                    <option value="12">Desember</option>
                                </select>
                                <span class="help-block has-error"><?php echo form_error('bulan_berakhir'); ?></span>
                            </div>
                            <label for="minggu_terakhir" class="col-sm-2 control-label">Minggu Berakhir</label>
                            <div class="col-sm-4">
                                <input type="number" min=1 max=4 class="form-control" id="minggu_terakhir" name="minggu_terakhir" placeholder="minggu terakhir" value="<?php echo set_value('minggu_terakhir'); ?>">
                                <span class="help-block has-error"><?php echo form_error('minggu_terakhir'); ?></span>
                            </div>
                        </div>
                        <div style="display:none;" class="form-group <?php if(!empty(form_error('tgl_berakhir')))echo "has-error"; ?>">
                            <label for="tgl_berakhir" class="col-sm-2 control-label">Tanggal Berakhir</label>
                            <div class="col-sm-10">
                                <input type="date" maxlength="10" data-mask="99-99-9999" class="form-control" id="tgl_berakhir" name="tgl_berakhir" placeholder="tanggal berakhir" value="<?php echo set_value('tgl_berakhir'); ?>">
                                <span class="help-block has-error"><?php echo form_error('tgl_berakhir'); ?></span>
                            </div>
                        </div>
                        <div class="form-group <?php if(!empty(form_error('id_user')))echo "has-error"; ?>">
                            <label for="id_user" class="col-sm-2 control-label">Pejabat Terkait</label>
                            <div class="col-sm-10">
                                <select name="id_user" id="id_user" class="form-control">
                                <?php
                                foreach ($datapejabat as $pejabat) {
                                    # code...
                                    if($pejabat['id']==$this->user['id'])$sel="selected=selected"; else $sel="";
                                    echo "<option value='".$pejabat['id']."' ".$sel.">".$pejabat['surname']."</option>";
                                }
                                ?>
                                </select>
                                <span class="help-block has-error"><?php echo form_error('id_user'); ?></span>
                            </div>
                        </div>
                        <div class="form-group <?php if(!empty(form_error('keterangan')))echo "has-error"; ?>">
                            <label for="keterangan" class="col-sm-2 control-label">Keterangan</label>
                            <div class="col-sm-10">
                                <textarea name="keterangan" id="keterangan" class="form-control"><?=set_value('keterangan')?></textarea>
                                <span class="help-block has-error"><?php echo form_error('keterangan'); ?></span>
                            </div>
                        </div>                        
                        <div class="form-group" style="margin-bottom:0">
                            <div class="col-sm-offset-2 col-sm-10 text-right">
                                <button type="reset" class="btn btn-default">Reset</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
      </div>
      <footer class="cm-footer"><span class="pull-right">&copy;</span></footer>
    </div>
    </div>
<script type="text/javascript">
$(document).ready(function(){
    $('#tgl_awal,#tgl_akhir').mask('00-00-0000');
    //$('#id_kegiatan').select2();
    $('#id_output').change(function(){
        $('#divsuboutput').hide();
        $('#divkomponenkegiatan').hide();
        $('#divsubkomponenkegiatan').hide();
        $('#id_sub_output').empty();
        $('#id_komponen_kegiatan').empty();
        $('#id_sub_komponen_kegiatan').empty();
        $('#id_sub_output').append('<option value="">Pilih</option>');
        $('#id_komponen_kegiatan').append('<option value="">Pilih</option>');
        $.ajax({
            url: base_url+'ajax/suboutput',
            type: 'POST',
            dataType: 'json',
            data: {id_output: $(this).val()},

        })
        .done(function(data) {
            console.log(data);
            $.each(data, function(index, val) {
                 /* iterate through array or object */
                if(val['kode_kegiatan'].length>3){
                    $('#id_sub_output').append('<option value="'+val['id_dipa']+'">'+val['kegiatan']+'</option>');
                    $('#divsuboutput').show();
                }else{
                    $('#id_komponen_kegiatan').append('<option value="'+val['id_dipa']+'">'+val['kegiatan']+'</option>');
                    $('#divkomponenkegiatan').show();
                }
            });
        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });

        $.ajax({
            url: base_url+'ajax/userpemohon',
            type: 'POST',
            dataType: 'json',
            data: {id_output: $(this).val()},

        })
        .done(function(data) {
            $('#id_user').empty();
            $.each(data, function(index, val) {
                $('#id_user').append('<option value="'+val['id']+'">'+val['surname']+'</option>');
            });
        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });
        
    });

    $('#id_sub_output, #id_komponen_kegiatan').change(function(){
        $('#divsubkomponenkegiatan').hide();
        $('#id_sub_komponen_kegiatan').empty();
        $.ajax({
            url: base_url+'ajax/subkomponen',
            type: 'POST',
            dataType: 'json',
            data: {id_induk: $(this).val()},

        })
        .done(function(data) {
            console.log(data);
            if(data.length==0)$('#divsubkomponenkegiatan').hide();
            $.each(data, function(index, val) {
                 /* iterate through array or object */
                if(val['kode_kegiatan'].length>3){
                    $('#id_komponen_kegiatan').append('<option value="'+val['id_dipa']+'">'+val['kegiatan']+'</option>');
                    $('#divkomponenkegiatan').show();
                   $('#id_komponen_kegiatan').trigger('change');
                }else{
                    $('#id_sub_komponen_kegiatan').append('<option value="'+val['id_dipa']+'">'+val['kegiatan']+'</option>');
                    $('#divsubkomponenkegiatan').show();
                    $('#id_sub_komponen_kegiatan').trigger('change');
                }
            });
        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });
        
    });

    $('#id_komponen_kegiatan, #id_sub_komponen_kegiatan').change(function(){
        $.ajax({
            url: base_url+'ajax/akunkegiatan',
            type: 'POST',
            dataType: 'json',
            data: {id: $(this).val()},

        })
        .done(function(data) {
            $('.akun').empty();
            $.each(data, function(index, val) {
                $('.akun').append('<option value="'+val['kode_kegiatan']+'">'+val['kegiatan']+'</option>');
            });
        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });
        
    });

});

    $(function () {
        $('[data-toggle="datepicker"]').datepicker({
          format: 'dd-mm-yyyy'
        });
    });
</script>
