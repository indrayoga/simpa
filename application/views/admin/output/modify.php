    <div id="global">
      <div class="container-fluid">
        <div class="row cm-fix-height">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">Edit Output</div>
                <div class="panel-body">
                    <form class="form-horizontal" method="post" action="<?=base_url('admin/output/update') ?>">
                        <input type="hidden" name="<?=$csrf['name'] ?>" value="<?=$csrf['hash'] ?>">
                        <input type="hidden" name="id" value="<?=$id ?>">
                        <div class="form-group <?php if(!empty(form_error('tahun_anggaran')))echo "has-error"; ?>">
                            <label for="tahun_anggaran" class="col-sm-2 control-label">Tahun Anggaran</label>
                            <div class="col-sm-10">
                                <input type="text" maxlength="4" readonly class="form-control" required id="tahun_anggaran" name="tahun_anggaran" placeholder="tahun_anggaran" value="<?php if(set_value('tahun_anggaran')=="")echo $item['tahun_anggaran']; else echo set_value('tahun_anggaran'); ?>">
                                <span class="help-block has-error"><?php echo form_error('tahun_anggaran'); ?></span>
                            </div>
                        </div>
                        <div class="form-group <?php if(!empty(form_error('kode_bidang')))echo "has-error"; ?>">
                            <label for="kode_bidang" class="col-sm-2 control-label">Bidang</label>
                            <div class="col-sm-10">
                                <select name="kode_bidang" id="kode_bidang" class="form-control">
                                    <?php
                                    foreach ($databidang as $bidang) {
                                        # code...
                                        if($bidang['kode_bidang']==$item['kode_bidang'])$sel="selected=selected"; else $sel="";
                                    ?>
                                    <option value="<?=$bidang['kode_bidang']?>" <?=$sel?> ><?=$bidang['nama_bidang']?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                                <span class="help-block has-error"><?php echo form_error('kode_bidang'); ?></span>
                            </div>
                        </div>
                        <div class="form-group <?php if(!empty(form_error('kode_kegiatan')))echo "has-error"; ?>">
                            <label for="kode_kegiatan" class="col-sm-2 control-label">Kode</label>
                            <div class="col-sm-10">
                                <input type="text" maxlength="100" minlength="1" class="form-control" required id="kode_kegiatan" name="kode_kegiatan" placeholder="kode_kegiatan" value="<?php if(set_value('kode_kegiatan')=="")echo $item['kode_kegiatan']; else echo set_value('kode_kegiatan'); ?>">
                                <span class="help-block has-error"><?php echo form_error('kode_kegiatan'); ?></span>
                            </div>
                        </div>
                        <div class="form-group <?php if(!empty(form_error('kegiatan')))echo "has-error"; ?>">
                            <label for="kegiatan" class="col-sm-2 control-label">Output</label>
                            <div class="col-sm-10">
                                <input type="text" maxlength="100" minlength="1" class="form-control" required id="kegiatan" name="kegiatan" placeholder="kegiatan" value="<?php if(set_value('kegiatan')=="")echo $item['kegiatan']; else echo set_value('kegiatan'); ?>">
                                <span class="help-block has-error"><?php echo form_error('kegiatan'); ?></span>
                            </div>
                        </div>
                        <div class="form-group" style="margin-bottom:0">
                            <div class="col-sm-offset-2 col-sm-10 text-right">
                                <button type="reset" class="btn btn-default">Reset</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
      </div>
      <footer class="cm-footer"><span class="pull-right">&copy;</span></footer>
    </div>
    </div>