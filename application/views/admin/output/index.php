    <div id="global">
      <div class="container-fluid cm-container-white">
        <div class="bs-example">
          <a href="<?php echo base_url('admin/output/add'); ?>" class="btn btn-info"><i class="fa-plus"></i> Tambah</a>
        </div>
        <form class="form-horizontal" method="" action="">
            <div class="form-group <?php if(!empty(form_error('tahun_anggaran')))echo "has-error"; ?>">
                <label for="tahun_anggaran" class="col-sm-2 control-label">Tahun Anggaran</label>
                <div class="col-sm-10">
                    <input type="text" readonly class="form-control" id="tahun_anggaran" name="tahun_anggaran" placeholder="tahun anggaran" value="<?php echo $this->session->tahun_anggaran; ?>">
                    <span class="help-block has-error"><?php echo form_error('tahun_anggaran'); ?></span>
                </div>
            </div>
            <div class="form-group">
                <label for="program" class="col-sm-2 control-label">Program</label>
                <div class="col-sm-10">
                    <input type="text" disabled class="form-control" value="PROGRAM PENINGKATAN PELAYANAN DAN PENEGEKAN HUKUM KEIMIGRASIAN">
                </div>
            </div>
            <div class="form-group">
                <label for="program" class="col-sm-2 control-label">Satker</label>
                <div class="col-sm-10">
                    <input type="text" disabled class="form-control" value="KANTOR IMIGRASI KELAS I KHUSUS JAKARTA BARAT">
                </div>
            </div>
        </form>
        <hr></hr>
        <table class="table table-bordered table-hover table-striped md-data-table " id="tableKecamatan">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Kode</th>
                    <th>Bidang</th>
                    <th>Output</th>
                    <th>Pilihan</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $no=1;
                foreach ($items as $item) {
                  ?>
                  <tr class="">
                      <td style="text-align:center"><?=$no?></td>
                      <td><?=$item['kode_kegiatan']?></td>
                      <td><?=strtoupper($item['kode_bidang'])?></td>
                      <td><?=$item['kegiatan']?></td>
                      <td>
                        <div class="dropdown">
                        <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Pilihan
                        <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                        <li><a href="<?=base_url("admin/output/modify/{$item['id_dipa']}")?>">Edit</a></li>
                        <li><a href="#" onclick="app_confirm('<?=base_url('admin/output/delete/'.$item['id_dipa'])?>','Apakah Yakin Ingin Menghapus Data Ini?')" >Hapus</a></li>
                        </ul>
                        </div>
                      </td>
                  </tr>
                  <?php
                  $no++;
                }
                ?>
            </tbody>
        </table>
      </div>
      <footer class="cm-footer"><span class="pull-right">&copy;</span></footer>
    </div>
