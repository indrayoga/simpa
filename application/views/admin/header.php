<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap-clearmin.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/roboto.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/material-design.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/small-n-flat.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/font-awesome.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datepicker.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/fullcalendar.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/fullcalendar.print.css') ?>" media="print">
    <script src="<?php echo base_url('assets/js/lib/jquery-2.1.3.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/datepicker.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/moment.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/fullcalendar.min.js'); ?>"></script>
    <title>Dashboard</title>
    <script type="text/javascript">
      var base_url="<?php echo base_url() ?>";
    </script>
  </head>
  <body class="cm-no-transition <?php echo (isset($navbar2))?"cm-2-navbar":"cm-1-navbar"; ?> ">
    <div id="cm-menu">
      <nav class="cm-navbar cm-navbar-primary">
        <div class="cm-flex"></div>
        <div class="btn btn-primary md-menu-white" data-toggle="cm-menu"></div>
      </nav>
      <div id="cm-menu-content">
        <div id="cm-menu-items-wrapper">
          <div id="cm-menu-scroller">
            <ul class="cm-menu-items">
              <li><a href="<?php echo base_url('admin/dashboard') ?>" class="sf-house">Beranda</a></li>
              <li><a href="<?php echo base_url('admin/bidang') ?>" class="sf-window-layout">Bidang DIPA</a></li>
              <li><a href="<?php echo base_url('admin/output') ?>" class="sf-folder-document">Output DIPA</a></li>
              <!--<li><a href="<?php echo base_url('admin/suboutput') ?>" class="sf-dashboard">Sub Output DIPA</a></li>-->
              <li><a href="<?php echo base_url('admin/dipa') ?>" class="sf-file-note">Sub output/Komponen Kegiatan DIPA</a></li>
              <li><a href="<?php echo base_url('admin/akunanggaran') ?>" class="sf-money">Akun Anggaran DIPA</a></li>
              <li><a href="<?php echo base_url('admin/akun') ?>" class="sf-dashboard">Akun</a></li>
              <li><a href="<?php echo base_url('admin/anggaran') ?>" class="sf-dashboard">Tahun Anggaran</a></li>
              <li class="cm-submenu">
                  <a class="sf-calendar">Kalender Kegiatan <span class="caret"></span></a>
                  <ul>
                      <li><a href="<?php echo base_url('admin/kk/add') ?>">Tambah</a></li>
                      <li><a href="<?php echo base_url('admin/kk') ?>">Daftar</a></li>
                      <li><a href="<?php echo base_url('admin/kk/kalender') ?>">View Monthly</a></li>
                  </ul>
              </li>              
              <li><a href="<?php echo base_url('admin/user') ?>" class="sf-profile-group">User</a></li>
              <li class="cm-submenu">
                  <a class="sf-window-layout">Laporan <span class="caret"></span></a>
                  <ul>
                        <li><a href="<?php echo base_url('admin/um/diajukan') ?>">Pengajuan Yang di Ajukan</a></li>
                        <li><a href="<?php echo base_url('admin/um/disetujui') ?>">Pengajuan Yang di Setujui</a></li>
                        <li><a href="<?php echo base_url('admin/um/lulus') ?>">Pengajuan Yang Lulus</a></li>
                        <li><a href="<?php echo base_url('admin/um/selesai') ?>">Pengajuan Yang Selesai</a></li>
                      <li><a href="<?php echo base_url('admin/realisasi') ?>">Realisasi Anggaran</a></li>
                      <li><a href="<?php echo base_url('admin/statistik') ?>">Statistik</a></li>
                  </ul>
              </li>                            
            </ul>
          </div>
        </div>
      </div>
    </div>
    <header id="cm-header">
      <nav class="cm-navbar cm-navbar-primary">
        <div class="btn btn-primary md-menu-white hidden-md hidden-lg" data-toggle="cm-menu"></div>
        <div class="cm-flex"><h1>Dashboard</h1></div>
      <div class="dropdown pull-right">
          <button class="btn btn-primary md-account-circle-white" data-toggle="dropdown"></button>
          <ul class="dropdown-menu">
              <li class="disabled text-center">
                  <a style="cursor:default;"><strong>Username</strong></a>
              </li>
              <li class="divider"></li>
              <li>
                  <a href="<?=base_url('logout') ?>"><i class="fa fa-fw fa-sign-out"></i> Sign out</a>
              </li>
          </ul>
      </div>

      </nav>
    </header>
