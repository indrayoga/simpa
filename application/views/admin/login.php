<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/css/bootstrap-clearmin.min.css');?>">
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/css/roboto.css');?>">
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/css/font-awesome.min.css');?>">
    <title>Login Masuk</title>
    <style>
      body { 
        background: url(<?=base_url()?>assets/img/background.jpg) no-repeat center center fixed; 
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
      }      
    </style>
  </head>
  <body class="">
    <div class="col-sm-6 col-md-4 col-lg-3" style="margin:40px auto; float:none;background-color: blue;">
      <form method="post" action="<?=base_url('/admin/login');?>" style="background-color: blue;">
      <?php echo validation_errors(); ?>
      <input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
    	<div class="col-xs-12 well" style="background-color: blue;">
              <div class="form-group">
    	    <div class="input-group">
    	      <div class="input-group-addon"><i class="fa fa-fw fa-user"></i></div>
    	      <input type="text" name="username" class="form-control" placeholder="Username">
    	    </div>
              </div>
              <div class="form-group">
    	    <div class="input-group">
    	      <div class="input-group-addon"><i class="fa fa-fw fa-lock"></i></div>
    	      <input type="password" name="password" class="form-control" placeholder="Password">
    	    </div>
              </div>
            </div>
          <div class="col-xs-6">
              <div class="checkbox"></div>
        	</div>
          <div class="col-xs-6">
            <button type="submit" class="btn btn-block btn-primary">Sign in</button>
        </div>
      </form>
    </div>
  </body>
</html>
