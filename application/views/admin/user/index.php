    <div id="global">
      <div class="container-fluid cm-container-white">
        <div class="bs-example">
          <a href="<?php echo base_url('admin/user/add'); ?>" class="btn btn-info"><i class="fa-plus"></i> Tambah</a>
        </div>
        <hr></hr>
        <table class="table table-bordered table-hover table-striped md-data-table " id="tableKecamatan">
            <thead>
                <tr>
                    <th>#</th>
                    <th>NAMA LENGKAP</th>
                    <th>NIP</th>
                    <th>USERNAME</th>
                    <th>NO HP</th>
                    <th>ROLE</th>
                    <th>SEKSI</th>
                    <th>STATUS</th>
                    <th>LOGIN TERAKHIR</th>
                    <th>ADMINISTRATOR</th>
                    <th>PILIHAN</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $no=1;
                foreach ($items as $item) {
                  ?>
                  <tr class="">
                      <td style="text-align:center"><?=$no?></td>
                      <td><?=$item['surname']?></td>
                      <td><?=$item['nip']?></td>
                      <td><?=$item['username']?></td>
                      <td><?=$item['no_hp']?></td>
                      <td><?=($item['is_admin'])?'Super Admin':$item['role']?></td>
                      <td><?=($item['is_admin'])?'Super Admin':$item['seksi']?></td>
                      <td><?=$item['status']?></td>
                      <td><?=$item['last_login']?></td>
                      <td style="text-align:center;"><?=($item['is_admin'])?'<img src="'.base_url().'/assets/img/sf/sign-check.svg" width="24" height="24">':'<img src="'.base_url().'/assets/img/sf/sign-error.svg" width="24" height="24">'?></td>
                      <td>
                        <div class="dropdown">
                        <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Pilihan
                        <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                        <li><a href="<?=base_url("admin/user/modify/{$item['id']}")?>">Edit</a></li>
                        <li><a href="#" onclick="app_confirm('<?=base_url('admin/user/block/'.$item['id'].'/')?><?=($item['status']=='AKTIF')?'1':'0'?>','Apakah Yakin Ingin Blokir Data Ini?')" ><?=($item['status']=='AKTIF')?'Blokir':'Aktifkan'?></a></li>
                        <li><a href="#" onclick="app_confirm('<?=base_url('admin/user/delete/'.$item['id'])?>','Apakah Yakin Ingin Menghapus Data Ini?')" >Hapus</a></li>

                        </ul>
                        </div>
                      </td>
                  </tr>
                  <?php
                  $no++;
                }
                ?>
            </tbody>
        </table>
      </div>
      <footer class="cm-footer"><span class="pull-right">&copy;</span></footer>
    </div>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/jquery.dataTables.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/dataTables.material.min.css') ?>">
    <script src="<?php echo base_url('assets/js/jquery.dataTables.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/dataTables.material.min.js'); ?>"></script>
