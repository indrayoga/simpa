    <div id="global">
      <div class="container-fluid">
        <div class="row cm-fix-height">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">Tambah Akun Anggaran Pada Kegiatan <?=$item['kegiatan']?></div>
                <div class="panel-body">
                    <form class="form-horizontal" method="post" action="<?=base_url('admin/akunanggaran/create') ?>">
                        <input type="hidden" name="<?=$csrf['name'] ?>" value="<?=$csrf['hash'] ?>">
                        <input type="hidden" name="id" value="<?=$id ?>">
                        <div class="form-group <?php if(!empty(form_error('tahun_anggaran')))echo "has-error"; ?>">
                            <label for="tahun_anggaran" class="col-sm-2 control-label">Tahun Anggaran</label>
                            <div class="col-sm-10">
                                <input type="text" maxlength="4" class="form-control" readonly required id="tahun_anggaran" name="tahun_anggaran" placeholder="tahun_anggaran" value="<?php if(set_value('tahun_anggaran')=="")echo $item['tahun_anggaran']; else echo set_value('tahun_anggaran'); ?>">
                                <span class="help-block has-error"><?php echo form_error('tahun_anggaran'); ?></span>
                            </div>
                        </div>
                        <div class="form-group <?php if(!empty(form_error('kode_bidang')))echo "has-error"; ?>">
                            <label for="kode_bidang" class="col-sm-2 control-label">Bidang</label>
                            <div class="col-sm-10">
                                <select name="kode_bidang" readonly id="kode_bidang" class="form-control">
                                    <?php
                                    foreach ($databidang as $bidang) {
                                        # code...
                                        if($bidang['kode_bidang']==$item['kode_bidang'])$sel="selected=selected"; else $sel="";
                                    ?>
                                    <option value="<?=$bidang['kode_bidang']?>" <?=$sel?> ><?=$bidang['nama_bidang']?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                                <span class="help-block has-error"><?php echo form_error('kode_bidang'); ?></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="nama_kegiatan" class="col-sm-2 control-label">Kegiatan</label>
                            <div class="col-sm-10">
                                <input type="text"class="form-control" disabled  value="<?=$item['kegiatan'] ?>">
                            </div>
                        </div>
                        <div class="form-group <?php if(!empty(form_error('kode_kegiatan')))echo "has-error"; ?>">
                            <label for="kode_kegiatan" class="col-sm-2 control-label">Kode</label>
                            <div class="col-sm-10">
                                <select name="kode_kegiatan" id="kode_kegiatan" class="form-control">
                                    <?php
                                    foreach ($dataakun as $akun) {
                                        # code...
                                        if($akun['kode_akun']==set_value('kode_kegiatan'))$sel="selected=selected"; else $sel="";
                                    ?>
                                    <option value="<?=$akun['kode_akun']?>" <?=$sel?> ><?=$akun['nama_akun']?></option>
                                    <?php
                                    }
                                    ?>
                                </select>

                                <span class="help-block has-error"><?php echo form_error('kode_kegiatan'); ?></span>
                            </div>
                        </div>
                        <div class="form-group <?php if(!empty(form_error('pagu')))echo "has-error"; ?>">
                            <label for="pagu" class="col-sm-2 control-label">Jumlah</label>
                            <div class="col-sm-10">
                                <input type="number" class="form-control" required id="pagu" name="pagu" placeholder="Jumlah Pagu" value="<?php echo set_value('pagu'); ?>">
                                <span class="help-block has-error"><?php echo form_error('pagu'); ?></span>
                            </div>
                        </div>
                        <div class="form-group" style="margin-bottom:0">
                            <div class="col-sm-offset-2 col-sm-10 text-right">
                                <button type="reset" class="btn btn-default">Reset</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
      </div>
      <footer class="cm-footer"><span class="pull-right">&copy;</span></footer>
    </div>
    </div>