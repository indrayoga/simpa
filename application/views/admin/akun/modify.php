    <div id="global">
      <div class="container-fluid">
        <div class="row cm-fix-height">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">Ubah Akun</div>
                <div class="panel-body">
                    <form class="form-horizontal" method="post" action="<?=base_url('index.php/admin/akun/update') ?>">
                        <input type="hidden" name="<?=$csrf['name'] ?>" value="<?=$csrf['hash'] ?>">
                        <input type="hidden" name="kode_akun_lama" value="<?=$item['kode_akun']; ?>">
                        <div class="form-group <?php if(!empty(form_error('kode_akun')))echo "has-error"; ?>">
                            <label for="kode_akun" class="col-sm-2 control-label">Kode Akun</label>
                            <div class="col-sm-10">
                                <input type="text" maxlength="100" minlength="1" class="form-control" required id="kode_akun" name="kode akun" placeholder="kode_akun" value="<?php echo $item['kode_akun']; ?>">
                                <span class="help-block has-error"><?php echo form_error('kode_akun'); ?></span>
                            </div>
                        </div>
                        <div class="form-group <?php if(!empty(form_error('nama_akun')))echo "has-error"; ?>">
                            <label for="nama_akun" class="col-sm-2 control-label">Nama Akun</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" required id="nama_akun" name="nama akun" placeholder="nama akun" value="<?php echo $item['nama_akun']; ?>">
                                <span class="help-block has-error"><?php echo form_error('nama_akun'); ?></span>
                            </div>
                        </div>
                        <div class="form-group" style="margin-bottom:0">
                            <div class="col-sm-offset-2 col-sm-10 text-right">
                                <button type="reset" class="btn btn-default">Cancel</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
      </div>
      <footer class="cm-footer"><span class="pull-right">&copy;</span></footer>
    </div>
    </div>
