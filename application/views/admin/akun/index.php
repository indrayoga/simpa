    <div id="global">
      <div class="container-fluid cm-container-white">
        <form class="form-horizontal" method="" action="">
            <div class="form-group <?php if(!empty(form_error('tahun_anggaran')))echo "has-error"; ?>">
                <label for="tahun_anggaran" class="col-sm-2 control-label">Tahun Anggaran</label>
                <div class="col-sm-10">
                    <input type="text" readonly class="form-control" id="tahun_anggaran" name="tahun_anggaran" placeholder="tahun anggaran" value="<?php echo $this->session->tahun_anggaran; ?>">
                    <span class="help-block has-error"><?php echo form_error('tahun_anggaran'); ?></span>
                </div>
            </div>
            <div class="form-group">
                <label for="program" class="col-sm-2 control-label">Program</label>
                <div class="col-sm-10">
                    <input type="text" disabled class="form-control" value="PROGRAM PENINGKATAN PELAYANAN DAN PENEGEKAN HUKUM KEIMIGRASIAN">
                </div>
            </div>
            <div class="form-group">
                <label for="program" class="col-sm-2 control-label">Satker</label>
                <div class="col-sm-10">
                    <input type="text" disabled class="form-control" value="KANTOR IMIGRASI KELAS I KHUSUS JAKARTA BARAT">
                </div>
            </div>
        </form>
        <hr></hr>
        <div class="bs-example">
          <a href="<?php echo base_url('admin/akun/add'); ?>" class="btn btn-info"><i class="fa-plus"></i> Tambah</a>
        </div>
        <hr></hr>
        <table class="table table-bordered table-hover table-striped md-data-table " id="tableKecamatan">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Kode</th>
                    <th>Akun</th>
                    <th>Pilihan</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $no=1;
                foreach ($items as $item) {
                  ?>
                  <tr class="">
                      <td style="text-align:center"><?=$no?></td>
                      <td><?=strtoupper($item['kode_akun'])?></td>
                      <td><?=$item['nama_akun']?></td>
                      <td>
                        <div class="dropdown">
                        <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Pilihan
                        <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                        <li><a href="<?=base_url("admin/akun/modify/{$item['kode_akun']}")?>">Edit</a></li>
                        <li><a href="#" onclick="app_confirm('<?=base_url('admin/akun/delete/'.$item['kode_akun'])?>','Apakah Yakin Ingin Menghapus Data Ini?')" >Hapus</a></li>

                        </ul>
                        </div>
                      </td>
                  </tr>
                  <?php
                  $no++;
                }
                ?>
            </tbody>
        </table>
      </div>
      <footer class="cm-footer"><span class="pull-right">&copy;</span></footer>
    </div>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/jquery.dataTables.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/dataTables.material.min.css') ?>">
    <script src="<?php echo base_url('assets/js/jquery.dataTables.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/dataTables.material.min.js'); ?>"></script>
