    <div id="global">
      <div class="container-fluid">
        <div class="row cm-fix-height">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">Tambah Tahun anggaran</div>
                <div class="panel-body">
                    <form class="form-horizontal" method="post" action="<?=base_url('admin/anggaran/create') ?>">
                        <input type="hidden" name="<?=$csrf['name'] ?>" value="<?=$csrf['hash'] ?>">
                        <div class="form-group <?php if(!empty(form_error('tahun')))echo "has-error"; ?>">
                            <label for="tahun" class="col-sm-2 control-label">Tahun Anggaran</label>
                            <div class="col-sm-10">
                                <input type="text" maxlength="4" class="form-control" required id="tahun" name="tahun" placeholder="tahun" value="<?php echo set_value('tahun'); ?>">
                                <span class="help-block has-error"><?php echo form_error('tahun'); ?></span>
                            </div>
                        </div>
                        <div class="form-group <?php if(!empty(form_error('tahun')))echo "has-error"; ?>">
                            <label for="copy" class="col-sm-2 control-label">Copy DIPA dari tahun sebelumnya(kosongkan jika tidak ingin di copy)</label>
                            <div class="col-sm-10">
                                <input type="text" maxlength="4" class="form-control" id="copy" name="copy" placeholder="tahun" value="<?php echo set_value('copy'); ?>">
                                <span class="help-block has-error"><?php echo form_error('copy'); ?></span>
                            </div>
                        </div>
                        <div class="form-group <?php if(!empty(form_error('tahun')))echo "has-error"; ?>">
                            <label for="copy" class="col-sm-2 control-label">status</label>
                            <div class="col-sm-10">
                                <input type="checkbox" id="aktif" name="aktif" value="1"> Aktif
                                <span class="help-block has-error"></span>
                            </div>
                        </div>
                        <div class="form-group" style="margin-bottom:0">
                            <div class="col-sm-offset-2 col-sm-10 text-right">
                                <button type="reset" class="btn btn-default">Cancel</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
      </div>
      <footer class="cm-footer"><span class="pull-right">&copy;</span></footer>
    </div>
    </div>
