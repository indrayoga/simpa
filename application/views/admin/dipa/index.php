    <div id="global">
      <div class="container-fluid cm-container-white">
        <form class="form-horizontal" method="" action="">
            <div class="form-group <?php if(!empty(form_error('tahun_anggaran')))echo "has-error"; ?>">
                <label for="tahun_anggaran" class="col-sm-2 control-label">Tahun Anggaran</label>
                <div class="col-sm-10">
                    <input type="text" readonly class="form-control" id="tahun_anggaran" name="tahun_anggaran" placeholder="tahun anggaran" value="<?php echo $this->session->tahun_anggaran; ?>">
                    <span class="help-block has-error"><?php echo form_error('tahun_anggaran'); ?></span>
                </div>
            </div>
            <div class="form-group">
                <label for="program" class="col-sm-2 control-label">Program</label>
                <div class="col-sm-10">
                    <input type="text" disabled class="form-control" value="PROGRAM PENINGKATAN PELAYANAN DAN PENEGEKAN HUKUM KEIMIGRASIAN">
                </div>
            </div>
            <div class="form-group">
                <label for="program" class="col-sm-2 control-label">Satker</label>
                <div class="col-sm-10">
                    <input type="text" disabled class="form-control" value="KANTOR IMIGRASI KELAS I KHUSUS JAKARTA BARAT">
                </div>
            </div>
        </form>
        <hr></hr>
        <table class="table table-bordered table-hover table-striped md-data-table " id="tableKecamatan">
          <thead>
            <tr>
              <th>Kode</th>
              <th>Bidang</th>
              <th>Kegiatan</th>
              <th>Pilihan</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $level=1;
            $parentId="";
            foreach ($items as $item) {
              # code...
              ?>
              <tr data-tt-id="<?=$item['id_dipa']?>" data-tt-parent-id="<?=$item['parentid']?>">
                <td><?=$item['kode_kegiatan']?></td>
                <td><?=$item['kode_bidang']?></td>
                <td><?=$item['kegiatan']?></td>
                <td><a href="<?=base_url('admin/dipa/add/'.$item['id_dipa'])?>">Tambah</a> || <a href="<?=base_url('admin/dipa/append/'.$item['id_dipa'])?>">Sisipkan</a> || <a href="<?=base_url('admin/dipa/modify/'.$item['id_dipa'])?>">Edit</a> || <a href="#" onclick="app_confirm('<?=base_url('admin/dipa/delete/'.$item['id_dipa'])?>','Apakah Yakin Ingin Menghapus Data Ini?')">Hapus</a></td>
              </tr>
              <?php
            }
            ?>
          </tbody>
        </table>        
      </div>
      <footer class="cm-footer"><span class="pull-right">&copy;</span></footer>
    </div>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/jquery-treetable/jquery.treetable.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/jquery-treetable/jquery.treetable.theme.default.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/jquery-treetable/screen.css') ?>">
    <script src="<?php echo base_url('assets/js/jquery.dataTables.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/dataTables.material.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.treetable.js'); ?>"></script>
    <script type="text/javascript">
        $('#tableKecamatan').treetable({ expandable: true });
        $('#tableKecamatan').treetable('expandAll');
    </script>
