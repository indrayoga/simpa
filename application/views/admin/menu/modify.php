    <div id="global">
      <div class="container-fluid">
        <div class="row cm-fix-height">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">Ubah Pengguna</div>
                <div class="panel-body">
                    <form class="form-horizontal" method="post" action="<?=base_url('index.php/admin/menu/update') ?>">
                        <input type="hidden" name="<?=$csrf['name'] ?>" value="<?=$csrf['hash'] ?>">
                        <input type="hidden" name="id" value="<?=$id; ?>">
                        <div class="form-group <?php if(!empty(form_error('idparent')))echo "has-error"; ?>">
                            <label for="idparent" class="col-sm-2 control-label">Parent</label>
                            <div class="col-sm-10">
                                <select name="idparent">
                                    <option value="">Pilih Parent</option>
                                <?php
                                    foreach ($dataparent as $parent) {
                                        # code...
                                        $sel="";
                                        if(!empty(set_value('idmenu')) && set_value('idmenu')==$parent['IDMENU']){
                                            $sel="selected=selected";
                                        }else{
                                            if($item['IDMENU']==$parent['IDMENU']){
                                                $sel="selected=selected";
                                            }
                                        }
                                    ?>
                                    <option value="<?=$parent['IDMENU']?>" <?=$sel?> ><?=$parent['TITLE']?></option>
                                    <?php
                                    }
                                ?>
                                </select>
                                <span class="help-block has-error"><?php echo form_error('idparent'); ?></span>
                            </div>
                        </div>
                        <div class="form-group <?php if(!empty(form_error('title')))echo "has-error"; ?>">
                            <label for="title" class="col-sm-2 control-label">TITLE</label>
                            <div class="col-sm-10">
                                <input type="text" maxlength="100" minlength="1" class="form-control" required id="title" name="title" placeholder="title" value="<?php echo (set_value('title')!="")?set_value('title'):$item['TITLE']; ?>">
                                <span class="help-block has-error"><?php echo form_error('title'); ?></span>
                            </div>
                        </div>
                        <div class="form-group <?php if(!empty(form_error('url')))echo "has-error"; ?>">
                            <label for="url" class="col-sm-2 control-label">URL</label>
                            <div class="col-sm-10">
                                <input type="text" maxlength="150" minlength="1" class="form-control" required id="url" name="url" placeholder="url" value="<?php echo (set_value('url')!="")?set_value('url'):$item['URL']; ?>">
                                <span class="help-block has-error"><?php echo form_error('url'); ?></span>
                            </div>
                        </div>
                        <div class="form-group <?php if(!empty(form_error('menuicon')))echo "has-error"; ?>">
                            <label for="menuicon" class="col-sm-2 control-label">MENU ICON</label>
                            <div class="col-sm-10">
                                <input type="text" maxlength="150" minlength="1" class="form-control" id="menuicon" name="menuicon" placeholder="menuicon" value="<?php echo (set_value('menuicon')!="")?set_value('menuicon'):$item['MENUICON']; ?>">
                                <span class="help-block has-error"><?php echo form_error('menuicon'); ?></span>
                            </div>
                        </div>
                        <div class="form-group <?php if(!empty(form_error('menuorder')))echo "has-error"; ?>">
                            <label for="menuorder" class="col-sm-2 control-label">MENU ORDER</label>
                            <div class="col-sm-10">
                                <input type="text" maxlength="150" minlength="1" class="form-control" id="menuorder" name="menuorder" placeholder="menuorder" value="<?php echo (set_value('menuorder')!="")?set_value('menuorder'):$item['MENUORDER']; ?>">
                                <span class="help-block has-error"><?php echo form_error('menuorder'); ?></span>
                            </div>
                        </div>
                        <div class="form-group" style="margin-bottom:0">
                            <div class="col-sm-offset-2 col-sm-10 text-right">
                                <button type="reset" class="btn btn-default">Cancel</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
      </div>
      <footer class="cm-footer"><span class="pull-right">&copy;</span></footer>
    </div>
    </div>
