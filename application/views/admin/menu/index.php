    <div id="global">
      <div class="container-fluid cm-container-white">
        <div class="bs-example">
          <a href="<?php echo base_url('admin/menu/add'); ?>" class="btn btn-info"><i class="fa-plus"></i> Tambah</a>
        </div>
        <hr></hr>
        <table class="table table-bordered table-hover table-striped md-data-table " id="tableKecamatan">
            <thead>
                <tr>
                    <th>#</th>
                    <th>PARENT</th>
                    <th>TITLE</th>
                    <th>URL</th>
                    <th>MENUICON</th>
                    <th>MENU ORDER</th>
                    <th>PILIHAN</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $no=1;
                foreach ($items as $item) {
                  ?>
                  <tr class="">
                      <td style="text-align:center"><?=$no?></td>
                      <td><?=$item['parent']?></td>
                      <td><?=$item['TITLE']?></td>
                      <td><?=$item['URL']?></td>
                      <td><?=$item['MENUICON']?></td>
                      <td><?=$item['MENUORDER']?></td>
                      <td>
                        <div class="dropdown">
                        <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Pilihan
                        <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                        <li><a href="<?=base_url("admin/menu/modify/{$item['IDMENU']}")?>">Edit</a></li>
                        <li><a href="#" onclick="app_confirm('<?=base_url('admin/menu/delete/'.$item['IDMENU'])?>','Apakah Yakin Ingin Menghapus Data Ini?')" >Hapus</a></li>

                        </ul>
                        </div>
                      </td>
                  </tr>
                  <?php
                  $no++;
                }
                ?>
            </tbody>
        </table>
      </div>
      <footer class="cm-footer"><span class="pull-right">&copy;</span></footer>
    </div>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/jquery.dataTables.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/dataTables.material.min.css') ?>">
    <script src="<?php echo base_url('assets/js/jquery.dataTables.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/dataTables.material.min.js'); ?>"></script>
