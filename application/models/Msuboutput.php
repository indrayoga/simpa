<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Msuboutput extends CI_Model{
	private $table='dipa';

	function getAll($tahun=""){
		if(empty($tahun))$tahun=$this->session->tahun_anggaran;		
		$query=$this->db->query('SELECT anak.id_dipa,(select parent.id_dipa from dipa as parent where anak.kiri > parent.kiri and anak.kanan < parent.kanan order by parent.kiri desc limit 1) as parentid,anak.tahun_anggaran,anak.kode_kegiatan, COUNT( induk.id_dipa )
				level , CONCAT(  anak.kegiatan
				) AS kegiatan, anak.kiri, anak.kanan,anak.pagu
				FROM dipa AS induk, (

				SELECT *
				FROM dipa
				) AS anak
				WHERE anak.kiri
				BETWEEN induk.kiri
				AND induk.kanan
				and induk.kiri > 1 and induk.tahun_anggaran="'.$tahun.'"
				GROUP BY anak.id_dipa having level<=3
				ORDER BY `anak`.`kiri` ASC');

		return $query->result_array();
	}

	public function get($id){
		$query=$this->db->get_where($this->table,array('id_dipa'=>$id));
		return $query->row_array();
	}

	public function create(array $newOutput,$id_parent){
		$this->db->trans_start();
		$this->db->query('SELECT @varKiri := kiri FROM dipa WHERE id_dipa= "'.$id_parent.'"');
		$this->db->query('UPDATE dipa SET kanan = kanan + 2 WHERE kanan > @varKiri');
		$this->db->query('UPDATE dipa SET kiri = kiri + 2 WHERE kiri > @varKiri');
		$this->db->set($newOutput);
		$this->db->set('kiri', '@varKiri+1', FALSE);
		$this->db->set('kanan', '@varKiri+2', FALSE);
    	$this->db->insert($this->table);
		//$this->db->query('INSERT INTO dipa VALUES("NULL","'.$kode_item_lab.'","'.$satuan.'","'.$tipe.'",@varKiri + 1, @varKiri + 2)');
		$this->db->trans_complete();

    	return true;
	}

	public function append(array $newOutput,$id){
		$this->db->trans_start();
		$this->db->query('SELECT @varKanan := kanan FROM dipa WHERE id_dipa = "'.$id.'"');
		$this->db->query('UPDATE dipa SET kanan = kanan + 2 WHERE kanan > @varKanan');
		$this->db->query('UPDATE dipa SET kiri = kiri + 2 WHERE kiri > @varKanan');
		$this->db->set($newOutput);
		$this->db->set('kiri', '@varKanan + 1', FALSE);
		$this->db->set('kanan', '@varKanan + 2', FALSE);
    	$this->db->insert($this->table);
		$this->db->trans_complete();
    	return true;
	}

	public function update(array $newOutput,$kode_output){
    	$this->db->update($this->table,$newOutput,array('id_dipa'=>$kode_output));
    	return true;
	}

	public function delete($id){
		$this->db->trans_start();
		$this->db->query('SELECT @varKiri := kiri, @varKanan := kanan, @varSelisih := kanan - kiri + 1 FROM dipa WHERE id_dipa = "'.$id.'"');
		$this->db->query('DELETE FROM dipa WHERE kiri BETWEEN @varKiri AND @varKanan');
		$this->db->query('UPDATE dipa SET kanan = kanan - @varSelisih WHERE kanan > @varKanan');
		$this->db->query('UPDATE dipa SET kiri = kiri - @varSelisih WHERE kiri > @varKanan');
		$this->db->trans_complete();

		return true;
	}

}