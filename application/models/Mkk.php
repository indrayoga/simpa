<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mkk extends CI_Model{

	public function getAll($user="",$fields=['kegiatan'=>'','tanggal_mulai'=>'','minggu_mulai'=>'','minggu_berakhir'=>'','tahun_anggaran'=>''],$limit=10,$offset=0){
		$this->db->select('kalender_kegiatan.*,dipa.*,users.surname,kalender_kegiatan.id,date_format(tgl_mulai,"%d-%m-%Y") as tgl_mulai,date_format(tgl_berakhir,"%d-%m-%Y") as tgl_berakhir');
		$this->db->join('dipa','kalender_kegiatan.id_kegiatan=dipa.id_dipa','left');
		$this->db->join('users','kalender_kegiatan.id_user=users.id','left');
		//$this->db->join('pengajuan_um','kalender_kegiatan.id_kegiatan=dipa.id_dipa','left');
		//$this->db->join('ppspm','pengajuan_um.id=ppspm.id_pengajuan_um','left');
		//$this->db->join('tanda_terima','pengajuan_um.id=tanda_terima.id_pengajuan_um','left');
		if(!empty($user))$this->db->where('kalender_kegiatan.id_user',$user);
		if(!empty($fields['kegiatan']))$this->db->where('kalender_kegiatan.id_kegiatan',$fields['kegiatan']);
		if(!empty($fields['tanggal_mulai']))$this->db->where('kalender_kegiatan.tgl_mulai',$fields['tanggal_mulai']);
		if(!empty($fields['minggu_mulai']))$this->db->where('kalender_kegiatan.minggu_mulai',$fields['minggu_mulai']);
		if(!empty($fields['tahun_anggaran']))$this->db->where('kalender_kegiatan.tahun_anggaran',$fields['tahun_anggaran']);
		$this->db->limit($limit,$offset);
		$query=$this->db->get('kalender_kegiatan');
		return $query->result_array();
	}

	public function getCountRows($user="",$fields=['kegiatan'=>'','tanggal_mulai'=>'','minggu_mulai'=>'','minggu_berakhir'=>'','tahun_anggaran'=>'']){
		$this->db->join('dipa','kalender_kegiatan.id_kegiatan=dipa.id_dipa','left');
		$this->db->join('users','kalender_kegiatan.id_user=users.id','left');
		$this->db->join('pengajuan_um','kalender_kegiatan.id_kegiatan=dipa.id_dipa','left');
		$this->db->join('ppspm','pengajuan_um.id=ppspm.id_pengajuan_um','left');
		$this->db->join('tanda_terima','pengajuan_um.id=tanda_terima.id_pengajuan_um','left');
		if(!empty($user))$this->db->where('kalender_kegiatan.id_user',$user);
		if(!empty($fields['kegiatan']))$this->db->where('kalender_kegiatan.id_kegiatan',$fields['kegiatan']);
		if(!empty($fields['tanggal_mulai']))$this->db->where('kalender_kegiatan.tgl_mulai',$fields['tanggal_mulai']);
		if(!empty($fields['minggu_mulai']))$this->db->where('kalender_kegiatan.minggu_mulai',$fields['minggu_mulai']);
		if(!empty($fields['tahun_anggaran']))$this->db->where('kalender_kegiatan.tahun_anggaran',$fields['tahun_anggaran']);
		$query=$this->db->get('kalender_kegiatan');
		return $query->num_rows();
	}

	public function get($id){
		$this->db->select('kalender_kegiatan.*,date_format(tgl_mulai,"%d-%m-%Y") as tgl_mulai,date_format(tgl_berakhir,"%d-%m-%Y") as tgl_berakhir');
		$query=$this->db->get_where('kalender_kegiatan',array('id'=>$id));
		return $query->row_array();
	}

	public function getByIdKegiatan($id){
		$this->db->select('kalender_kegiatan.*,date_format(tgl_mulai,"%d-%m-%Y") as tgl_mulai,date_format(tgl_berakhir,"%d-%m-%Y") as tgl_berakhir');
		$this->db->join('pengajuan_um','kalender_kegiatan.id_kegiatan=dipa.id_dipa','left');
		$this->db->where('pengajuan_um.id!=','NULL');
		$query=$this->db->get_where('id_kegiatan',array('id'=>$id));
		return $query->result_array();
	}

	public function create(array $newKk){
    	$this->db->insert('kalender_kegiatan',$newKk);
    	return true;
	}

	public function update(array $newKk,$id){
    	$this->db->update('kalender_kegiatan',$newKk,array('id'=>$id));
    	return true;
	}

	public function delete($id)
	{
		$this->db->delete('kalender_kegiatan',array('id'=>$id));			
		return true;		
	}

}