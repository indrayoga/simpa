<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mbendahara extends CI_Model{

	public function getCatatanTerbaru($id){
		$this->db->order_by('tanggal','DESC');
		return $this->db->get_where('persetujuan_bendahara',array('id_pengajuan_um'=>$id))->row_array();
	}

	public function get($id){
		$query=$this->db->get_where('tanda_terima',array('tanda_terima.id'=>$id));
		return $query->row_array();
	}

	public function getKwitansi($id){
		$this->db->select('kwitansi.*,date_format(kwitansi.tanggal_surat,"%d-%m-%Y") as tanggal_surat,date_format(kwitansi.tanggal_disetujui,"%d-%m-%Y") as tanggal_disetujui,date_format(kwitansi.tanggal_diterima,"%d-%m-%Y") as tanggal_diterima,a.surname as kepada,a.nip as nip_kepada,b.surname as bendahara,b.nip as nip_bendahara,c.surname as penerima,c.nip as nip_penerima, d.surname as ppk,d.nip as nip_ppk');
		$this->db->join('users a','kwitansi.kepada=a.id');
		$this->db->join('users b','kwitansi.bendahara=b.id');
		$this->db->join('users c','kwitansi.penerima=c.id');
		$this->db->join('users d','kwitansi.ppk=d.id');
		$query=$this->db->get_where('kwitansi',array('kwitansi.id'=>$id));
		return $query->row_array();
	}

	public function create(array $newTandaTerima){
    	$this->db->insert('tanda_terima',$newTandaTerima);
    	return true;
	}

	public function createbendahara(array $newApproval){
    	$this->db->insert('persetujuan_bendahara',$newApproval);
    	return true;
	}

	public function update(array $newTandaTerima,$id){
    	$this->db->update('tanda_terima',$newTandaTerima,array('id'=>$id));
    	return true;
	}

	public function delete($id)
	{
		$this->db->delete('tanda_terima',array('id'=>$id));			
		return true;		
	}

}