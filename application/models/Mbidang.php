<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mbidang extends CI_Model{
	private $table='bidang';

	public function getAll(){
		$query=$this->db->get($this->table);
		return $query->result_array();
	}

	public function get($kode_bidang){
		$query=$this->db->get_where($this->table,array('kode_bidang'=>$kode_bidang));
		return $query->row_array();
	}

	public function create(array $newBidang){
    	$this->db->insert($this->table,$newBidang);
    	return true;
	}

	public function update(array $newBidang,$kode_bidang){
    	$this->db->update($this->table,$newBidang,array('kode_bidang'=>$kode_bidang));
    	return true;
	}

	public function delete($kode_bidang)
	{
		$this->db->delete($this->table,array('kode_bidang'=>$kode_bidang));			
		return true;		
	}


}