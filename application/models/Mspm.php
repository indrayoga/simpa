<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mspm extends CI_Model{

	public function getCatatanTerbaru($id){
		$this->db->order_by('tanggal','DESC');
		return $this->db->get_where('persetujuan_ppspm',array('id_pengajuan_um'=>$id))->row_array();
	}

	public function get($id){
		$query=$this->db->get_where('ppspm',array('ppspm.id'=>$id));
		return $query->row_array();
	}
	public function create(array $newSpm){
    	$this->db->insert('ppspm',$newSpm);
    	return true;
	}

	public function createspm(array $newApproval){
    	$this->db->insert('persetujuan_ppspm',$newApproval);
    	return true;
	}


	public function update(array $newSpm,$id){
    	$this->db->update('ppspm',$newSpm,array('id'=>$id));
    	return true;
	}

	public function delete($id)
	{
		$this->db->delete('ppspm',array('id'=>$id));			
		return true;		
	}

}