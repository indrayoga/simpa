<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mppk extends CI_Model{

	public function getCatatanTerbaru($id){
		$this->db->order_by('tanggal','DESC');
		return $this->db->get_where('persetujuan_ppk',array('id_pengajuan_um'=>$id))->row_array();
	}

	public function getAll($id_pengajuan_um,$order='tanggal',$sort='DESC'){
		$this->db->select('persetujuan_ppk.*,if(status=2,"Belum disetujui","Sudah disetujui") as status_pesan,date_format(tanggal,"%d-%m-%Y %h:%i:%s") as tanggal',false);
		if(!empty($id_pengajuan_um))$this->db->where('id_pengajuan_um',$id_pengajuan_um);
		$this->db->order_by($order,$sort);
		return $this->db->get('persetujuan_ppk')->result_array();
	}

	public function getAllPengeluaran(){
		$this->db->select('*,date_format(tanggal,"%d-%m-%Y") as tanggal');
		return $this->db->get('serapan_ppk')->result_array();
	}

	public function getPengeluaran($id){
		$this->db->select('*,date_format(tanggal,"%d-%m-%Y") as tanggal');
		return $this->db->get_where('serapan_ppk',array('id'=>$id))->row_array();
	}

	public function create(array $newApproval){
    	$this->db->insert('persetujuan_ppk',$newApproval);
    	return true;
	}

	public function createpengeluaran(array $newUM){
    	$this->db->insert('serapan_ppk',$newUM);
    	return $this->db->insert_id();
	}

	public function updatepengeluaran(array $newUM,$id){
    	$this->db->update('serapan_ppk',$newUM,array('id'=>$id));
    	return true;
    }

	public function createEvaluasi(array $newEvaluasi){
    	$this->db->insert('evaluasi_ppk',$newEvaluasi);
    	return true;
	}

	public function update(array $newApproval,$id){
    	$this->db->update('persetujuan_ppk',$newApproval,array('id'=>$id));
    	return true;
	}

	public function delete($id)
	{
		$this->db->delete('persetujuan_ppk',array('id'=>$id));			
		return true;		
	}

	public function deletepengeluaran($id)
	{
		$this->db->delete('serapan_ppk',array('id'=>$id));			
		return true;		
	}

}