<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mkpa extends CI_Model{

	public function getCatatanTerbaru($id){
		$this->db->order_by('tanggal','DESC');
		return $this->db->get_where('persetujuan_kpa',array('id_pengajuan_um'=>$id))->row_array();
	}

	public function getAll($id_pengajuan_um,$order='tanggal',$sort='DESC'){
		$this->db->select('persetujuan_kpa.*,if(status=2,"Belum disetujui","Sudah disetujui") as status_pesan,date_format(tanggal,"%d-%m-%Y %h:%i:%s") as tanggal',false);
		if(!empty($id_pengajuan_um))$this->db->where('id_pengajuan_um',$id_pengajuan_um);
		$this->db->order_by($order,$sort);
		return $this->db->get('persetujuan_kpa')->result_array();
	}

	public function create(array $newApproval){
    	$this->db->insert('persetujuan_kpa',$newApproval);
    	return true;
	}

	public function update(array $newApproval,$id){
    	$this->db->update('persetujuan_kpa',$newApproval,array('id'=>$id));
    	return true;
	}

	public function delete($id)
	{
		$this->db->delete('persetujuan_kpa',array('id'=>$id));			
		return true;		
	}

}