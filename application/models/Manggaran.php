<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manggaran extends CI_Model{
	private $table='tahun_anggaran';

	public function getAll(){
		$query=$this->db->get($this->table);
		return $query->result_array();
	}

	public function get($id){
		$query=$this->db->get_where($this->table,array('tahun'=>$id));
		return $query->row_array();
	}

	public function create(array $newAccess){
		if($newAccess['aktif']){
			$this->db->query('update tahun_anggaran set aktif=0');
		}
    	$this->db->insert($this->table,$newAccess);
    	return true;
	}

	public function copyDipa($tahunBaru,$tahunSebelum){
		$this->db->query('insert into dipa select NULL,"'.$tahunBaru.'",kode_bidang,kode_kegiatan,kegiatan,tipe,pagu,kiri,kanan from dipa where tahun_anggaran="'.$tahunSebelum.'"');
		return true;		
	}
	public function update(array $oldAccess,$id){
    	$this->db->update($this->table,$oldAccess,array('tahun'=>$id));
    	return true;
	}

	public function delete($access)
	{
		$this->db->delete($this->table,array('tahun'=>$id));			
		return true;		
	}

	public function activated($tahun)
	{
		$this->db->query('update tahun_anggaran set aktif=0');
		$this->db->query('update tahun_anggaran set aktif=1 where tahun="'.$tahun.'"');
		return true;		
	}


}