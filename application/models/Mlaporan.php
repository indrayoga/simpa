<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mlaporan extends CI_Model{

	public function getRealisasi($bulan="",$tahun="",$bidang=""){
		if(empty($tahun))$tahun=$this->session->tahun_anggaran;
		if(!empty($bulan))$bulan=" and month(waktu)='".$bulan."'"; else $bulan="";
		//$this->db->select('komponen_kegiatan.*,sub_output.sub_output,output.nama_output');
		//$this->db->join('sub_output','komponen_kegiatan.kode_sub_output=sub_output.kode_sub_output','left');
		//$this->db->join('output','komponen_kegiatan.kode_output=output.kode_output','left');
		//if(!empty($bidang))$this->db->where('output.kode_bidang',strtolower($bidang));
		//$query=$this->db->get('dipa');
		if(empty($bidang)){
		$query=$this->db->query('select dipa2.kiri,1 as urut,1 as kode_akun,kode_kegiatan as kode,kegiatan as keterangan,"" as vol,"" as satuan,"" as harga,(select sum(ifnull(jumlah,0)) from detil_pengajuan_um join pengajuan_um on detil_pengajuan_um.id_pengajuan_um=pengajuan_um.id join dipa on pengajuan_um.id_kegiatan=dipa.id_dipa where (dipa.kiri between dipa2.kiri and dipa2.kanan or (detil_pengajuan_um.kode_akun=dipa2.kode_kegiatan and pengajuan_um.id_kegiatan=(select parent.id_dipa from dipa as parent where dipa2.kiri > parent.kiri and dipa2.kanan < parent.kanan order by parent.kiri desc limit 1))) and year(pengajuan_um.waktu)="'.$tahun.'" ) as jumlah,(select sum(pagu) from dipa as parent where parent.kiri between dipa2.kiri and dipa2.kanan) as pagu from ( SELECT anak.id_dipa,anak.tahun_anggaran,anak.kode_kegiatan, COUNT( induk.id_dipa )
				level , CONCAT( REPEAT( "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", (
				COUNT( induk.id_dipa ) -1 ) ) , anak.kegiatan
				) AS kegiatan, anak.kiri, anak.kanan,anak.pagu
				FROM dipa AS induk, (

				SELECT *
				FROM dipa
				) AS anak
				WHERE anak.kiri
				BETWEEN induk.kiri
				AND induk.kanan
				and induk.kiri > 1
				and induk.tahun_anggaran="'.$tahun.'"
				GROUP BY anak.id_dipa
				ORDER BY `anak`.`kiri` ASC) dipa2 left join pengajuan_um on dipa2.id_dipa=pengajuan_um.id_kegiatan left join detil_pengajuan_um on pengajuan_um.id=detil_pengajuan_um.id_pengajuan_um 
				group by dipa2.id_dipa 
				UNION ALL
				SELECT dipa1.kiri,2 as urut,akun.kode_akun as kode,akun.kode_akun as kode_akun, concat("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;",detil_belanja) AS keterangan,vol,satuan,harga_satuan,jumlah, "" as pagu FROM detil_pengajuan_um join akun ON detil_pengajuan_um.kode_akun=akun.kode_akun join pengajuan_um on detil_pengajuan_um.id_pengajuan_um=pengajuan_um.id join dipa on pengajuan_um.id_kegiatan=dipa.id_dipa
					join dipa dipa1 on detil_pengajuan_um.kode_akun=dipa1.kode_kegiatan and dipa1.kiri between dipa.kiri and dipa.kanan
					join (select * from persetujuan_bendahara where status=1 order by tanggal desc limit 1) as persetujuan_bendahara on pengajuan_um.id=persetujuan_bendahara.id_pengajuan_um
								where year(pengajuan_um.waktu)="'.$tahun.'" 
				order by kiri asc,kode_akun,urut ');

		}else{
		$query=$this->db->query('select dipa2.kiri,1 as urut,1 as kode_akun,kode_kegiatan as kode,kegiatan as keterangan,"" as vol,"" as satuan,"" as harga,(select sum(ifnull(jumlah,0)) from detil_pengajuan_um join pengajuan_um on detil_pengajuan_um.id_pengajuan_um=pengajuan_um.id join dipa on pengajuan_um.id_kegiatan=dipa.id_dipa where (dipa.kiri between dipa2.kiri and dipa2.kanan or (detil_pengajuan_um.kode_akun=dipa2.kode_kegiatan and pengajuan_um.id_kegiatan<dipa2.id_dipa)) and year(pengajuan_um.waktu)="'.$tahun.'" ) as jumlah,(select sum(pagu) from dipa as parent where parent.kiri between dipa2.kiri and dipa2.kanan) as pagu from ( SELECT anak.id_dipa,anak.tahun_anggaran,anak.kode_kegiatan, COUNT( induk.id_dipa )
				level , CONCAT( REPEAT( "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", (
				COUNT( induk.id_dipa ) -1 ) ) , anak.kegiatan
				) AS kegiatan, anak.kiri, anak.kanan,anak.pagu
				FROM dipa AS induk, (

				SELECT *
				FROM dipa
				) AS anak
				WHERE anak.kiri
				BETWEEN induk.kiri
				AND induk.kanan
				and induk.kiri > 1
				and induk.kode_bidang="'.$bidang.'" and induk.tahun_anggaran="'.$tahun.'"
				GROUP BY anak.id_dipa
				ORDER BY `anak`.`kiri` ASC) dipa2 left join pengajuan_um on dipa2.id_dipa=pengajuan_um.id_kegiatan left join detil_pengajuan_um on pengajuan_um.id=detil_pengajuan_um.id_pengajuan_um 
				group by dipa2.id_dipa 
				UNION ALL
				SELECT dipa1.kiri,2 as urut,akun.kode_akun as kode,akun.kode_akun as kode_akun, concat("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;",detil_belanja) AS keterangan,vol,satuan,harga_satuan,jumlah, "" as pagu FROM detil_pengajuan_um join akun ON detil_pengajuan_um.kode_akun=akun.kode_akun join pengajuan_um on detil_pengajuan_um.id_pengajuan_um=pengajuan_um.id join dipa on pengajuan_um.id_kegiatan=dipa.id_dipa
					join dipa dipa1 on detil_pengajuan_um.kode_akun=dipa1.kode_kegiatan and dipa1.kiri between dipa.kiri and dipa.kanan

								where year(pengajuan_um.waktu)="'.$tahun.'" 
				order by kiri asc,kode_akun,urut ');

		}
		return $query->result_array();
	}

	public function getStatistik($bulan="",$tahun=""){
		if(empty($tahun))$tahun=$this->session->tahun_anggaran;
		if(!empty($bulan))$bulan=" and month(waktu)='".$bulan."'"; else $bulan="";
		$query=$this->db->query('select bulan.kode_bulan,bulan.nama_bulan,ifnull(forsakim.jumlah,0) as forsakim,ifnull(dazinim.jumlah,0) as dazinim,ifnull(lalintuskim.jumlah,0) as lalintuskim,ifnull(tu.jumlah,0) as tu,ifnull(wasdakim.jumlah,0) as wasdakim,(SELECT sum(pagu) as total FROM `dipa` where tahun_anggaran="'.$tahun.'") as total,
			ifnull((forsakim.jumlah/(SELECT sum(pagu) as total FROM `dipa` where tahun_anggaran="'.$tahun.'"))*100,0) as persenforsakim, 
			ifnull((dazinim.jumlah/(SELECT sum(pagu) as total FROM `dipa` where tahun_anggaran="'.$tahun.'"))*100,0) as persendazinim, 
			ifnull((lalintuskim.jumlah/(SELECT sum(pagu) as total FROM `dipa` where tahun_anggaran="'.$tahun.'"))*100,0) as persenlalintuskim, 
			ifnull((tu.jumlah/(SELECT sum(pagu) as total FROM `dipa` where tahun_anggaran="'.$tahun.'"))*100,0) as persentu, 
			ifnull((wasdakim.jumlah/(SELECT sum(pagu) as total FROM `dipa` where tahun_anggaran="'.$tahun.'"))*100,0) as persenwasdakim 
			from bulan left join
			(SELECT month(persetujuan_bendahara.tanggal) as bulan,dipa.kode_bidang,bidang.nama_bidang,sum(ifnull(jumlah,0)) as jumlah FROM dipa join pengajuan_um on dipa.id_dipa=pengajuan_um.id_kegiatan join detil_pengajuan_um on pengajuan_um.id=detil_pengajuan_um.id_pengajuan_um join bidang on dipa.kode_bidang=bidang.kode_bidang join (select * from persetujuan_bendahara where status=1 order by tanggal desc limit 1) as persetujuan_bendahara on pengajuan_um.id=persetujuan_bendahara.id_pengajuan_um
			where dipa.tahun_anggaran="'.$tahun.'" and bidang.kode_bidang="darinsuk" group by dipa.kode_bidang) as dazinim on bulan.kode_bulan=dazinim.bulan
			left join (SELECT month(persetujuan_bendahara.tanggal) as bulan,dipa.kode_bidang,bidang.nama_bidang,sum(ifnull(jumlah,0)) as jumlah FROM dipa join pengajuan_um on dipa.id_dipa=pengajuan_um.id_kegiatan join detil_pengajuan_um on pengajuan_um.id=detil_pengajuan_um.id_pengajuan_um join bidang on dipa.kode_bidang=bidang.kode_bidang join (select * from persetujuan_bendahara where status=1 order by tanggal desc limit 1) as persetujuan_bendahara on pengajuan_um.id=persetujuan_bendahara.id_pengajuan_um
			where dipa.tahun_anggaran="'.$tahun.'" and persetujuan_bendahara.status=1 and bidang.kode_bidang="forsakim" group by dipa.kode_bidang) as forsakim on bulan.kode_bulan=forsakim.bulan 
			left join (SELECT month(persetujuan_bendahara.tanggal) as bulan,dipa.kode_bidang,bidang.nama_bidang,sum(ifnull(jumlah,0)) as jumlah FROM dipa join pengajuan_um on dipa.id_dipa=pengajuan_um.id_kegiatan join detil_pengajuan_um on pengajuan_um.id=detil_pengajuan_um.id_pengajuan_um join bidang on dipa.kode_bidang=bidang.kode_bidang join (select * from persetujuan_bendahara where status=1 order by tanggal desc limit 1) as persetujuan_bendahara on pengajuan_um.id=persetujuan_bendahara.id_pengajuan_um
			where dipa.tahun_anggaran="'.$tahun.'" and bidang.kode_bidang="lalintuskim" group by dipa.kode_bidang) as lalintuskim on bulan.kode_bulan=lalintuskim.bulan 
			left join (SELECT month(persetujuan_bendahara.tanggal) as bulan,dipa.kode_bidang,bidang.nama_bidang,sum(ifnull(jumlah,0)) as jumlah FROM dipa join pengajuan_um on dipa.id_dipa=pengajuan_um.id_kegiatan join detil_pengajuan_um on pengajuan_um.id=detil_pengajuan_um.id_pengajuan_um join bidang on dipa.kode_bidang=bidang.kode_bidang join (select * from persetujuan_bendahara where status=1 order by tanggal desc limit 1) as persetujuan_bendahara on pengajuan_um.id=persetujuan_bendahara.id_pengajuan_um
			where dipa.tahun_anggaran="'.$tahun.'" and bidang.kode_bidang="TU" group by dipa.kode_bidang) as tu on bulan.kode_bulan=tu.bulan 
			left join (SELECT month(persetujuan_bendahara.tanggal) as bulan,dipa.kode_bidang,bidang.nama_bidang,sum(ifnull(jumlah,0)) as jumlah FROM dipa join pengajuan_um on dipa.id_dipa=pengajuan_um.id_kegiatan join detil_pengajuan_um on pengajuan_um.id=detil_pengajuan_um.id_pengajuan_um join bidang on dipa.kode_bidang=bidang.kode_bidang join (select * from persetujuan_bendahara where status=1 order by tanggal desc limit 1) as persetujuan_bendahara on pengajuan_um.id=persetujuan_bendahara.id_pengajuan_um
			where dipa.tahun_anggaran="'.$tahun.'" and bidang.kode_bidang="wasdakim" group by dipa.kode_bidang) as wasdakim on bulan.kode_bulan=wasdakim.bulan order by kode_bulan
			');

		/*$query=$this->db->query('SELECT dipa.kode_bidang,bidang.nama_bidang,sum(ifnull(jumlah,0)) as jumlah FROM dipa join pengajuan_um on dipa.id_dipa=pengajuan_um.id_kegiatan join detil_pengajuan_um on pengajuan_um.id=detil_pengajuan_um.id_pengajuan_um join bidang on dipa.kode_bidang=bidang.kode_bidang
			where year(pengajuan_um.waktu)="'.$tahun.'" '.$bulan.'
			group by dipa.kode_bidang ');*/
		return $query->result_array();
	}

	public function getStatistikPerBidang($bidang="",$tahun=""){
		if(empty($tahun))$tahun=$this->session->tahun_anggaran;
		if(!empty($bulan))$bulan=" and month(waktu)='".$bulan."'"; else $bulan="";
		$bidang=strtolower($bidang);
		if($bidang=='forsakim'){
			$query=$this->db->query('select bulan.kode_bulan,bulan.nama_bulan,ifnull(forsakim.jumlah,0) as jumlah,ifnull((forsakim.jumlah/(SELECT sum(pagu) as total FROM `dipa` where tahun_anggaran="'.$tahun.'"))*100,0) as persenjumlah
				from bulan
				left join (SELECT month(persetujuan_bendahara.tanggal) as bulan,dipa.kode_bidang,bidang.nama_bidang,sum(ifnull(jumlah,0)) as jumlah FROM dipa join pengajuan_um on dipa.id_dipa=pengajuan_um.id_kegiatan join detil_pengajuan_um on pengajuan_um.id=detil_pengajuan_um.id_pengajuan_um join bidang on dipa.kode_bidang=bidang.kode_bidang join (select * from persetujuan_bendahara where status=1 order by tanggal desc limit 1) as persetujuan_bendahara on pengajuan_um.id=persetujuan_bendahara.id_pengajuan_um
				where dipa.tahun_anggaran="'.$tahun.'" and persetujuan_bendahara.status=1 and bidang.kode_bidang="forsakim" group by dipa.kode_bidang) as forsakim on bulan.kode_bulan=forsakim.bulan 
				');
		}else if($bidang=='darinsuk'){
			$query=$this->db->query('select bulan.kode_bulan,bulan.nama_bulan,ifnull(dazinim.jumlah,0) as jumlah,ifnull((dazinim.jumlah/(SELECT sum(pagu) as total FROM `dipa` where tahun_anggaran="'.$tahun.'"))*100,0) as persenjumlah
				from bulan
				left join (SELECT month(persetujuan_bendahara.tanggal) as bulan,dipa.kode_bidang,bidang.nama_bidang,sum(ifnull(jumlah,0)) as jumlah FROM dipa join pengajuan_um on dipa.id_dipa=pengajuan_um.id_kegiatan join detil_pengajuan_um on pengajuan_um.id=detil_pengajuan_um.id_pengajuan_um join bidang on dipa.kode_bidang=bidang.kode_bidang join (select * from persetujuan_bendahara where status=1 order by tanggal desc limit 1) as persetujuan_bendahara on pengajuan_um.id=persetujuan_bendahara.id_pengajuan_um
				where dipa.tahun_anggaran="'.$tahun.'" and persetujuan_bendahara.status=1 and bidang.kode_bidang="darinsuk" group by dipa.kode_bidang) as dazinim on bulan.kode_bulan=dazinim.bulan 
				');
		}else if($bidang=='lalintuskim'){
			$query=$this->db->query('select bulan.kode_bulan,bulan.nama_bulan,ifnull(lalintuskim.jumlah,0) as jumlah,ifnull((lalintuskim.jumlah/(SELECT sum(pagu) as total FROM `dipa` where tahun_anggaran="'.$tahun.'"))*100,0) as persenjumlah
				from bulan
				left join (SELECT month(persetujuan_bendahara.tanggal) as bulan,dipa.kode_bidang,bidang.nama_bidang,sum(ifnull(jumlah,0)) as jumlah FROM dipa join pengajuan_um on dipa.id_dipa=pengajuan_um.id_kegiatan join detil_pengajuan_um on pengajuan_um.id=detil_pengajuan_um.id_pengajuan_um join bidang on dipa.kode_bidang=bidang.kode_bidang join (select * from persetujuan_bendahara where status=1 order by tanggal desc limit 1) as persetujuan_bendahara on pengajuan_um.id=persetujuan_bendahara.id_pengajuan_um
				where dipa.tahun_anggaran="'.$tahun.'" and persetujuan_bendahara.status=1 and bidang.kode_bidang="lalintuskim" group by dipa.kode_bidang) as lalintuskim on bulan.kode_bulan=lalintuskim.bulan 
				');
		}else if($bidang=='wasdakim'){
			$query=$this->db->query('select bulan.kode_bulan,bulan.nama_bulan,ifnull(wasdakim.jumlah,0) as jumlah,ifnull((wasdakim.jumlah/(SELECT sum(pagu) as total FROM `dipa` where tahun_anggaran="'.$tahun.'"))*100,0) as persenjumlah
				from bulan
				left join (SELECT month(persetujuan_bendahara.tanggal) as bulan,dipa.kode_bidang,bidang.nama_bidang,sum(ifnull(jumlah,0)) as jumlah FROM dipa join pengajuan_um on dipa.id_dipa=pengajuan_um.id_kegiatan join detil_pengajuan_um on pengajuan_um.id=detil_pengajuan_um.id_pengajuan_um join bidang on dipa.kode_bidang=bidang.kode_bidang join (select * from persetujuan_bendahara where status=1 order by tanggal desc limit 1) as persetujuan_bendahara on pengajuan_um.id=persetujuan_bendahara.id_pengajuan_um
				where dipa.tahun_anggaran="'.$tahun.'" and persetujuan_bendahara.status=1 and bidang.kode_bidang="wasdakim" group by dipa.kode_bidang) as wasdakim on bulan.kode_bulan=wasdakim.bulan 
				');
		}else if($bidang=='tu'){
			$query=$this->db->query('select bulan.kode_bulan,bulan.nama_bulan,ifnull(tu.jumlah,0) as jumlah,ifnull((tu.jumlah/(SELECT sum(pagu) as total FROM `dipa` where tahun_anggaran="'.$tahun.'"))*100,0) as persenjumlah
				from bulan
				left join (SELECT month(persetujuan_bendahara.tanggal) as bulan,dipa.kode_bidang,bidang.nama_bidang,sum(ifnull(jumlah,0)) as jumlah FROM dipa join pengajuan_um on dipa.id_dipa=pengajuan_um.id_kegiatan join detil_pengajuan_um on pengajuan_um.id=detil_pengajuan_um.id_pengajuan_um join bidang on dipa.kode_bidang=bidang.kode_bidang join (select * from persetujuan_bendahara where status=1 order by tanggal desc limit 1) as persetujuan_bendahara on pengajuan_um.id=persetujuan_bendahara.id_pengajuan_um
				where dipa.tahun_anggaran="'.$tahun.'" and persetujuan_bendahara.status=1 and bidang.kode_bidang="tu" group by dipa.kode_bidang) as tu on bulan.kode_bulan=tu.bulan 
				');
		}else{
			$query=$this->db->query('select bulan.kode_bulan,bulan.nama_bulan,0 as jumlah,0 as persenjumlah
				from bulan				');
		}

		/*$query=$this->db->query('SELECT dipa.kode_bidang,bidang.nama_bidang,sum(ifnull(jumlah,0)) as jumlah FROM dipa join pengajuan_um on dipa.id_dipa=pengajuan_um.id_kegiatan join detil_pengajuan_um on pengajuan_um.id=detil_pengajuan_um.id_pengajuan_um join bidang on dipa.kode_bidang=bidang.kode_bidang
			where year(pengajuan_um.waktu)="'.$tahun.'" '.$bulan.'
			group by dipa.kode_bidang ');*/
		return $query->result_array();
	}

	public function getPersenRealisasi($bidang,$tahun=""){
		if(empty($tahun))$tahun=$this->session->tahun_anggaran;
		if(empty($bidang)){		
			$query=$this->db->query('select month(tanggal_pengajuan) as bulan,sum(detil_pengajuan_um.jumlah) as jumlah,(SELECT sum(pagu) as total FROM `dipa` where tahun_anggaran="'.$tahun.'") as total,(jumlah/(SELECT sum(pagu) as total FROM `dipa` where tahun_anggaran="'.$tahun.'"))*100 as persen from pengajuan_um join detil_pengajuan_um on pengajuan_um.id=detil_pengajuan_um.id_pengajuan_um where year(tanggal_pengajuan)="'.$tahun.'" ');
		}else{
			$query=$this->db->query('select month(tanggal_pengajuan) as bulan,sum(detil_pengajuan_um.jumlah) as jumlah,(SELECT sum(pagu) as total FROM `dipa` where tahun_anggaran="'.$tahun.'" and kode_bidang="'.$bidang.'") as total,(jumlah/(SELECT sum(pagu) as total FROM `dipa` where tahun_anggaran="'.$tahun.'" and kode_bidang="'.$bidang.'"))*100 as persen from pengajuan_um join dipa on pengajuan_um.id_kegiatan=dipa.id_dipa join detil_pengajuan_um on pengajuan_um.id=detil_pengajuan_um.id_pengajuan_um where year(tanggal_pengajuan)="'.$tahun.'" and dipa.kode_bidang="'.$bidang.'" ');			
		}
		return $query->result_array();

	}

	public function getStatistikBidang($bidang,$bulan,$tahun=""){
		if(empty($tahun))$tahun=$this->session->tahun_anggaran;
		if(!empty($bulan))$bulan=" and month(waktu)='".$bulan."'"; else $bulan="";
		if(empty($bidang)){
		$query=$this->db->query('SELECT dipa.kode_bidang,bidang.nama_bidang,sum(jumlah) as jumlah FROM dipa join pengajuan_um on dipa.id_dipa=pengajuan_um.id_kegiatan join detil_pengajuan_um on pengajuan_um.id=detil_pengajuan_um.id_pengajuan_um join bidang on dipa.kode_bidang=bidang.kode_bidang
			where year(pengajuan_um.waktu)="'.$tahun.'" '.$bulan.'
			group by dipa.kode_bidang ');

		}else{
		$query=$this->db->query('SELECT dipa.kode_bidang,bidang.nama_bidang,sum(jumlah) as jumlah FROM dipa join pengajuan_um on dipa.id_dipa=pengajuan_um.id_kegiatan join detil_pengajuan_um on pengajuan_um.id=detil_pengajuan_um.id_pengajuan_um join bidang on dipa.kode_bidang=bidang.kode_bidang
			where year(pengajuan_um.waktu)="'.$tahun.'" '.$bulan.' and dipa.kode_bidang="'.$bidang.'"
			group by dipa.kode_bidang ');

		}
		return $query->row_array();
	}

	public function getKegiatan($ket,$bidang,$tahun=""){
		if(empty($tahun))$tahun=$this->session->tahun_anggaran;
		if(!empty($bidang))$bid=" and dipa.kode_bidang='".$bidang."'"; else $bid="";
		if($ket==1){
		$query=$this->db->query('SELECT count(1) as jumlah FROM dipa join pengajuan_um on dipa.id_dipa=pengajuan_um.id_kegiatan join bidang on dipa.kode_bidang=bidang.kode_bidang
			where dipa.tahun_anggaran="'.$tahun.'" '.$bid.' ');			
		}
		if($ket==2){
		$query=$this->db->query('SELECT count(1) as jumlah FROM dipa join pengajuan_um on dipa.id_dipa=pengajuan_um.id_kegiatan join bidang on dipa.kode_bidang=bidang.kode_bidang
			where dipa.tahun_anggaran="'.$tahun.'" '.$bid.' and tag_kpa=1');			
		}
		if($ket==3){
		$query=$this->db->query('SELECT count(1) as jumlah FROM dipa join pengajuan_um on dipa.id_dipa=pengajuan_um.id_kegiatan join bidang on dipa.kode_bidang=bidang.kode_bidang
			where dipa.tahun_anggaran="'.$tahun.'" '.$bid.' and tag_ppk=1');			
		}
		if($ket==4){
		$query=$this->db->query('SELECT count(1) as jumlah FROM dipa join pengajuan_um on dipa.id_dipa=pengajuan_um.id_kegiatan join bidang on dipa.kode_bidang=bidang.kode_bidang
			where dipa.tahun_anggaran="'.$tahun.'" '.$bid.' and tag_evaluasi=2');			
		}
		$item=$query->row_array();
		if(empty($item))return 0;
		return $item['jumlah'];
	}

	public function getDetilBelanja($id_pengajuan_um,$bulan="",$tahun="",$bidang=""){
		$this->db->query("SELECT 1 as urut,akun.kode_akun,akun.nama_akun as keterangan,'' as vol,'' as satuan,'' as harga_satuan,sum(jumlah) FROM akun join detil_pengajuan_um on akun.kode_akun=detil_pengajuan_um.kode_akun
			where detil_pengajuan_um.id_pengajuan_um='".$id_pengajuan_um."'
						UNION ALL
						SELECT 2 as urut,detil_pengajuan_um.kode_akun, detil_belanja AS keterangan,vol,satuan,harga_satuan,jumlah FROM detil_pengajuan_um join akun ON detil_pengajuan_um.kode_akun=akun.kode_akun
						");
	}

	public function getSerapanPPK($bidang=""){
		if(empty($bidang)){
			$items=$this->db->query('select sum(jumlah) as jumlah from serapan_ppk')->row_array();
		}else{
			$items=$this->db->query('select sum(jumlah) as jumlah from serapan_ppk where kode_bidang="'.$this->user['seksi'].'"')->row_array();
		}
		if(empty($items))return $items['jumlah'];
		return $items;
	}

	public function getProgressByBidang($bidang="",$tahun=""){
		if(empty($tahun))$tahun=$this->session->tahun_anggaran;
		//$this->db->select('komponen_kegiatan.*,sub_output.sub_output,output.nama_output');
		//$this->db->join('sub_output','komponen_kegiatan.kode_sub_output=sub_output.kode_sub_output','left');
		//$this->db->join('output','komponen_kegiatan.kode_output=output.kode_output','left');
		//if(!empty($bidang))$this->db->where('output.kode_bidang',strtolower($bidang));
		//$query=$this->db->get('dipa');
		if(empty($bidang)){
		$query=$this->db->query('select * from (SELECT anak.id_dipa,anak.tahun_anggaran,anak.kode_kegiatan, COUNT( induk.id_dipa )
				level , CONCAT( REPEAT( "&nbsp;", (
				COUNT( induk.id_dipa ) -1 ) ),anak.kode_kegiatan,"	-" , anak.kegiatan
				) AS kegiatan, anak.kiri, anak.kanan,anak.pagu
				FROM dipa AS induk, (

				SELECT *
				FROM dipa
				) AS anak
				WHERE anak.kiri
				BETWEEN induk.kiri
				AND induk.kanan
				and induk.kiri > 1 and anak.tahun_anggaran="'.$tahun.'" and anak.is_akun=0
				GROUP BY anak.id_dipa
				ORDER BY `anak`.`kiri` ASC) dipa left join pengajuan_um on dipa.id_dipa=pengajuan_um.id_kegiatan where dipa.tahun_anggaran="'.$tahun.'" order by dipa.kiri asc');

		}else{
		$query=$this->db->query('select * from (SELECT anak.id_dipa,anak.tahun_anggaran,anak.kode_kegiatan, COUNT( induk.id_dipa )
				level , CONCAT( REPEAT( "&nbsp;", (
				COUNT( induk.id_dipa ) -1 ) ),anak.kode_kegiatan,"	-" , anak.kegiatan
				) AS kegiatan, anak.kiri, anak.kanan,anak.pagu
				FROM dipa AS induk, (

				SELECT *
				FROM dipa
				) AS anak
				WHERE anak.kiri
				BETWEEN induk.kiri
				AND induk.kanan
				and induk.kiri > 1 and anak.tahun_anggaran="'.$tahun.'" and anak.is_akun=0
				and induk.kode_bidang="'.$bidang.'"
				GROUP BY anak.id_dipa
				ORDER BY `anak`.`kiri` ASC) dipa left join pengajuan_um on dipa.id_dipa=pengajuan_um.id_kegiatan where dipa.tahun_anggaran="'.$tahun.'" order by dipa.kiri asc');

		}
		return $query->result_array();
	}

	public function eventkk()
	{
		$items=$this->mkk->getAll($this->user['id']);
		$data=array();
		foreach ($items as $item) {
			# code...
			$hitungminggu=(($item['bulan_berakhir']-1)*4)+$item['minggu_terakhir'];
			$date = new DateTime();
			$date->setISODate($item['tahun_anggaran'], $hitungminggu, 7);
			$data[]=array(
				'title'=>$item['kegiatan'],
				'start'=>$item['tgl_mulai'],
				'end'=>$date->format('Y-m-d')
				);
		}
		echo json_encode($data);
	}

}