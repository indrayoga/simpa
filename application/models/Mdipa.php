<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mdipa extends CI_Model{

	function getAll($tahun=""){
		if(empty($tahun))$tahun=$this->session->tahun_anggaran;
		$query=$this->db->query('SELECT anak.id_dipa,(select parent.id_dipa from dipa as parent where anak.kiri > parent.kiri and anak.kanan < parent.kanan order by parent.kiri desc limit 1) as parentid,anak.kode_bidang,anak.tahun_anggaran,anak.kode_kegiatan, COUNT( induk.id_dipa )
				level , CONCAT( REPEAT( "&nbsp;", (
				COUNT( induk.id_dipa ) -1 ) ),anak.kode_kegiatan,"	-" , anak.kegiatan
				) AS kegiatan, anak.kiri, anak.kanan,anak.pagu
				FROM dipa AS induk, (

				SELECT *
				FROM dipa
				) AS anak
				WHERE anak.kiri
				BETWEEN induk.kiri
				AND induk.kanan
				and induk.kiri > 1 and induk.tahun_anggaran="'.$tahun.'" and anak.is_akun=0
				GROUP BY anak.id_dipa
				ORDER BY `anak`.`kiri` ASC');

		return $query->result_array();
	}

	function getAllOutput($bidang="",$tahun=""){
		if(empty($tahun))$tahun=$this->session->tahun_anggaran;
		if(empty($bidang)){
		$query=$this->db->query('SELECT anak.id_dipa,anak.tahun_anggaran,anak.kode_kegiatan, COUNT( induk.id_dipa )
				level , CONCAT( REPEAT( "&nbsp;", (
				COUNT( induk.id_dipa ) -1 ) ),anak.kode_kegiatan,"	-" , anak.kegiatan
				) AS kegiatan, anak.kiri, anak.kanan,anak.pagu
				FROM dipa AS induk, (

				SELECT *
				FROM dipa
				) AS anak
				WHERE anak.kiri
				BETWEEN induk.kiri
				AND induk.kanan
				and induk.kiri > 1 and induk.tahun_anggaran="'.$tahun.'"
				GROUP BY anak.id_dipa having level=1
				ORDER BY `anak`.`kiri` ASC');

		}else{
		$query=$this->db->query('SELECT anak.id_dipa,anak.tahun_anggaran,anak.kode_kegiatan, COUNT( induk.id_dipa )
				level , CONCAT( REPEAT( "&nbsp;", (
				COUNT( induk.id_dipa ) -1 ) ),anak.kode_kegiatan,"	-" , anak.kegiatan
				) AS kegiatan, anak.kiri, anak.kanan,anak.pagu
				FROM dipa AS induk, (

				SELECT *
				FROM dipa
				) AS anak
				WHERE anak.kiri
				BETWEEN induk.kiri
				AND induk.kanan
				and induk.kiri > 1
				and induk.kode_bidang="'.$bidang.'" and induk.tahun_anggaran="'.$tahun.'"
				GROUP BY anak.id_dipa having level=1
				ORDER BY `anak`.`kiri` ASC');

		}

		return $query->result_array();
	}

	function getAllSubOutput($id_output="",$tahun=""){
		if(empty($tahun))$tahun=$this->session->tahun_anggaran;
		$output=$this->db->get_where('dipa',array('id_dipa'=>$id_output))->row_array();
		if(empty($output))return array();
		$query=$this->db->query('SELECT anak.id_dipa,anak.tahun_anggaran,anak.kode_kegiatan, COUNT( induk.id_dipa )
				level , CONCAT( REPEAT( "&nbsp;", (
				COUNT( induk.id_dipa ) -1 ) ),anak.kode_kegiatan,"	-" , anak.kegiatan
				) AS kegiatan, anak.kiri, anak.kanan,anak.pagu
				FROM dipa AS induk, (

				SELECT *
				FROM dipa
				) AS anak
				WHERE anak.kiri
				BETWEEN induk.kiri AND induk.kanan
				and anak.tahun_anggaran="'.$tahun.'" and anak.is_akun=0
				and induk.kiri > '.$output['kiri'].'
				AND induk.kanan < '.$output['kanan'].'
				GROUP BY anak.id_dipa having level=1
				ORDER BY `anak`.`kiri` ASC');

		return $query->result_array();
	}

	function getAllAkunKegiatan($id_dipa="",$tahun=""){
		if(empty($tahun))$tahun=$this->session->tahun_anggaran;
		$kegiatan=$this->db->get_where('dipa',array('id_dipa'=>$id_dipa))->row_array();
		$query=$this->db->query('SELECT anak.id_dipa,anak.tahun_anggaran,anak.kode_kegiatan, COUNT( induk.id_dipa )
				level , CONCAT( REPEAT( "&nbsp;", (
				COUNT( induk.id_dipa ) -1 ) ),anak.kode_kegiatan,"	-" , anak.kegiatan
				) AS kegiatan, anak.kiri, anak.kanan,anak.pagu
				FROM dipa AS induk, (

				SELECT *
				FROM dipa
				) AS anak
				WHERE anak.kiri
				BETWEEN induk.kiri AND induk.kanan
				and anak.tahun_anggaran="'.$tahun.'"
				and induk.kiri > '.$kegiatan['kiri'].'
				AND induk.kiri < '.$kegiatan['kanan'].' and anak.is_akun=1
				GROUP BY anak.id_dipa 
				ORDER BY `anak`.`kiri` ASC');

		return $query->result_array();
	}

	public function getByBidang($bidang="",$tahun=""){
		if(empty($tahun))$tahun=$this->session->tahun_anggaran;
		//$this->db->select('komponen_kegiatan.*,sub_output.sub_output,output.nama_output');
		//$this->db->join('sub_output','komponen_kegiatan.kode_sub_output=sub_output.kode_sub_output','left');
		//$this->db->join('output','komponen_kegiatan.kode_output=output.kode_output','left');
		//if(!empty($bidang))$this->db->where('output.kode_bidang',strtolower($bidang));
		//$query=$this->db->get('dipa');
		if(empty($bidang)){
		$query=$this->db->query('SELECT anak.id_dipa,anak.tahun_anggaran,anak.kode_kegiatan, COUNT( induk.id_dipa )
				level , CONCAT( REPEAT( "&nbsp;", (
				COUNT( induk.id_dipa ) -1 ) ),anak.kode_kegiatan,"	-" , anak.kegiatan
				) AS kegiatan, anak.kiri, anak.kanan,anak.pagu
				FROM dipa AS induk, (

				SELECT *
				FROM dipa
				) AS anak
				WHERE anak.kiri
				BETWEEN induk.kiri
				AND induk.kanan
				and induk.kiri > 1 and anak.tahun_anggaran="'.$tahun.'"
				GROUP BY anak.id_dipa
				ORDER BY `anak`.`kiri` ASC');

		}else{
		$query=$this->db->query('SELECT anak.id_dipa,anak.tahun_anggaran,anak.kode_kegiatan, COUNT( induk.id_dipa )
				level , CONCAT( REPEAT( "&nbsp;", (
				COUNT( induk.id_dipa ) -1 ) ),anak.kode_kegiatan,"	-" , anak.kegiatan
				) AS kegiatan, anak.kiri, anak.kanan,anak.pagu
				FROM dipa AS induk, (

				SELECT *
				FROM dipa
				) AS anak
				WHERE anak.kiri
				BETWEEN induk.kiri
				AND induk.kanan
				and induk.kiri > 1 and anak.tahun_anggaran="'.$tahun.'"
				and induk.kode_bidang="'.$bidang.'"
				GROUP BY anak.id_dipa
				ORDER BY `anak`.`kiri` ASC');

		}
		return $query->result_array();
	}

	public function getByBidangKalender($bidang="",$tahun=""){
		if(empty($tahun))$tahun=$this->session->tahun_anggaran;
		//$this->db->select('komponen_kegiatan.*,sub_output.sub_output,output.nama_output');
		//$this->db->join('sub_output','komponen_kegiatan.kode_sub_output=sub_output.kode_sub_output','left');
		//$this->db->join('output','komponen_kegiatan.kode_output=output.kode_output','left');
		//if(!empty($bidang))$this->db->where('output.kode_bidang',strtolower($bidang));
		//$query=$this->db->get('dipa');
		if(empty($bidang)){
		$query=$this->db->query('SELECT anak.id_dipa,anak.tahun_anggaran,anak.kode_kegiatan, COUNT( induk.id_dipa )
				level , CONCAT( REPEAT( "&nbsp;", (
				COUNT( induk.id_dipa ) -1 ) ),anak.kode_kegiatan,"	-" , anak.kegiatan
				) AS kegiatan, anak.kiri, anak.kanan,anak.pagu
				FROM dipa AS induk, (

				SELECT *
				FROM dipa
				) AS anak
				WHERE anak.kiri
				BETWEEN induk.kiri
				AND induk.kanan
				and induk.kiri > 1 and anak.tahun_anggaran="'.$tahun.'" and anak.is_akun=0
				GROUP BY anak.id_dipa
				ORDER BY `anak`.`kiri` ASC');

		}else{
		$query=$this->db->query('SELECT anak.id_dipa,anak.tahun_anggaran,anak.kode_kegiatan, COUNT( induk.id_dipa )
				level , CONCAT( REPEAT( "&nbsp;", (
				COUNT( induk.id_dipa ) -1 ) ),anak.kode_kegiatan,"	-" , anak.kegiatan
				) AS kegiatan, anak.kiri, anak.kanan,anak.pagu
				FROM dipa AS induk, (

				SELECT *
				FROM dipa
				) AS anak
				WHERE anak.kiri
				BETWEEN induk.kiri
				AND induk.kanan
				and induk.kiri > 1 and anak.tahun_anggaran="'.$tahun.'" and anak.is_akun=0
				and induk.kode_bidang="'.$bidang.'"
				GROUP BY anak.id_dipa
				ORDER BY `anak`.`kiri` ASC');

		}
		return $query->result_array();
	}

	public function getTreeKegiatan($id_kegiatan="",$tahun=""){
		if(empty($tahun))$tahun=$this->session->tahun_anggaran;
		//$this->db->select('komponen_kegiatan.*,sub_output.sub_output,output.nama_output');
		//$this->db->join('sub_output','komponen_kegiatan.kode_sub_output=sub_output.kode_sub_output','left');
		//$this->db->join('output','komponen_kegiatan.kode_output=output.kode_output','left');
		//if(!empty($bidang))$this->db->where('output.kode_bidang',strtolower($bidang));
		//$query=$this->db->get('dipa');
		$item=$this->db->get_where('dipa',array('id_dipa'=>$id_kegiatan))->row_array();

		$query=$this->db->query('SELECT anak.id_dipa,anak.tahun_anggaran,anak.kode_kegiatan, COUNT( induk.id_dipa )
				level , CONCAT( REPEAT( "&nbsp;", (
				COUNT( induk.id_dipa ) -1 ) ),anak.kode_kegiatan,"	-" , anak.kegiatan
				) AS kegiatan, anak.kiri, anak.kanan,anak.pagu
				FROM dipa AS induk, (

				SELECT *
				FROM dipa
				) AS anak
				WHERE anak.kiri <= "'.$item['kiri'].'" AND anak.kanan >= "'.$item['kanan'].'" and anak.tahun_anggaran="'.$tahun.'"
				GROUP BY anak.id_dipa
				ORDER BY `anak`.`kiri` ASC');

		return $query->result_array();
	}


	public function get($id){
		$query=$this->db->get_where('dipa',array('id_dipa'=>$id));
		return $query->row_array();
	}

	public function create(array $newOutput,$id){
		$this->db->trans_start();
		$this->db->query('SELECT @varKiri := kiri FROM dipa WHERE id_dipa = "'.$id.'"');
		$this->db->query('UPDATE dipa SET kanan = kanan + 2 WHERE kanan > @varKiri');
		$this->db->query('UPDATE dipa SET kiri = kiri + 2 WHERE kiri > @varKiri');
		$this->db->set($newOutput);
		$this->db->set('kiri', '@varKiri+1', FALSE);
		$this->db->set('kanan', '@varKiri+2', FALSE);
    	$this->db->insert('dipa');
		//$this->db->query('INSERT INTO dipa VALUES("NULL","'.$kode_item_lab.'","'.$satuan.'","'.$tipe.'",@varKiri + 1, @varKiri + 2)');
		$this->db->trans_complete();

    	return true;
	}

	public function update(array $newOutput,$kode_output){
    	$this->db->update('dipa',$newOutput,array('id_dipa'=>$kode_output));
    	return true;
	}

	public function append(array $newOutput,$id){
		$this->db->trans_start();
		$this->db->query('SELECT @varKanan := kanan FROM dipa WHERE id_dipa = "'.$id.'"');
		$this->db->query('UPDATE dipa SET kanan = kanan + 2 WHERE kanan > @varKanan');
		$this->db->query('UPDATE dipa SET kiri = kiri + 2 WHERE kiri > @varKanan');
		$this->db->set($newOutput);
		$this->db->set('kiri', '@varKanan + 1', FALSE);
		$this->db->set('kanan', '@varKanan + 2', FALSE);
    	$this->db->insert('dipa');
		$this->db->trans_complete();
    	return true;
	}

	public function delete($id)
	{
		$this->db->trans_start();
		$this->db->query('SELECT @varKiri := kiri, @varKanan := kanan, @varSelisih := kanan - kiri + 1 FROM dipa WHERE id_dipa = "'.$id.'"');
		$this->db->query('DELETE FROM dipa WHERE kiri BETWEEN @varKiri AND @varKanan');
		$this->db->query('UPDATE dipa SET kanan = kanan - @varSelisih WHERE kanan > @varKanan');
		$this->db->query('UPDATE dipa SET kiri = kiri - @varSelisih WHERE kiri > @varKanan');
		$this->db->trans_complete();

		return true;
	}

}