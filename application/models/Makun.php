<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Makun extends CI_Model{
	private $table='akun';

	public function getAll(){
		$query=$this->db->get($this->table);
		return $query->result_array();
	}

	public function get($kode_akun){
		$query=$this->db->get_where($this->table,array('kode_akun'=>$kode_akun));
		return $query->row_array();
	}

	public function create(array $newAkun){
    	$this->db->insert($this->table,$newAkun);
    	return true;
	}

	public function update(array $newAkun,$kode_akun){
    	$this->db->update($this->table,$newAkun,array('kode_akun'=>$kode_akun));
    	return true;
	}

	public function delete($kode_akun)
	{
		$this->db->delete($this->table,array('kode_akun'=>$kode_akun));			
		return true;		
	}


}