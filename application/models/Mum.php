<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mum extends CI_Model{

	public function getTreeKegiatan($id_dipa,$tahun=""){
		$kegiatan=$this->db->get_where('dipa',array('id_dipa'=>$id_dipa))->row_array();
		if(empty($tahun))$tahun=$this->session->tahun_anggaran;
		$items=$this->db->query('SELECT anak.id_dipa,anak.tahun_anggaran,anak.kode_kegiatan,anak.kode_bidang, COUNT( induk.id_dipa ) level , CONCAT( anak.kegiatan ) AS kegiatan, anak.kiri, anak.kanan,anak.pagu FROM dipa AS induk, ( SELECT * FROM dipa ) AS anak WHERE anak.kiri <= '.$kegiatan['kiri'].' and anak.kanan >='.$kegiatan['kanan'].' and induk.tahun_anggaran="'.$tahun.'" and anak.kiri > 1 GROUP BY anak.id_dipa ORDER BY `anak`.`kiri` ASC 
			');
		return $items->result_array();
	}

	public function getAll($field=['user'=>'','kegiatan'=>'','waktu'=>'','lokasi'=>'','tag_kpa'=>'','tag_ppk'=>'','tag_evaluasi'=>''],$limit=10,$offset=0){
		$this->db->select('pengajuan_um.*,kalender_kegiatan.*,dipa.*,users.surname,pengajuan_um.id,date_format(waktu,"%d-%m-%Y %H:%i:%s") as waktu,date_format(ppspm.tanggal,"%d-%m-%Y %H:%i:%s") as tanggal_sppm,ppspm.spm,ifnull(ppspm.spm,"Belum Di Upload") as spm_file,ppspm.id as id_spm,date_format(tanda_terima.tanggal,"%d-%m-%Y %H:%i:%s") as tanggal_tanda_terima,tanda_terima.tanda_terima,ifnull(tanda_terima.tanda_terima,"Belum Di Upload") as tanda_terima_file,tanda_terima.id as id_tanda_terima');
		$this->db->join('kalender_kegiatan','pengajuan_um.id_kalender_kegiatan=kalender_kegiatan.id','left');
		$this->db->join('dipa','pengajuan_um.id_kegiatan=dipa.id_dipa','left');
		$this->db->join('users','pengajuan_um.id_user=users.id','left');
		$this->db->join('ppspm','pengajuan_um.id=ppspm.id_pengajuan_um','left');
		$this->db->join('tanda_terima','pengajuan_um.id=tanda_terima.id_pengajuan_um','left');
		//$this->db->join('hasil_kegiatan','pengajuan_um.id=hasil_kegiatan.id_pengajuan_um','left');
		if(!empty($field['user']))$this->db->where('users.id',$field['user']);
		if(!empty($field['waktu']))$this->db->where('date(pengajuan_um.waktu)',$field['waktu']);
		if(!empty($field['lokasi']))$this->db->where('pengajuan_um.lokasi',$field['lokasi']);
		if(!empty($field['tag_kpa']))$this->db->where('pengajuan_um.tag_kpa',$field['tag_kpa']);
		if(!empty($field['tag_ppk']))$this->db->where('pengajuan_um.tag_ppk',$field['tag_ppk']);
		if(!empty($field['tag_evaluasi']))$this->db->where('pengajuan_um.tag_evaluasi',$field['tag_evaluasi']);
		if(!empty($field['kegiatan']))$this->db->where('pengajuan_um.id_kegiatan',$field['kegiatan']);
		$this->db->limit($limit,$offset);
		$query=$this->db->get('pengajuan_um');
		return $query->result_array();
	}

	public function getCountRows($field=['user'=>'','kegiatan'=>'','waktu'=>'','lokasi'=>'','tag_kpa'=>'','tag_ppk'=>'']){
		$this->db->join('kalender_kegiatan','pengajuan_um.id_kalender_kegiatan=kalender_kegiatan.id','left');
		$this->db->join('dipa','kalender_kegiatan.id_kegiatan=dipa.id_dipa','left');
		$this->db->join('users','kalender_kegiatan.id_user=users.id','left');
		$this->db->join('ppspm','pengajuan_um.id=ppspm.id_pengajuan_um','left');
		$this->db->join('tanda_terima','pengajuan_um.id=tanda_terima.id_pengajuan_um','left');
		if(!empty($field['user']))$this->db->where('users.id',$field['user']);
		if(!empty($field['waktu']))$this->db->where('date(pengajuan_um.waktu)',$field['waktu']);
		if(!empty($field['lokasi']))$this->db->where('pengajuan_um.lokasi',$field['lokasi']);
		if(!empty($field['tag_kpa']))$this->db->where('pengajuan_um.tag_kpa',$field['tag_kpa']);
		if(!empty($field['tag_ppk']))$this->db->where('pengajuan_um.tag_ppk',$field['tag_ppk']);
		if(!empty($field['kegiatan']))$this->db->where('kalender_kegiatan.id_kegiatan',$field['kegiatan']);
		$query=$this->db->get('pengajuan_um');
		return $query->num_rows();
	}

	public function get($id){
		$this->db->select('pengajuan_um.*,date_format(waktu,"%d-%m-%Y %H:%i:%s") as waktu,date_format(tanggal_pengajuan,"%d-%m-%Y %H:%i:%s") as tanggal_pengajuan,dipa.id_dipa,dipa.kegiatan,users.surname');
		$this->db->join('dipa','pengajuan_um.id_kegiatan=dipa.id_dipa','left');
		$this->db->join('users','pengajuan_um.id_user=users.id','left');
		$query=$this->db->get_where('pengajuan_um',array('pengajuan_um.id'=>$id));
		return $query->row_array();
	}

	public function getAllBelumSelesai($id_user){
		$query=$this->db->get_where('pengajuan_um',array('tag_evaluasi!='=>2,'id_user'=>$id_user));
		return $query->result_array();
	}

	public function getAllHasilKegiatan($id_pengajuan_um){

		$query=$this->db->get_where('hasil_kegiatan',array('id_pengajuan_um'=>$id_pengajuan_um));
		return $query->result_array();
	}

	public function getDetil($id_pengajuan_um){
		$query=$this->db->get_where('detil_pengajuan_um',array('detil_pengajuan_um.id_pengajuan_um'=>$id_pengajuan_um));
		return $query->result_array();
	}

	public function getTotalDetilBelanja($id_pengajuan_um){
		$this->db->select_sum('jumlah');
		$item=$this->db->get_where('detil_pengajuan_um',array('detil_pengajuan_um.id_pengajuan_um'=>$id_pengajuan_um))->row_array();
		return $item['jumlah'];
	}

	public function getHasilKegiatan($id){
		$query=$this->db->get_where('hasil_kegiatan',array('hasil_kegiatan.id'=>$id));
		return $query->row_array();
	}

	public function getHistoryKegiatan($id){
		$query=$this->db->query('
			select date_format(tanggal,"%d-%m-%Y %H:%i:%s") as tanggal,"KPA" as level,if(status=2,"Belum disetujui","Sudah disetujui") as status_pesan,catatan from persetujuan_kpa where id_pengajuan_um="'.$id.'"
				union ALL 
				select date_format(tanggal_revisi,"%d-%m-%Y %H:%i:%s") as tanggal,revisi as level,concat("Revisi ",revisi) as status_pesan,"" as catatan from revisi where id_pengajuan_um="'.$id.'"  
				union all 
				select date_format(tanggal,"%d-%m-%Y %H:%i:%s") as tanggal,"PPK" as level,if(status=2,"Tidak Lulus","Lulus") as status_pesan,catatan from persetujuan_ppk where id_pengajuan_um="'.$id.'"
				ORDER BY `tanggal` ASC 
			');
		return $query->result_array();
	}

	public function create(array $newUM){
    	$this->db->insert('pengajuan_um',$newUM);
    	return $this->db->insert_id();
	}

	public function createDetil(array $newDetilUM){
    	$this->db->insert('detil_pengajuan_um',$newDetilUM);
    	return $this->db->insert_id();
	}

	public function createEvaluasi(array $newEvaluasi){
    	$this->db->insert('hasil_kegiatan',$newEvaluasi);
    	return true;
	}

	public function update(array $newUM,$id){
    	$this->db->update('pengajuan_um',$newUM,array('id'=>$id));
    	return true;
	}

	public function revisi(array $newUM,$id,$revisi){
		switch ($revisi) {
			case 'KPA':
				$this->db->set('tag_revisi','tag_revisi+1',false);
				$this->db->set('tag_kpa',0);
				break;
			case 'PPK':
				$this->db->set('tag_revisi_ppk','tag_revisi_ppk+1',false);
				$this->db->set('tag_ppk',0);
				break;
			case 'SPM':
				$this->db->set('tag_revisi_spm','tag_revisi_spm+1',false);
				$this->db->set('tag_spm',0);
				break;
			case 'BENDAHARA':
				$this->db->set('tag_revisi_bendahara','tag_revisi_bendahara+1',false);
				$this->db->set('tag_bendahara',0);
				break;
			
			default:
				# code...
				break;
		}
		$this->db->update('pengajuan_um',$newUM,array('id'=>$id));
    	$this->db->set('revisi',$revisi);
    	$this->db->set('id_pengajuan_um',$id);
    	$this->db->insert('revisi');
    	return true;
	}

	public function delete($id)
	{
		$this->db->delete('pengajuan_um',array('id'=>$id));			
		return true;		
	}

	public function deletehasilkegiatan($id)
	{
		$this->db->delete('hasil_kegiatan',array('id'=>$id));			
		return true;		
	}

}